object DataModuleShippers: TDataModuleShippers
  OldCreateOrder = False
  Height = 317
  Width = 445
  object QShippers: TFDQuery
    Connection = DevelopConnection.FB_NORTHWIND
    SQL.Strings = (
      'SELECT ID_SHIPPER  ,'
      '       COMPANY_NAME,'
      '       PHONE'
      'FROM SHIPPERS')
    Left = 72
    Top = 88
    object QShippersID_SHIPPER: TIntegerField
      FieldName = 'ID_SHIPPER'
      Origin = 'ID_SHIPPER'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
    end
    object QShippersCOMPANY_NAME: TWideStringField
      FieldName = 'COMPANY_NAME'
      Origin = 'COMPANY_NAME'
      Required = True
      Size = 40
    end
    object QShippersPHONE: TWideStringField
      FieldName = 'PHONE'
      Origin = 'PHONE'
      Size = 24
    end
  end
end
