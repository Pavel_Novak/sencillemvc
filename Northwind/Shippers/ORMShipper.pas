(***********************************************************)
(* Unit generated automaticaly. Modify width care, please. *)
(* (c) 2016 by Juan C.Cilleruelo                           *)
(* contact with me at juanc.cilleruelo@sencille.es         *)
(***********************************************************)
unit ORMShipper;

interface

uses System.SysUtils, System.Generics.Collections, 
     Data.DB,                                      
     senCille.ORMClasses;                           

type
  TShipperOrm = class(TCustomOrm)
  private
    FLineState   :TOrmStatus;                
    function GetOrmVersion :string;          
  private
    FID_SHIPPER   :TIntegerField;
    FCOMPANY_NAME :TWideStringField;
    FPHONE        :TWideStringField;
  public
    destructor Destroy;   override;
    procedure Initialize; override;
    procedure Clear;               
    function  Clone:TShipperOrm;
    procedure Assign(AShipper :TShipperOrm);                  
    property OrmVersion  :string     read GetOrmVersion;                
    property LineState   :TOrmStatus read FLineState  write FLineState; 
  published
    property ID_SHIPPER   :TIntegerField      read FID_SHIPPER   write FID_SHIPPER  ;
    property COMPANY_NAME :TWideStringField   read FCOMPANY_NAME write FCOMPANY_NAME;
    property PHONE        :TWideStringField   read FPHONE        write FPHONE       ;
  end;

implementation
uses TypInfo;

{ TShipperOrm }

destructor TShipperOrm.Destroy;
begin
   inherited;
end;

procedure TShipperOrm.Initialize;
begin
   FID_SHIPPER := TIntegerField.Create(FDataSet);
   FID_SHIPPER.FieldName     := 'ID_SHIPPER';
   FID_SHIPPER.FieldKind     := fkData;
   FID_SHIPPER.DataSet       := FDataSet;
   FID_SHIPPER.Size          := 0;
   FID_SHIPPER.Required      := True;
   FID_SHIPPER.DisplayFormat := '###,##0';
   Fields.Add('ID_SHIPPER', FID_SHIPPER);

   FCOMPANY_NAME := TWideStringField.Create(FDataSet);
   FCOMPANY_NAME.FieldName     := 'COMPANY_NAME';
   FCOMPANY_NAME.FieldKind     := fkData;
   FCOMPANY_NAME.DataSet       := FDataSet;
   FCOMPANY_NAME.Size          := 40;
   FCOMPANY_NAME.Required      := True;
   Fields.Add('COMPANY_NAME', FCOMPANY_NAME);

   FPHONE := TWideStringField.Create(FDataSet);
   FPHONE.FieldName     := 'PHONE';
   FPHONE.FieldKind     := fkData;
   FPHONE.DataSet       := FDataSet;
   FPHONE.Size          := 24;
   Fields.Add('PHONE', FPHONE);

   FDataSet.CreateDataSet;   
end;

procedure TShipperOrm.Clear;                                   
var Field :TField;                                                  
begin                                                               
   for Field in Fields.Values do begin                              
      Field.Clear;                                                  
   end;                                                             
end;                                                                

function TShipperOrm.Clone: TShipperOrm;
var Field   :TField;        
    MyField :TField;        
begin
    Result := TShipperOrm.Create(DisplayFormats);           
    Result.Edit;                                                  
    for Field in Fields.Values do begin                           
       if not Field.IsNull then begin                             
          Result.Fields.TryGetValue(Field.FieldName, MyField);    
          MyField.Assign(Field);                                  
       end;                                                       
    end;                                                          
                                                                  
                                                                  
    Result.Post;                                                  
end;

procedure TShipperOrm.Assign(AShipper :TShipperOrm);    
var Field   :TField;                                                   
    MyField :TField;                                                   
begin                                                                  
   Edit;                                                               
   for Field in AShipper.Fields.Values do begin                   
      if not Field.IsNull then begin                                   
         Fields.TryGetValue(Field.FieldName, MyField);                 
         MyField.Assign(Field);                                        
      end;                                                             
   end;                                                                
   Post;                                                               
end;


function TShipperOrm.GetOrmVersion: string;                       
begin                                                                  
   Result := '5.08';                                                 
   {3.01 Correction of Clone method                                }   
   {3.02 Now NUMERIC field type is considered TCurrencyField class }   
   {3.03 Uses clause adapted for Firemonkey. (not run yet for VCL) }   
   {3.04 Added BLOB SUB_TYPE 1 (TEXT) fields for Memo Fiels        }   
   {3.05 Added BLOB SUB_TYPE 0 (BLOB) fields for Memo Fiels        }   
   {3.06 Improvement (totally new) method "Clone"                  }   
   {     New method "Assign"                                       }   
   {     New property "LineState :TDetailStatus;"                  }   
   {---------------------------------------------------------------}   
   {4.00 Generated with the information of his TFDDataSet Component}   
   {4.01 Method change his name to GetItemVersion from GetVersion  }   
   {4.02 New method "Clear". Set to Null all fiels, but            }   
   {     do not remove the DetailLines, DisplayFormats, etc.       }   
   {4.03 Removed from the first uses clause FMX.Controls and       }   
   {     FMX.StdCtrls, to allow compatibility with FMX and VCL     }   
   {4.04 Now based on TDataSet instead of TFDDatSet, to allow to   }   
   {     take the field definitions from TClientDataSets.          }   
   {4.05 FDetailLines :TList<TccCustomDragItem>; Disappears from   }   
   {     TccCustomDragItem class.                                  }   
   {   - A new property with the same functionallity but personali-}   
   {     zed, appears on each class, defined, here, in the         }   
   {     TDetailDataClass record.                                  }   
   {   - Base class name, changes his name, from TccCustomDragItem }   
   {     to TFieldValuesItem.                                      }   
   {5.00 Items becomes DBRows. Refactoring for accomodate names.   }   
   {5.01 Added the posibility of enable a MathParser.              }   
   {5.02 Added JSON decorators.                                    }   
   {5.03 Modification into Evaluate method.                        }   
   {5.04 New method EvaluateField.                                 }   
   {5.05 Removed JSON decorators.                                  }   
   {5.06 if the class has a Detailed class, i.e. Lines, or         }   
   {     Composition, when we clone the object, the Detail members }   
   {     will be cloned too.                                       }   
   {5.07 Added overloaded SetVar methods for improve the Parser    }   
   {5.08 Complete rename to ORM instead Row.                       }   
end;                                                                   

end.