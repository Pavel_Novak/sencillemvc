unit DMCustomers;

interface

uses
  System.SysUtils, System.Classes, Data.DB,
  FireDAC.Stan.Intf, FireDAC.Stan.Option, FireDAC.Stan.Param, FireDAC.Stan.Error,
  FireDAC.DatS, FireDAC.Phys.Intf, FireDAC.DApt.Intf, FireDAC.Stan.Async, FireDAC.DApt,
  FireDAC.Comp.Client, FireDAC.Comp.DataSet;

type
  TDataModuleCustomers = class(TDataModule)
    QCustomers: TFDQuery;
    QCustomersID_CUSTOMER: TWideStringField;
    QCustomersCOMPANY_NAME: TWideStringField;
    QCustomersCONTACT_NAME: TWideStringField;
    QCustomersCONTACT_TITLE: TWideStringField;
    QCustomersADDRESS: TWideStringField;
    QCustomersCITY: TWideStringField;
    QCustomersREGION: TWideStringField;
    QCustomersPOSTAL_CODE: TWideStringField;
    QCustomersCOUNTRY: TWideStringField;
    QCustomersPHONE: TWideStringField;
    QCustomersFAX: TWideStringField;
  end;

implementation

{%CLASSGROUP 'FMX.Controls.TControl'}



{$R *.dfm}

end.


