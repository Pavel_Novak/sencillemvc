object DataModuleCustomers: TDataModuleCustomers
  OldCreateOrder = False
  Height = 317
  Width = 445
  object QCustomers: TFDQuery
    Connection = DevelopConnection.FB_NORTHWIND
    SQL.Strings = (
      'SELECT ID_CUSTOMER  ,'
      '       COMPANY_NAME ,'
      '       CONTACT_NAME ,'
      '       CONTACT_TITLE,'
      '       ADDRESS      ,'
      '       CITY         ,'
      '       REGION       ,'
      '       POSTAL_CODE  ,'
      '       COUNTRY      ,'
      '       PHONE        ,'
      '       FAX'
      'FROM CUSTOMERS')
    Left = 72
    Top = 88
    object QCustomersID_CUSTOMER: TWideStringField
      FieldName = 'ID_CUSTOMER'
      Origin = 'ID_CUSTOMER'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
      Size = 5
    end
    object QCustomersCOMPANY_NAME: TWideStringField
      FieldName = 'COMPANY_NAME'
      Origin = 'COMPANY_NAME'
      Required = True
      Size = 40
    end
    object QCustomersCONTACT_NAME: TWideStringField
      FieldName = 'CONTACT_NAME'
      Origin = 'CONTACT_NAME'
      Size = 30
    end
    object QCustomersCONTACT_TITLE: TWideStringField
      FieldName = 'CONTACT_TITLE'
      Origin = 'CONTACT_TITLE'
      Size = 30
    end
    object QCustomersADDRESS: TWideStringField
      FieldName = 'ADDRESS'
      Origin = 'ADDRESS'
      Size = 60
    end
    object QCustomersCITY: TWideStringField
      FieldName = 'CITY'
      Origin = 'CITY'
      Size = 15
    end
    object QCustomersREGION: TWideStringField
      FieldName = 'REGION'
      Origin = 'REGION'
      Size = 15
    end
    object QCustomersPOSTAL_CODE: TWideStringField
      FieldName = 'POSTAL_CODE'
      Origin = 'POSTAL_CODE'
      Size = 10
    end
    object QCustomersCOUNTRY: TWideStringField
      FieldName = 'COUNTRY'
      Origin = 'COUNTRY'
      Size = 15
    end
    object QCustomersPHONE: TWideStringField
      FieldName = 'PHONE'
      Origin = 'PHONE'
      Size = 24
    end
    object QCustomersFAX: TWideStringField
      FieldName = 'FAX'
      Origin = 'FAX'
      Size = 24
    end
  end
end
