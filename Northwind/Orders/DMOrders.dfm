object DataModuleOrders: TDataModuleOrders
  OldCreateOrder = False
  Height = 317
  Width = 445
  object QOrders: TFDQuery
    Connection = DevelopConnection.FB_NORTHWIND
    SQL.Strings = (
      'SELECT ID_ORDER       ,'
      '       ID_CUSTOMER    ,'
      '       ID_EMPLOYEE    ,'
      '       DATE_ORDER     ,'
      '       DATE_REQUIRED  ,'
      '       DATE_SHIPPED   ,'
      '       SHIP_VIA       ,'
      '       FREIGHT        ,'
      '       SHIP_NAME      ,'
      '       SHIP_ADDRESS   ,'
      '       SHIP_CITY      ,'
      '       SHIP_REGION    ,'
      '       SHIP_POSTALCODE,'
      '       SHIP_COUNTRY   '
      'FROM ORDERS')
    Left = 72
    Top = 88
    object QOrdersID_ORDER: TIntegerField
      FieldName = 'ID_ORDER'
      Origin = 'ID_ORDER'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
    end
    object QOrdersID_CUSTOMER: TWideStringField
      FieldName = 'ID_CUSTOMER'
      Origin = 'ID_CUSTOMER'
      Size = 5
    end
    object QOrdersID_EMPLOYEE: TIntegerField
      FieldName = 'ID_EMPLOYEE'
      Origin = 'ID_EMPLOYEE'
    end
    object QOrdersDATE_ORDER: TWideStringField
      FieldName = 'DATE_ORDER'
      Origin = 'DATE_ORDER'
      Size = 15
    end
    object QOrdersDATE_REQUIRED: TWideStringField
      FieldName = 'DATE_REQUIRED'
      Origin = 'DATE_REQUIRED'
      Size = 15
    end
    object QOrdersDATE_SHIPPED: TWideStringField
      FieldName = 'DATE_SHIPPED'
      Origin = 'DATE_SHIPPED'
      Size = 15
    end
    object QOrdersSHIP_VIA: TIntegerField
      FieldName = 'SHIP_VIA'
      Origin = 'SHIP_VIA'
    end
    object QOrdersFREIGHT: TIntegerField
      FieldName = 'FREIGHT'
      Origin = 'FREIGHT'
    end
    object QOrdersSHIP_NAME: TWideStringField
      FieldName = 'SHIP_NAME'
      Origin = 'SHIP_NAME'
      Size = 40
    end
    object QOrdersSHIP_ADDRESS: TWideStringField
      FieldName = 'SHIP_ADDRESS'
      Origin = 'SHIP_ADDRESS'
      Size = 60
    end
    object QOrdersSHIP_CITY: TWideStringField
      FieldName = 'SHIP_CITY'
      Origin = 'SHIP_CITY'
      Size = 15
    end
    object QOrdersSHIP_REGION: TWideStringField
      FieldName = 'SHIP_REGION'
      Origin = 'SHIP_REGION'
      Size = 15
    end
    object QOrdersSHIP_POSTALCODE: TWideStringField
      FieldName = 'SHIP_POSTALCODE'
      Origin = 'SHIP_POSTALCODE'
      Size = 10
    end
    object QOrdersSHIP_COUNTRY: TWideStringField
      FieldName = 'SHIP_COUNTRY'
      Origin = 'SHIP_COUNTRY'
      Size = 15
    end
  end
end
