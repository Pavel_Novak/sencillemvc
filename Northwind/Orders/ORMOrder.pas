(***********************************************************)
(* Unit generated automaticaly. Modify width care, please. *)
(* (c) 2016 by Juan C.Cilleruelo                           *)
(* contact with me at juanc.cilleruelo@sencille.es         *)
(***********************************************************)
unit ORMOrder;

interface

uses System.SysUtils, System.Generics.Collections, 
     Data.DB,                                      
     senCille.ORMClasses;                           

type
  TOrderOrm = class(TCustomOrm)
  private
    FLineState   :TOrmStatus;                
    function GetOrmVersion :string;          
  private
    FID_ORDER        :TIntegerField;
    FID_CUSTOMER     :TWideStringField;
    FID_EMPLOYEE     :TIntegerField;
    FDATE_ORDER      :TWideStringField;
    FDATE_REQUIRED   :TWideStringField;
    FDATE_SHIPPED    :TWideStringField;
    FSHIP_VIA        :TIntegerField;
    FFREIGHT         :TIntegerField;
    FSHIP_NAME       :TWideStringField;
    FSHIP_ADDRESS    :TWideStringField;
    FSHIP_CITY       :TWideStringField;
    FSHIP_REGION     :TWideStringField;
    FSHIP_POSTALCODE :TWideStringField;
    FSHIP_COUNTRY    :TWideStringField;
  public
    destructor Destroy;   override;
    procedure Initialize; override;
    procedure Clear;               
    function  Clone:TOrderOrm;
    procedure Assign(AOrder :TOrderOrm);                  
    property OrmVersion  :string     read GetOrmVersion;                
    property LineState   :TOrmStatus read FLineState  write FLineState; 
  published
    property ID_ORDER        :TIntegerField      read FID_ORDER        write FID_ORDER       ;
    property ID_CUSTOMER     :TWideStringField   read FID_CUSTOMER     write FID_CUSTOMER    ;
    property ID_EMPLOYEE     :TIntegerField      read FID_EMPLOYEE     write FID_EMPLOYEE    ;
    property DATE_ORDER      :TWideStringField   read FDATE_ORDER      write FDATE_ORDER     ;
    property DATE_REQUIRED   :TWideStringField   read FDATE_REQUIRED   write FDATE_REQUIRED  ;
    property DATE_SHIPPED    :TWideStringField   read FDATE_SHIPPED    write FDATE_SHIPPED   ;
    property SHIP_VIA        :TIntegerField      read FSHIP_VIA        write FSHIP_VIA       ;
    property FREIGHT         :TIntegerField      read FFREIGHT         write FFREIGHT        ;
    property SHIP_NAME       :TWideStringField   read FSHIP_NAME       write FSHIP_NAME      ;
    property SHIP_ADDRESS    :TWideStringField   read FSHIP_ADDRESS    write FSHIP_ADDRESS   ;
    property SHIP_CITY       :TWideStringField   read FSHIP_CITY       write FSHIP_CITY      ;
    property SHIP_REGION     :TWideStringField   read FSHIP_REGION     write FSHIP_REGION    ;
    property SHIP_POSTALCODE :TWideStringField   read FSHIP_POSTALCODE write FSHIP_POSTALCODE;
    property SHIP_COUNTRY    :TWideStringField   read FSHIP_COUNTRY    write FSHIP_COUNTRY   ;
  end;

implementation
uses TypInfo;

{ TOrderOrm }

destructor TOrderOrm.Destroy;
begin
   inherited;
end;

procedure TOrderOrm.Initialize;
begin
   FID_ORDER := TIntegerField.Create(FDataSet);
   FID_ORDER.FieldName     := 'ID_ORDER';
   FID_ORDER.FieldKind     := fkData;
   FID_ORDER.DataSet       := FDataSet;
   FID_ORDER.Size          := 0;
   FID_ORDER.Required      := True;
   FID_ORDER.DisplayFormat := '###,##0';
   Fields.Add('ID_ORDER', FID_ORDER);

   FID_CUSTOMER := TWideStringField.Create(FDataSet);
   FID_CUSTOMER.FieldName     := 'ID_CUSTOMER';
   FID_CUSTOMER.FieldKind     := fkData;
   FID_CUSTOMER.DataSet       := FDataSet;
   FID_CUSTOMER.Size          := 5;
   Fields.Add('ID_CUSTOMER', FID_CUSTOMER);

   FID_EMPLOYEE := TIntegerField.Create(FDataSet);
   FID_EMPLOYEE.FieldName     := 'ID_EMPLOYEE';
   FID_EMPLOYEE.FieldKind     := fkData;
   FID_EMPLOYEE.DataSet       := FDataSet;
   FID_EMPLOYEE.Size          := 0;
   FID_EMPLOYEE.DisplayFormat := '###,##0';
   Fields.Add('ID_EMPLOYEE', FID_EMPLOYEE);

   FDATE_ORDER := TWideStringField.Create(FDataSet);
   FDATE_ORDER.FieldName     := 'DATE_ORDER';
   FDATE_ORDER.FieldKind     := fkData;
   FDATE_ORDER.DataSet       := FDataSet;
   FDATE_ORDER.Size          := 15;
   Fields.Add('DATE_ORDER', FDATE_ORDER);

   FDATE_REQUIRED := TWideStringField.Create(FDataSet);
   FDATE_REQUIRED.FieldName     := 'DATE_REQUIRED';
   FDATE_REQUIRED.FieldKind     := fkData;
   FDATE_REQUIRED.DataSet       := FDataSet;
   FDATE_REQUIRED.Size          := 15;
   Fields.Add('DATE_REQUIRED', FDATE_REQUIRED);

   FDATE_SHIPPED := TWideStringField.Create(FDataSet);
   FDATE_SHIPPED.FieldName     := 'DATE_SHIPPED';
   FDATE_SHIPPED.FieldKind     := fkData;
   FDATE_SHIPPED.DataSet       := FDataSet;
   FDATE_SHIPPED.Size          := 15;
   Fields.Add('DATE_SHIPPED', FDATE_SHIPPED);

   FSHIP_VIA := TIntegerField.Create(FDataSet);
   FSHIP_VIA.FieldName     := 'SHIP_VIA';
   FSHIP_VIA.FieldKind     := fkData;
   FSHIP_VIA.DataSet       := FDataSet;
   FSHIP_VIA.Size          := 0;
   FSHIP_VIA.DisplayFormat := '###,##0';
   Fields.Add('SHIP_VIA', FSHIP_VIA);

   FFREIGHT := TIntegerField.Create(FDataSet);
   FFREIGHT.FieldName     := 'FREIGHT';
   FFREIGHT.FieldKind     := fkData;
   FFREIGHT.DataSet       := FDataSet;
   FFREIGHT.Size          := 0;
   FFREIGHT.DisplayFormat := '###,##0';
   Fields.Add('FREIGHT', FFREIGHT);

   FSHIP_NAME := TWideStringField.Create(FDataSet);
   FSHIP_NAME.FieldName     := 'SHIP_NAME';
   FSHIP_NAME.FieldKind     := fkData;
   FSHIP_NAME.DataSet       := FDataSet;
   FSHIP_NAME.Size          := 40;
   Fields.Add('SHIP_NAME', FSHIP_NAME);

   FSHIP_ADDRESS := TWideStringField.Create(FDataSet);
   FSHIP_ADDRESS.FieldName     := 'SHIP_ADDRESS';
   FSHIP_ADDRESS.FieldKind     := fkData;
   FSHIP_ADDRESS.DataSet       := FDataSet;
   FSHIP_ADDRESS.Size          := 60;
   Fields.Add('SHIP_ADDRESS', FSHIP_ADDRESS);

   FSHIP_CITY := TWideStringField.Create(FDataSet);
   FSHIP_CITY.FieldName     := 'SHIP_CITY';
   FSHIP_CITY.FieldKind     := fkData;
   FSHIP_CITY.DataSet       := FDataSet;
   FSHIP_CITY.Size          := 15;
   Fields.Add('SHIP_CITY', FSHIP_CITY);

   FSHIP_REGION := TWideStringField.Create(FDataSet);
   FSHIP_REGION.FieldName     := 'SHIP_REGION';
   FSHIP_REGION.FieldKind     := fkData;
   FSHIP_REGION.DataSet       := FDataSet;
   FSHIP_REGION.Size          := 15;
   Fields.Add('SHIP_REGION', FSHIP_REGION);

   FSHIP_POSTALCODE := TWideStringField.Create(FDataSet);
   FSHIP_POSTALCODE.FieldName     := 'SHIP_POSTALCODE';
   FSHIP_POSTALCODE.FieldKind     := fkData;
   FSHIP_POSTALCODE.DataSet       := FDataSet;
   FSHIP_POSTALCODE.Size          := 10;
   Fields.Add('SHIP_POSTALCODE', FSHIP_POSTALCODE);

   FSHIP_COUNTRY := TWideStringField.Create(FDataSet);
   FSHIP_COUNTRY.FieldName     := 'SHIP_COUNTRY';
   FSHIP_COUNTRY.FieldKind     := fkData;
   FSHIP_COUNTRY.DataSet       := FDataSet;
   FSHIP_COUNTRY.Size          := 15;
   Fields.Add('SHIP_COUNTRY', FSHIP_COUNTRY);

   FDataSet.CreateDataSet;   
end;

procedure TOrderOrm.Clear;                                   
var Field :TField;                                                  
begin                                                               
   for Field in Fields.Values do begin                              
      Field.Clear;                                                  
   end;                                                             
end;                                                                

function TOrderOrm.Clone: TOrderOrm;
var Field   :TField;        
    MyField :TField;        
begin
    Result := TOrderOrm.Create(DisplayFormats);           
    Result.Edit;                                                  
    for Field in Fields.Values do begin                           
       if not Field.IsNull then begin                             
          Result.Fields.TryGetValue(Field.FieldName, MyField);    
          MyField.Assign(Field);                                  
       end;                                                       
    end;                                                          
                                                                  
                                                                  
    Result.Post;                                                  
end;

procedure TOrderOrm.Assign(AOrder :TOrderOrm);    
var Field   :TField;                                                   
    MyField :TField;                                                   
begin                                                                  
   Edit;                                                               
   for Field in AOrder.Fields.Values do begin                   
      if not Field.IsNull then begin                                   
         Fields.TryGetValue(Field.FieldName, MyField);                 
         MyField.Assign(Field);                                        
      end;                                                             
   end;                                                                
   Post;                                                               
end;


function TOrderOrm.GetOrmVersion: string;                       
begin                                                                  
   Result := '5.08';                                                 
   {3.01 Correction of Clone method                                }   
   {3.02 Now NUMERIC field type is considered TCurrencyField class }   
   {3.03 Uses clause adapted for Firemonkey. (not run yet for VCL) }   
   {3.04 Added BLOB SUB_TYPE 1 (TEXT) fields for Memo Fiels        }   
   {3.05 Added BLOB SUB_TYPE 0 (BLOB) fields for Memo Fiels        }   
   {3.06 Improvement (totally new) method "Clone"                  }   
   {     New method "Assign"                                       }   
   {     New property "LineState :TDetailStatus;"                  }   
   {---------------------------------------------------------------}   
   {4.00 Generated with the information of his TFDDataSet Component}   
   {4.01 Method change his name to GetItemVersion from GetVersion  }   
   {4.02 New method "Clear". Set to Null all fiels, but            }   
   {     do not remove the DetailLines, DisplayFormats, etc.       }   
   {4.03 Removed from the first uses clause FMX.Controls and       }   
   {     FMX.StdCtrls, to allow compatibility with FMX and VCL     }   
   {4.04 Now based on TDataSet instead of TFDDatSet, to allow to   }   
   {     take the field definitions from TClientDataSets.          }   
   {4.05 FDetailLines :TList<TccCustomDragItem>; Disappears from   }   
   {     TccCustomDragItem class.                                  }   
   {   - A new property with the same functionallity but personali-}   
   {     zed, appears on each class, defined, here, in the         }   
   {     TDetailDataClass record.                                  }   
   {   - Base class name, changes his name, from TccCustomDragItem }   
   {     to TFieldValuesItem.                                      }   
   {5.00 Items becomes DBRows. Refactoring for accomodate names.   }   
   {5.01 Added the posibility of enable a MathParser.              }   
   {5.02 Added JSON decorators.                                    }   
   {5.03 Modification into Evaluate method.                        }   
   {5.04 New method EvaluateField.                                 }   
   {5.05 Removed JSON decorators.                                  }   
   {5.06 if the class has a Detailed class, i.e. Lines, or         }   
   {     Composition, when we clone the object, the Detail members }   
   {     will be cloned too.                                       }   
   {5.07 Added overloaded SetVar methods for improve the Parser    }   
   {5.08 Complete rename to ORM instead Row.                       }   
end;                                                                   

end.