unit DMOrders;

interface

uses
  System.SysUtils, System.Classes, Data.DB,
  FireDAC.Stan.Intf, FireDAC.Stan.Option, FireDAC.Stan.Param, FireDAC.Stan.Error,
  FireDAC.DatS, FireDAC.Phys.Intf, FireDAC.DApt.Intf, FireDAC.Stan.Async, FireDAC.DApt,
  FireDAC.Comp.Client, FireDAC.Comp.DataSet;

type
  TDataModuleOrders = class(TDataModule)
    QOrders: TFDQuery;
    QOrdersID_ORDER: TIntegerField;
    QOrdersID_CUSTOMER: TWideStringField;
    QOrdersID_EMPLOYEE: TIntegerField;
    QOrdersDATE_ORDER: TWideStringField;
    QOrdersDATE_REQUIRED: TWideStringField;
    QOrdersDATE_SHIPPED: TWideStringField;
    QOrdersSHIP_VIA: TIntegerField;
    QOrdersFREIGHT: TIntegerField;
    QOrdersSHIP_NAME: TWideStringField;
    QOrdersSHIP_ADDRESS: TWideStringField;
    QOrdersSHIP_CITY: TWideStringField;
    QOrdersSHIP_REGION: TWideStringField;
    QOrdersSHIP_POSTALCODE: TWideStringField;
    QOrdersSHIP_COUNTRY: TWideStringField;
  end;

implementation

{%CLASSGROUP 'FMX.Controls.TControl'}

uses DMDevelopConnection;


{$R *.dfm}

end.


