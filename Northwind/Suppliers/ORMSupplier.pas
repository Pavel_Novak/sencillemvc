(***********************************************************)
(* Unit generated automaticaly. Modify width care, please. *)
(* (c) 2016 by Juan C.Cilleruelo                           *)
(* contact with me at juanc.cilleruelo@sencille.es         *)
(***********************************************************)
unit ORMSupplier;

interface

uses System.SysUtils, System.Generics.Collections, 
     Data.DB,                                      
     senCille.ORMClasses;                           

type
  TSupplierOrm = class(TCustomOrm)
  private
    FLineState   :TOrmStatus;                
    function GetOrmVersion :string;          
  private
    FID_SUPPLIER   :TIntegerField;
    FCOMPANY_NAME  :TWideStringField;
    FCONTACT_NAME  :TWideStringField;
    FCONTACT_TITLE :TWideStringField;
    FADDRESS       :TWideStringField;
    FCITY          :TWideStringField;
    FREGION        :TWideStringField;
    FPOSTAL_CODE   :TWideStringField;
    FCOUNTRY       :TWideStringField;
    FPHONE         :TWideStringField;
    FFAX           :TWideStringField;
    FHOME_PAGE     :TWideStringField;
  public
    destructor Destroy;   override;
    procedure Initialize; override;
    procedure Clear;               
    function  Clone:TSupplierOrm;
    procedure Assign(ASupplier :TSupplierOrm);                  
    property OrmVersion  :string     read GetOrmVersion;                
    property LineState   :TOrmStatus read FLineState  write FLineState; 
  published
    property ID_SUPPLIER   :TIntegerField      read FID_SUPPLIER   write FID_SUPPLIER  ;
    property COMPANY_NAME  :TWideStringField   read FCOMPANY_NAME  write FCOMPANY_NAME ;
    property CONTACT_NAME  :TWideStringField   read FCONTACT_NAME  write FCONTACT_NAME ;
    property CONTACT_TITLE :TWideStringField   read FCONTACT_TITLE write FCONTACT_TITLE;
    property ADDRESS       :TWideStringField   read FADDRESS       write FADDRESS      ;
    property CITY          :TWideStringField   read FCITY          write FCITY         ;
    property REGION        :TWideStringField   read FREGION        write FREGION       ;
    property POSTAL_CODE   :TWideStringField   read FPOSTAL_CODE   write FPOSTAL_CODE  ;
    property COUNTRY       :TWideStringField   read FCOUNTRY       write FCOUNTRY      ;
    property PHONE         :TWideStringField   read FPHONE         write FPHONE        ;
    property FAX           :TWideStringField   read FFAX           write FFAX          ;
    property HOME_PAGE     :TWideStringField   read FHOME_PAGE     write FHOME_PAGE    ;
  end;

implementation
uses TypInfo;

{ TSupplierOrm }

destructor TSupplierOrm.Destroy;
begin
   inherited;
end;

procedure TSupplierOrm.Initialize;
begin
   FID_SUPPLIER := TIntegerField.Create(FDataSet);
   FID_SUPPLIER.FieldName     := 'ID_SUPPLIER';
   FID_SUPPLIER.FieldKind     := fkData;
   FID_SUPPLIER.DataSet       := FDataSet;
   FID_SUPPLIER.Size          := 0;
   FID_SUPPLIER.Required      := True;
   FID_SUPPLIER.DisplayFormat := '###,##0';
   Fields.Add('ID_SUPPLIER', FID_SUPPLIER);

   FCOMPANY_NAME := TWideStringField.Create(FDataSet);
   FCOMPANY_NAME.FieldName     := 'COMPANY_NAME';
   FCOMPANY_NAME.FieldKind     := fkData;
   FCOMPANY_NAME.DataSet       := FDataSet;
   FCOMPANY_NAME.Size          := 40;
   FCOMPANY_NAME.Required      := True;
   Fields.Add('COMPANY_NAME', FCOMPANY_NAME);

   FCONTACT_NAME := TWideStringField.Create(FDataSet);
   FCONTACT_NAME.FieldName     := 'CONTACT_NAME';
   FCONTACT_NAME.FieldKind     := fkData;
   FCONTACT_NAME.DataSet       := FDataSet;
   FCONTACT_NAME.Size          := 30;
   Fields.Add('CONTACT_NAME', FCONTACT_NAME);

   FCONTACT_TITLE := TWideStringField.Create(FDataSet);
   FCONTACT_TITLE.FieldName     := 'CONTACT_TITLE';
   FCONTACT_TITLE.FieldKind     := fkData;
   FCONTACT_TITLE.DataSet       := FDataSet;
   FCONTACT_TITLE.Size          := 30;
   Fields.Add('CONTACT_TITLE', FCONTACT_TITLE);

   FADDRESS := TWideStringField.Create(FDataSet);
   FADDRESS.FieldName     := 'ADDRESS';
   FADDRESS.FieldKind     := fkData;
   FADDRESS.DataSet       := FDataSet;
   FADDRESS.Size          := 60;
   Fields.Add('ADDRESS', FADDRESS);

   FCITY := TWideStringField.Create(FDataSet);
   FCITY.FieldName     := 'CITY';
   FCITY.FieldKind     := fkData;
   FCITY.DataSet       := FDataSet;
   FCITY.Size          := 15;
   Fields.Add('CITY', FCITY);

   FREGION := TWideStringField.Create(FDataSet);
   FREGION.FieldName     := 'REGION';
   FREGION.FieldKind     := fkData;
   FREGION.DataSet       := FDataSet;
   FREGION.Size          := 15;
   Fields.Add('REGION', FREGION);

   FPOSTAL_CODE := TWideStringField.Create(FDataSet);
   FPOSTAL_CODE.FieldName     := 'POSTAL_CODE';
   FPOSTAL_CODE.FieldKind     := fkData;
   FPOSTAL_CODE.DataSet       := FDataSet;
   FPOSTAL_CODE.Size          := 10;
   Fields.Add('POSTAL_CODE', FPOSTAL_CODE);

   FCOUNTRY := TWideStringField.Create(FDataSet);
   FCOUNTRY.FieldName     := 'COUNTRY';
   FCOUNTRY.FieldKind     := fkData;
   FCOUNTRY.DataSet       := FDataSet;
   FCOUNTRY.Size          := 15;
   Fields.Add('COUNTRY', FCOUNTRY);

   FPHONE := TWideStringField.Create(FDataSet);
   FPHONE.FieldName     := 'PHONE';
   FPHONE.FieldKind     := fkData;
   FPHONE.DataSet       := FDataSet;
   FPHONE.Size          := 24;
   Fields.Add('PHONE', FPHONE);

   FFAX := TWideStringField.Create(FDataSet);
   FFAX.FieldName     := 'FAX';
   FFAX.FieldKind     := fkData;
   FFAX.DataSet       := FDataSet;
   FFAX.Size          := 24;
   Fields.Add('FAX', FFAX);

   FHOME_PAGE := TWideStringField.Create(FDataSet);
   FHOME_PAGE.FieldName     := 'HOME_PAGE';
   FHOME_PAGE.FieldKind     := fkData;
   FHOME_PAGE.DataSet       := FDataSet;
   FHOME_PAGE.Size          := 2000;
   Fields.Add('HOME_PAGE', FHOME_PAGE);

   FDataSet.CreateDataSet;   
end;

procedure TSupplierOrm.Clear;                                   
var Field :TField;                                                  
begin                                                               
   for Field in Fields.Values do begin                              
      Field.Clear;                                                  
   end;                                                             
end;                                                                

function TSupplierOrm.Clone: TSupplierOrm;
var Field   :TField;        
    MyField :TField;        
begin
    Result := TSupplierOrm.Create(DisplayFormats);           
    Result.Edit;                                                  
    for Field in Fields.Values do begin                           
       if not Field.IsNull then begin                             
          Result.Fields.TryGetValue(Field.FieldName, MyField);    
          MyField.Assign(Field);                                  
       end;                                                       
    end;                                                          
                                                                  
                                                                  
    Result.Post;                                                  
end;

procedure TSupplierOrm.Assign(ASupplier :TSupplierOrm);    
var Field   :TField;                                                   
    MyField :TField;                                                   
begin                                                                  
   Edit;                                                               
   for Field in ASupplier.Fields.Values do begin                   
      if not Field.IsNull then begin                                   
         Fields.TryGetValue(Field.FieldName, MyField);                 
         MyField.Assign(Field);                                        
      end;                                                             
   end;                                                                
   Post;                                                               
end;


function TSupplierOrm.GetOrmVersion: string;                       
begin                                                                  
   Result := '5.08';                                                 
   {3.01 Correction of Clone method                                }   
   {3.02 Now NUMERIC field type is considered TCurrencyField class }   
   {3.03 Uses clause adapted for Firemonkey. (not run yet for VCL) }   
   {3.04 Added BLOB SUB_TYPE 1 (TEXT) fields for Memo Fiels        }   
   {3.05 Added BLOB SUB_TYPE 0 (BLOB) fields for Memo Fiels        }   
   {3.06 Improvement (totally new) method "Clone"                  }   
   {     New method "Assign"                                       }   
   {     New property "LineState :TDetailStatus;"                  }   
   {---------------------------------------------------------------}   
   {4.00 Generated with the information of his TFDDataSet Component}   
   {4.01 Method change his name to GetItemVersion from GetVersion  }   
   {4.02 New method "Clear". Set to Null all fiels, but            }   
   {     do not remove the DetailLines, DisplayFormats, etc.       }   
   {4.03 Removed from the first uses clause FMX.Controls and       }   
   {     FMX.StdCtrls, to allow compatibility with FMX and VCL     }   
   {4.04 Now based on TDataSet instead of TFDDatSet, to allow to   }   
   {     take the field definitions from TClientDataSets.          }   
   {4.05 FDetailLines :TList<TccCustomDragItem>; Disappears from   }   
   {     TccCustomDragItem class.                                  }   
   {   - A new property with the same functionallity but personali-}   
   {     zed, appears on each class, defined, here, in the         }   
   {     TDetailDataClass record.                                  }   
   {   - Base class name, changes his name, from TccCustomDragItem }   
   {     to TFieldValuesItem.                                      }   
   {5.00 Items becomes DBRows. Refactoring for accomodate names.   }   
   {5.01 Added the posibility of enable a MathParser.              }   
   {5.02 Added JSON decorators.                                    }   
   {5.03 Modification into Evaluate method.                        }   
   {5.04 New method EvaluateField.                                 }   
   {5.05 Removed JSON decorators.                                  }   
   {5.06 if the class has a Detailed class, i.e. Lines, or         }   
   {     Composition, when we clone the object, the Detail members }   
   {     will be cloned too.                                       }   
   {5.07 Added overloaded SetVar methods for improve the Parser    }   
   {5.08 Complete rename to ORM instead Row.                       }   
end;                                                                   

end.