object DataModuleSuppliers: TDataModuleSuppliers
  OldCreateOrder = False
  Height = 317
  Width = 445
  object QSuppliers: TFDQuery
    Connection = DevelopConnection.FB_NORTHWIND
    SQL.Strings = (
      'SELECT ID_SUPPLIER  ,'
      '       COMPANY_NAME ,'
      '       CONTACT_NAME ,'
      '       CONTACT_TITLE,'
      '       ADDRESS      ,'
      '       CITY         ,'
      '       REGION       ,'
      '       POSTAL_CODE  ,'
      '       COUNTRY      ,'
      '       PHONE        ,'
      '       FAX          ,'
      '       HOME_PAGE'
      'FROM SUPPLIERS')
    Left = 72
    Top = 88
    object QSuppliersID_SUPPLIER: TIntegerField
      FieldName = 'ID_SUPPLIER'
      Origin = 'ID_SUPPLIER'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
    end
    object QSuppliersCOMPANY_NAME: TWideStringField
      FieldName = 'COMPANY_NAME'
      Origin = 'COMPANY_NAME'
      Required = True
      Size = 40
    end
    object QSuppliersCONTACT_NAME: TWideStringField
      FieldName = 'CONTACT_NAME'
      Origin = 'CONTACT_NAME'
      Size = 30
    end
    object QSuppliersCONTACT_TITLE: TWideStringField
      FieldName = 'CONTACT_TITLE'
      Origin = 'CONTACT_TITLE'
      Size = 30
    end
    object QSuppliersADDRESS: TWideStringField
      FieldName = 'ADDRESS'
      Origin = 'ADDRESS'
      Size = 60
    end
    object QSuppliersCITY: TWideStringField
      FieldName = 'CITY'
      Origin = 'CITY'
      Size = 15
    end
    object QSuppliersREGION: TWideStringField
      FieldName = 'REGION'
      Origin = 'REGION'
      Size = 15
    end
    object QSuppliersPOSTAL_CODE: TWideStringField
      FieldName = 'POSTAL_CODE'
      Origin = 'POSTAL_CODE'
      Size = 10
    end
    object QSuppliersCOUNTRY: TWideStringField
      FieldName = 'COUNTRY'
      Origin = 'COUNTRY'
      Size = 15
    end
    object QSuppliersPHONE: TWideStringField
      FieldName = 'PHONE'
      Origin = 'PHONE'
      Size = 24
    end
    object QSuppliersFAX: TWideStringField
      FieldName = 'FAX'
      Origin = 'FAX'
      Size = 24
    end
    object QSuppliersHOME_PAGE: TWideStringField
      FieldName = 'HOME_PAGE'
      Origin = 'HOME_PAGE'
      Size = 2000
    end
  end
end
