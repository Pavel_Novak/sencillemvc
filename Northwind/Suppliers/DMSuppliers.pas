unit DMSuppliers;

interface

uses
  System.SysUtils, System.Classes, Data.DB,
  FireDAC.Stan.Intf, FireDAC.Stan.Option, FireDAC.Stan.Param, FireDAC.Stan.Error,
  FireDAC.DatS, FireDAC.Phys.Intf, FireDAC.DApt.Intf, FireDAC.Stan.Async, FireDAC.DApt,
  FireDAC.Comp.Client, FireDAC.Comp.DataSet;

type
  TDataModuleSuppliers = class(TDataModule)
    QSuppliers: TFDQuery;
    QSuppliersID_SUPPLIER: TIntegerField;
    QSuppliersCOMPANY_NAME: TWideStringField;
    QSuppliersCONTACT_NAME: TWideStringField;
    QSuppliersCONTACT_TITLE: TWideStringField;
    QSuppliersADDRESS: TWideStringField;
    QSuppliersCITY: TWideStringField;
    QSuppliersREGION: TWideStringField;
    QSuppliersPOSTAL_CODE: TWideStringField;
    QSuppliersCOUNTRY: TWideStringField;
    QSuppliersPHONE: TWideStringField;
    QSuppliersFAX: TWideStringField;
    QSuppliersHOME_PAGE: TWideStringField;
  end;

implementation

{%CLASSGROUP 'FMX.Controls.TControl'}

uses DMDevelopConnection;


{$R *.dfm}

end.


