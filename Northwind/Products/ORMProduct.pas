(***********************************************************)
(* Unit generated automaticaly. Modify width care, please. *)
(* (c) 2016 by Juan C.Cilleruelo                           *)
(* contact with me at juanc.cilleruelo@sencille.es         *)
(***********************************************************)
unit ORMProduct;

interface

uses System.SysUtils, System.Generics.Collections, 
     Data.DB,                                      
     senCille.ORMClasses;                           

type
  TProductOrm = class(TCustomOrm)
  private
    FLineState   :TOrmStatus;                
    function GetOrmVersion :string;          
  private
    FID_PRODUCT        :TIntegerField;
    FDS_PRODUCT        :TWideStringField;
    FID_SUPPLIER       :TIntegerField;
    FID_CATEGORY       :TIntegerField;
    FQUANTITY_PER_UNIT :TWideStringField;
    FUNIT_PRICE        :TBCDField;
    FUNITS_IN_STOCK    :TIntegerField;
    FUNITS_ON_ORDER    :TIntegerField;
    FREORDER_LEVEL     :TIntegerField;
    FDISCONTINUED      :TIntegerField;
  public
    destructor Destroy;   override;
    procedure Initialize; override;
    procedure Clear;               
    function  Clone:TProductOrm;
    procedure Assign(AProduct :TProductOrm);                  
    property OrmVersion  :string     read GetOrmVersion;                
    property LineState   :TOrmStatus read FLineState  write FLineState; 
  published
    property ID_PRODUCT        :TIntegerField      read FID_PRODUCT        write FID_PRODUCT       ;
    property DS_PRODUCT        :TWideStringField   read FDS_PRODUCT        write FDS_PRODUCT       ;
    property ID_SUPPLIER       :TIntegerField      read FID_SUPPLIER       write FID_SUPPLIER      ;
    property ID_CATEGORY       :TIntegerField      read FID_CATEGORY       write FID_CATEGORY      ;
    property QUANTITY_PER_UNIT :TWideStringField   read FQUANTITY_PER_UNIT write FQUANTITY_PER_UNIT;
    property UNIT_PRICE        :TBCDField          read FUNIT_PRICE        write FUNIT_PRICE       ;
    property UNITS_IN_STOCK    :TIntegerField      read FUNITS_IN_STOCK    write FUNITS_IN_STOCK   ;
    property UNITS_ON_ORDER    :TIntegerField      read FUNITS_ON_ORDER    write FUNITS_ON_ORDER   ;
    property REORDER_LEVEL     :TIntegerField      read FREORDER_LEVEL     write FREORDER_LEVEL    ;
    property DISCONTINUED      :TIntegerField      read FDISCONTINUED      write FDISCONTINUED     ;
  end;

implementation
uses TypInfo;

{ TProductOrm }

destructor TProductOrm.Destroy;
begin
   inherited;
end;

procedure TProductOrm.Initialize;
begin
   FID_PRODUCT := TIntegerField.Create(FDataSet);
   FID_PRODUCT.FieldName     := 'ID_PRODUCT';
   FID_PRODUCT.FieldKind     := fkData;
   FID_PRODUCT.DataSet       := FDataSet;
   FID_PRODUCT.Size          := 0;
   FID_PRODUCT.Required      := True;
   FID_PRODUCT.DisplayFormat := '###,##0';
   Fields.Add('ID_PRODUCT', FID_PRODUCT);

   FDS_PRODUCT := TWideStringField.Create(FDataSet);
   FDS_PRODUCT.FieldName     := 'DS_PRODUCT';
   FDS_PRODUCT.FieldKind     := fkData;
   FDS_PRODUCT.DataSet       := FDataSet;
   FDS_PRODUCT.Size          := 40;
   FDS_PRODUCT.Required      := True;
   Fields.Add('DS_PRODUCT', FDS_PRODUCT);

   FID_SUPPLIER := TIntegerField.Create(FDataSet);
   FID_SUPPLIER.FieldName     := 'ID_SUPPLIER';
   FID_SUPPLIER.FieldKind     := fkData;
   FID_SUPPLIER.DataSet       := FDataSet;
   FID_SUPPLIER.Size          := 0;
   FID_SUPPLIER.DisplayFormat := '###,##0';
   Fields.Add('ID_SUPPLIER', FID_SUPPLIER);

   FID_CATEGORY := TIntegerField.Create(FDataSet);
   FID_CATEGORY.FieldName     := 'ID_CATEGORY';
   FID_CATEGORY.FieldKind     := fkData;
   FID_CATEGORY.DataSet       := FDataSet;
   FID_CATEGORY.Size          := 0;
   FID_CATEGORY.DisplayFormat := '###,##0';
   Fields.Add('ID_CATEGORY', FID_CATEGORY);

   FQUANTITY_PER_UNIT := TWideStringField.Create(FDataSet);
   FQUANTITY_PER_UNIT.FieldName     := 'QUANTITY_PER_UNIT';
   FQUANTITY_PER_UNIT.FieldKind     := fkData;
   FQUANTITY_PER_UNIT.DataSet       := FDataSet;
   FQUANTITY_PER_UNIT.Size          := 20;
   Fields.Add('QUANTITY_PER_UNIT', FQUANTITY_PER_UNIT);

   FUNIT_PRICE := TBCDField.Create(FDataSet);
   FUNIT_PRICE.FieldName     := 'UNIT_PRICE';
   FUNIT_PRICE.FieldKind     := fkData;
   FUNIT_PRICE.DataSet       := FDataSet;
   FUNIT_PRICE.Size          := 3;
   FUNIT_PRICE.DisplayFormat := '###,##0.00';
   Fields.Add('UNIT_PRICE', FUNIT_PRICE);

   FUNITS_IN_STOCK := TIntegerField.Create(FDataSet);
   FUNITS_IN_STOCK.FieldName     := 'UNITS_IN_STOCK';
   FUNITS_IN_STOCK.FieldKind     := fkData;
   FUNITS_IN_STOCK.DataSet       := FDataSet;
   FUNITS_IN_STOCK.Size          := 0;
   FUNITS_IN_STOCK.DisplayFormat := '###,##0';
   Fields.Add('UNITS_IN_STOCK', FUNITS_IN_STOCK);

   FUNITS_ON_ORDER := TIntegerField.Create(FDataSet);
   FUNITS_ON_ORDER.FieldName     := 'UNITS_ON_ORDER';
   FUNITS_ON_ORDER.FieldKind     := fkData;
   FUNITS_ON_ORDER.DataSet       := FDataSet;
   FUNITS_ON_ORDER.Size          := 0;
   FUNITS_ON_ORDER.DisplayFormat := '###,##0';
   Fields.Add('UNITS_ON_ORDER', FUNITS_ON_ORDER);

   FREORDER_LEVEL := TIntegerField.Create(FDataSet);
   FREORDER_LEVEL.FieldName     := 'REORDER_LEVEL';
   FREORDER_LEVEL.FieldKind     := fkData;
   FREORDER_LEVEL.DataSet       := FDataSet;
   FREORDER_LEVEL.Size          := 0;
   FREORDER_LEVEL.DisplayFormat := '###,##0';
   Fields.Add('REORDER_LEVEL', FREORDER_LEVEL);

   FDISCONTINUED := TIntegerField.Create(FDataSet);
   FDISCONTINUED.FieldName     := 'DISCONTINUED';
   FDISCONTINUED.FieldKind     := fkData;
   FDISCONTINUED.DataSet       := FDataSet;
   FDISCONTINUED.Size          := 0;
   FDISCONTINUED.Required      := True;
   FDISCONTINUED.DisplayFormat := '###,##0';
   Fields.Add('DISCONTINUED', FDISCONTINUED);

   FDataSet.CreateDataSet;   
end;

procedure TProductOrm.Clear;                                   
var Field :TField;                                                  
begin                                                               
   for Field in Fields.Values do begin                              
      Field.Clear;                                                  
   end;                                                             
end;                                                                

function TProductOrm.Clone: TProductOrm;
var Field   :TField;        
    MyField :TField;        
begin
    Result := TProductOrm.Create(DisplayFormats);           
    Result.Edit;                                                  
    for Field in Fields.Values do begin                           
       if not Field.IsNull then begin                             
          Result.Fields.TryGetValue(Field.FieldName, MyField);    
          MyField.Assign(Field);                                  
       end;                                                       
    end;                                                          
                                                                  
                                                                  
    Result.Post;                                                  
end;

procedure TProductOrm.Assign(AProduct :TProductOrm);    
var Field   :TField;                                                   
    MyField :TField;                                                   
begin                                                                  
   Edit;                                                               
   for Field in AProduct.Fields.Values do begin                   
      if not Field.IsNull then begin                                   
         Fields.TryGetValue(Field.FieldName, MyField);                 
         MyField.Assign(Field);                                        
      end;                                                             
   end;                                                                
   Post;                                                               
end;


function TProductOrm.GetOrmVersion: string;                       
begin                                                                  
   Result := '5.08';                                                 
   {3.01 Correction of Clone method                                }   
   {3.02 Now NUMERIC field type is considered TCurrencyField class }   
   {3.03 Uses clause adapted for Firemonkey. (not run yet for VCL) }   
   {3.04 Added BLOB SUB_TYPE 1 (TEXT) fields for Memo Fiels        }   
   {3.05 Added BLOB SUB_TYPE 0 (BLOB) fields for Memo Fiels        }   
   {3.06 Improvement (totally new) method "Clone"                  }   
   {     New method "Assign"                                       }   
   {     New property "LineState :TDetailStatus;"                  }   
   {---------------------------------------------------------------}   
   {4.00 Generated with the information of his TFDDataSet Component}   
   {4.01 Method change his name to GetItemVersion from GetVersion  }   
   {4.02 New method "Clear". Set to Null all fiels, but            }   
   {     do not remove the DetailLines, DisplayFormats, etc.       }   
   {4.03 Removed from the first uses clause FMX.Controls and       }   
   {     FMX.StdCtrls, to allow compatibility with FMX and VCL     }   
   {4.04 Now based on TDataSet instead of TFDDatSet, to allow to   }   
   {     take the field definitions from TClientDataSets.          }   
   {4.05 FDetailLines :TList<TccCustomDragItem>; Disappears from   }   
   {     TccCustomDragItem class.                                  }   
   {   - A new property with the same functionallity but personali-}   
   {     zed, appears on each class, defined, here, in the         }   
   {     TDetailDataClass record.                                  }   
   {   - Base class name, changes his name, from TccCustomDragItem }   
   {     to TFieldValuesItem.                                      }   
   {5.00 Items becomes DBRows. Refactoring for accomodate names.   }   
   {5.01 Added the posibility of enable a MathParser.              }   
   {5.02 Added JSON decorators.                                    }   
   {5.03 Modification into Evaluate method.                        }   
   {5.04 New method EvaluateField.                                 }   
   {5.05 Removed JSON decorators.                                  }   
   {5.06 if the class has a Detailed class, i.e. Lines, or         }   
   {     Composition, when we clone the object, the Detail members }   
   {     will be cloned too.                                       }   
   {5.07 Added overloaded SetVar methods for improve the Parser    }   
   {5.08 Complete rename to ORM instead Row.                       }   
end;                                                                   

end.