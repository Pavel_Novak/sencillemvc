object DataModuleProducts: TDataModuleProducts
  OldCreateOrder = False
  Height = 317
  Width = 445
  object QProducts: TFDQuery
    Connection = DevelopConnection.FB_NORTHWIND
    SQL.Strings = (
      'SELECT ID_PRODUCT       , '
      '       DS_PRODUCT       ,'
      '       ID_SUPPLIER      ,'
      '       ID_CATEGORY      ,'
      '       QUANTITY_PER_UNIT,'
      '       UNIT_PRICE       ,'
      '       UNITS_IN_STOCK   ,'
      '       UNITS_ON_ORDER   ,'
      '       REORDER_LEVEL    ,'
      '       DISCONTINUED'
      'FROM PRODUCTS')
    Left = 72
    Top = 88
    object QProductsID_PRODUCT: TIntegerField
      FieldName = 'ID_PRODUCT'
      Origin = 'ID_PRODUCT'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
    end
    object QProductsDS_PRODUCT: TWideStringField
      FieldName = 'DS_PRODUCT'
      Origin = 'DS_PRODUCT'
      Required = True
      Size = 40
    end
    object QProductsID_SUPPLIER: TIntegerField
      FieldName = 'ID_SUPPLIER'
      Origin = 'ID_SUPPLIER'
    end
    object QProductsID_CATEGORY: TIntegerField
      FieldName = 'ID_CATEGORY'
      Origin = 'ID_CATEGORY'
    end
    object QProductsQUANTITY_PER_UNIT: TWideStringField
      FieldName = 'QUANTITY_PER_UNIT'
      Origin = 'QUANTITY_PER_UNIT'
    end
    object QProductsUNIT_PRICE: TBCDField
      FieldName = 'UNIT_PRICE'
      Origin = 'UNIT_PRICE'
      Precision = 18
      Size = 3
    end
    object QProductsUNITS_IN_STOCK: TIntegerField
      FieldName = 'UNITS_IN_STOCK'
      Origin = 'UNITS_IN_STOCK'
    end
    object QProductsUNITS_ON_ORDER: TIntegerField
      FieldName = 'UNITS_ON_ORDER'
      Origin = 'UNITS_ON_ORDER'
    end
    object QProductsREORDER_LEVEL: TIntegerField
      FieldName = 'REORDER_LEVEL'
      Origin = 'REORDER_LEVEL'
    end
    object QProductsDISCONTINUED: TIntegerField
      FieldName = 'DISCONTINUED'
      Origin = 'DISCONTINUED'
      Required = True
    end
  end
end
