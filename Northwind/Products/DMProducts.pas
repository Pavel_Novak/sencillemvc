unit DMProducts;

interface

uses
  System.SysUtils, System.Classes, Data.DB,
  FireDAC.Stan.Intf, FireDAC.Stan.Option, FireDAC.Stan.Param, FireDAC.Stan.Error,
  FireDAC.DatS, FireDAC.Phys.Intf, FireDAC.DApt.Intf, FireDAC.Stan.Async, FireDAC.DApt,
  FireDAC.Comp.Client, FireDAC.Comp.DataSet;

type
  TDataModuleProducts = class(TDataModule)
    QProducts: TFDQuery;
    QProductsID_PRODUCT: TIntegerField;
    QProductsDS_PRODUCT: TWideStringField;
    QProductsID_SUPPLIER: TIntegerField;
    QProductsID_CATEGORY: TIntegerField;
    QProductsQUANTITY_PER_UNIT: TWideStringField;
    QProductsUNIT_PRICE: TBCDField;
    QProductsUNITS_IN_STOCK: TIntegerField;
    QProductsUNITS_ON_ORDER: TIntegerField;
    QProductsREORDER_LEVEL: TIntegerField;
    QProductsDISCONTINUED: TIntegerField;
  end;

implementation

{%CLASSGROUP 'FMX.Controls.TControl'}

{$R *.dfm}

end.


