(***********************************************************)
(* Unit generated automaticaly. Modify with care, please!  *)
(* (c) 2016 by Juan Carlos Cilleruelo                      *)
(* contact with me at juanc.cilleruelo@sencille.es         *)
(***********************************************************)
unit ORMCategory;

interface

uses System.SysUtils, System.Generics.Collections, 
     Data.DB,                                      
     senCille.ORMClasses;                           

type
  TCategoryOrm = class(TCustomOrm)
  private
    FLineState   :TOrmStatus;                
    function GetOrmVersion :string;          
  private
    FID_CATEGORY :TIntegerField;
    FDS_CATEGORY :TWideStringField;
    FDESCRIPTION :TBlobField;
    FPICTURE     :TBlobField;
  public
    destructor Destroy;   override;
    procedure Initialize; override;
    procedure Clear;               
    function  Clone:TCategoryOrm;
    procedure Assign(ACategory :TCategoryOrm);                  
    property OrmVersion  :string     read GetOrmVersion;                
    property LineState   :TOrmStatus read FLineState  write FLineState; 
  published
    property ID_CATEGORY :TIntegerField      read FID_CATEGORY write FID_CATEGORY;
    property DS_CATEGORY :TWideStringField   read FDS_CATEGORY write FDS_CATEGORY;
    property DESCRIPTION :TBlobField         read FDESCRIPTION write FDESCRIPTION;
    property PICTURE     :TBlobField         read FPICTURE     write FPICTURE    ;
  end;

implementation
uses TypInfo;

{ TCategoryOrm }

destructor TCategoryOrm.Destroy;
begin
   inherited;
end;

procedure TCategoryOrm.Initialize;
begin
   FID_CATEGORY := TIntegerField.Create(FDataSet);
   FID_CATEGORY.FieldName     := 'ID_CATEGORY';
   FID_CATEGORY.FieldKind     := fkData;
   FID_CATEGORY.DataSet       := FDataSet;
   FID_CATEGORY.Size          := 0;
   FID_CATEGORY.Required      := True;
   FID_CATEGORY.DisplayFormat := '###,##0';
   Fields.Add('ID_CATEGORY', FID_CATEGORY);

   FDS_CATEGORY := TWideStringField.Create(FDataSet);
   FDS_CATEGORY.FieldName     := 'DS_CATEGORY';
   FDS_CATEGORY.FieldKind     := fkData;
   FDS_CATEGORY.DataSet       := FDataSet;
   FDS_CATEGORY.Size          := 15;
   FDS_CATEGORY.Required      := True;
   Fields.Add('DS_CATEGORY', FDS_CATEGORY);

   FDESCRIPTION := TBlobField.Create(FDataSet);
   FDESCRIPTION.FieldName     := 'DESCRIPTION';
   FDESCRIPTION.FieldKind     := fkData;
   FDESCRIPTION.BlobType      := ftBlob;
   FDESCRIPTION.DataSet       := FDataSet;
   FDESCRIPTION.Size          := 0;
   Fields.Add('DESCRIPTION', FDESCRIPTION);

   FPICTURE := TBlobField.Create(FDataSet);
   FPICTURE.FieldName     := 'PICTURE';
   FPICTURE.FieldKind     := fkData;
   FPICTURE.BlobType      := ftBlob;
   FPICTURE.DataSet       := FDataSet;
   FPICTURE.Size          := 0;
   Fields.Add('PICTURE', FPICTURE);

   FDataSet.CreateDataSet;   
end;

procedure TCategoryOrm.Clear;                                   
var Field :TField;                                                  
begin                                                               
   for Field in Fields.Values do begin                              
      Field.Clear;                                                  
   end;                                                             
end;                                                                

function TCategoryOrm.Clone: TCategoryOrm;
var Field   :TField;        
    MyField :TField;        
begin
    Result := TCategoryOrm.Create(DisplayFormats);           
    Result.Edit;                                                  
    for Field in Fields.Values do begin                           
       if not Field.IsNull then begin                             
          Result.Fields.TryGetValue(Field.FieldName, MyField);    
          MyField.Assign(Field);                                  
       end;                                                       
    end;                                                          
                                                                  
                                                                  
    Result.Post;                                                  
end;

procedure TCategoryOrm.Assign(ACategory :TCategoryOrm);    
var Field   :TField;                                                   
    MyField :TField;                                                   
begin                                                                  
   Edit;                                                               
   for Field in ACategory.Fields.Values do begin                   
      if not Field.IsNull then begin                                   
         Fields.TryGetValue(Field.FieldName, MyField);                 
         MyField.Assign(Field);                                        
      end;                                                             
   end;                                                                
   Post;                                                               
end;


function TCategoryOrm.GetOrmVersion: string;                       
begin                                                                  
   Result := '5.08';                                                 
   {3.01 Correction of Clone method                                }   
   {3.02 Now NUMERIC field type is considered TCurrencyField class }   
   {3.03 Uses clause adapted for Firemonkey. (not run yet for VCL) }   
   {3.04 Added BLOB SUB_TYPE 1 (TEXT) fields for Memo Fiels        }   
   {3.05 Added BLOB SUB_TYPE 0 (BLOB) fields for Memo Fiels        }   
   {3.06 Improvement (totally new) method "Clone"                  }   
   {     New method "Assign"                                       }   
   {     New property "LineState :TDetailStatus;"                  }   
   {---------------------------------------------------------------}   
   {4.00 Generated with the information of his TFDDataSet Component}   
   {4.01 Method change his name to GetItemVersion from GetVersion  }   
   {4.02 New method "Clear". Set to Null all fiels, but            }   
   {     do not remove the DetailLines, DisplayFormats, etc.       }   
   {4.03 Removed from the first uses clause FMX.Controls and       }   
   {     FMX.StdCtrls, to allow compatibility with FMX and VCL     }   
   {4.04 Now based on TDataSet instead of TFDDataSet, to allow to  }   
   {     take the field definitions from TClientDataSets.          }   
   {4.05 FDetailLines :TList<TccCustomDragItem>; Disappears from   }   
   {     TccCustomDragItem class.                                  }   
   {   - A new property with the same functionallity but personali-}   
   {     zed, appears on each class, defined, here, in the         }   
   {     TDetailDataClass record.                                  }   
   {   - Base class name, changes his name, from TccCustomDragItem }   
   {     to TFieldValuesItem.                                      }   
   {5.00 Items becomes DBRows. Refactoring for accomodate names.   }   
   {5.01 Added the posibility of enable a MathParser.              }   
   {5.02 Added JSON decorators.                                    }   
   {5.03 Modification into Evaluate method.                        }   
   {5.04 New method EvaluateField.                                 }   
   {5.05 Removed JSON decorators.                                  }   
   {5.06 if the class has a Detailed class, i.e. Lines, or         }   
   {     Composition, when we clone the object, the Detail members }   
   {     will be cloned too.                                       }   
   {5.07 Added overloaded SetVar methods for improve the Parser    }   
   {5.08 Complete rename to ORM instead Row.                       }   
end;                                                                   

end.