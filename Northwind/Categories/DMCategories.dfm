object DataModuleCategories: TDataModuleCategories
  OldCreateOrder = False
  Height = 317
  Width = 445
  object QCategories: TFDQuery
    Connection = DevelopConnection.FB_NORTHWIND
    SQL.Strings = (
      'SELECT ID_CATEGORY,'
      '       DS_CATEGORY,'
      '       DESCRIPTION,'
      '       PICTURE'
      'FROM CATEGORIES')
    Left = 72
    Top = 88
    object QCategoriesID_CATEGORY: TIntegerField
      FieldName = 'ID_CATEGORY'
      Origin = 'ID_CATEGORY'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
    end
    object QCategoriesDS_CATEGORY: TWideStringField
      FieldName = 'DS_CATEGORY'
      Origin = 'DS_CATEGORY'
      Required = True
      Size = 15
    end
    object QCategoriesDESCRIPTION: TBlobField
      FieldName = 'DESCRIPTION'
      Origin = 'DESCRIPTION'
    end
    object QCategoriesPICTURE: TBlobField
      FieldName = 'PICTURE'
      Origin = 'PICTURE'
    end
  end
end
