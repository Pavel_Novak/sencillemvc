unit DMCategories;

interface

uses
  System.SysUtils, System.Classes, Data.DB,
  FireDAC.Stan.Intf, FireDAC.Stan.Option, FireDAC.Stan.Param, FireDAC.Stan.Error,
  FireDAC.DatS, FireDAC.Phys.Intf, FireDAC.DApt.Intf, FireDAC.Stan.Async, FireDAC.DApt,
  FireDAC.Comp.Client, FireDAC.Comp.DataSet;

type
  TDataModuleCategories = class(TDataModule)
    QCategories: TFDQuery;
    QCategoriesID_CATEGORY: TIntegerField;
    QCategoriesDS_CATEGORY: TWideStringField;
    QCategoriesDESCRIPTION: TBlobField;
    QCategoriesPICTURE: TBlobField;
  end;

implementation

{%CLASSGROUP 'FMX.Controls.TControl'}
{$R *.dfm}

end.


