unit DMDevelopConnection;

interface

uses
  System.SysUtils, System.Classes,
  Data.DB,
  FireDAC.Stan.Intf, FireDAC.Stan.Option, FireDAC.Stan.Param, FireDAC.Stan.Error,
  FireDAC.DatS, FireDAC.Phys.Intf, FireDAC.DApt.Intf, FireDAC.Stan.Async, FireDAC.DApt,
  FireDAC.Comp.Client, FireDAC.Comp.DataSet, FireDAC.Phys.IB, FireDAC.Comp.UI, FireDAC.UI.Intf, FireDAC.FMXUI.Wait,
  FireDAC.Stan.Def, FireDAC.Stan.Pool, FireDAC.Phys,
  FireDAC.Phys.FB, FireDAC.Phys.FBDef, FireDAC.Phys.MSSQL, FireDAC.Phys.MSSQLDef, FireDAC.Phys.IBBase, FireDAC.Phys.OracleDef, FireDAC.Stan.ExprFuncs,
  FireDAC.Phys.SQLiteDef, FireDAC.Phys.SQLite, FireDAC.Phys.Oracle, FireDAC.Phys.ODBCBase, FireDAC.Phys.IBDef, FireDAC.Phys.IBLiteDef;

type
  TDevelopConnection = class(TDataModule)
    FB_NORTHWIND: TFDConnection;
    GUIxWaitCursor: TFDGUIxWaitCursor;
    MSSQL_NORTHWIND: TFDConnection;
    FDPhysFBDriverLink: TFDPhysFBDriverLink;
    FDPhysMSSQLDriverLink: TFDPhysMSSQLDriverLink;
    FDPhysOracleDriverLink: TFDPhysOracleDriverLink;
    FDPhysSQLiteDriverLink: TFDPhysSQLiteDriverLink;
    FDPhysIBDriverLink1: TFDPhysIBDriverLink;
  private
  public
  end;

var
  DevelopConnection: TDevelopConnection;

implementation

{%CLASSGROUP 'FMX.Controls.TControl'}

{$R *.dfm}

end.




