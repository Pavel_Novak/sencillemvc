object DataModuleEmployees: TDataModuleEmployees
  OldCreateOrder = False
  Height = 317
  Width = 445
  object QEmployees: TFDQuery
    Connection = DevelopConnection.FB_NORTHWIND
    SQL.Strings = (
      'SELECT ID_EMPLOYEE   ,'
      '       LAST_NAME     ,'
      '       FIRST_NAME    ,'
      '       TITLE         ,'
      '       COURTESY_TITLE,'
      '       BIRTH_DATE    ,'
      '       HIRE_DATE     ,'
      '       ADDRESS       ,'
      '       CITY          ,'
      '       REGION        ,'
      '       POSTAL_CODE   ,'
      '       COUNTRY       ,'
      '       HOME_PHONE    ,'
      '       EXTENSION     ,'
      '       PHOTO         ,'
      '       NOTES         ,'
      '       REPORTS_TO    ,'
      '       PHOTO_PATH '
      'FROM EMPLOYEES')
    Left = 72
    Top = 88
    object QEmployeesID_EMPLOYEE: TIntegerField
      FieldName = 'ID_EMPLOYEE'
      Origin = 'ID_EMPLOYEE'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
    end
    object QEmployeesLAST_NAME: TWideStringField
      FieldName = 'LAST_NAME'
      Origin = 'LAST_NAME'
      Required = True
    end
    object QEmployeesFIRST_NAME: TWideStringField
      FieldName = 'FIRST_NAME'
      Origin = 'FIRST_NAME'
      Required = True
      Size = 10
    end
    object QEmployeesTITLE: TWideStringField
      FieldName = 'TITLE'
      Origin = 'TITLE'
      Size = 30
    end
    object QEmployeesCOURTESY_TITLE: TWideStringField
      FieldName = 'COURTESY_TITLE'
      Origin = 'COURTESY_TITLE'
      Size = 25
    end
    object QEmployeesBIRTH_DATE: TSQLTimeStampField
      FieldName = 'BIRTH_DATE'
      Origin = 'BIRTH_DATE'
    end
    object QEmployeesHIRE_DATE: TSQLTimeStampField
      FieldName = 'HIRE_DATE'
      Origin = 'HIRE_DATE'
    end
    object QEmployeesADDRESS: TWideStringField
      FieldName = 'ADDRESS'
      Origin = 'ADDRESS'
      Size = 60
    end
    object QEmployeesCITY: TWideStringField
      FieldName = 'CITY'
      Origin = 'CITY'
      Size = 15
    end
    object QEmployeesREGION: TWideStringField
      FieldName = 'REGION'
      Origin = 'REGION'
      Size = 15
    end
    object QEmployeesPOSTAL_CODE: TWideStringField
      FieldName = 'POSTAL_CODE'
      Origin = 'POSTAL_CODE'
      Size = 10
    end
    object QEmployeesCOUNTRY: TWideStringField
      FieldName = 'COUNTRY'
      Origin = 'COUNTRY'
      Size = 15
    end
    object QEmployeesHOME_PHONE: TWideStringField
      FieldName = 'HOME_PHONE'
      Origin = 'HOME_PHONE'
      Size = 24
    end
    object QEmployeesEXTENSION: TWideStringField
      FieldName = 'EXTENSION'
      Origin = 'EXTENSION'
      Size = 4
    end
    object QEmployeesPHOTO: TBlobField
      FieldName = 'PHOTO'
      Origin = 'PHOTO'
    end
    object QEmployeesNOTES: TBlobField
      FieldName = 'NOTES'
      Origin = 'NOTES'
    end
    object QEmployeesREPORTS_TO: TIntegerField
      FieldName = 'REPORTS_TO'
      Origin = 'REPORTS_TO'
    end
    object QEmployeesPHOTO_PATH: TWideStringField
      FieldName = 'PHOTO_PATH'
      Origin = 'PHOTO_PATH'
      Size = 255
    end
  end
end
