(***********************************************************)
(* Unit generated automaticaly. Modify width care, please. *)
(* (c) 2016 by Juan C.Cilleruelo                           *)
(* contact with me at juanc.cilleruelo@sencille.es         *)
(***********************************************************)
unit ORMEmployee;

interface

uses System.SysUtils, System.Generics.Collections, 
     Data.DB,                                      
     senCille.ORMClasses;                           

type
  TEmployeeOrm = class(TCustomOrm)
  private
    FLineState   :TOrmStatus;                
    function GetOrmVersion :string;          
  private
    FID_EMPLOYEE    :TIntegerField;
    FLAST_NAME      :TWideStringField;
    FFIRST_NAME     :TWideStringField;
    FTITLE          :TWideStringField;
    FCOURTESY_TITLE :TWideStringField;
    FBIRTH_DATE     :TSQLTimeStampField;
    FHIRE_DATE      :TSQLTimeStampField;
    FADDRESS        :TWideStringField;
    FCITY           :TWideStringField;
    FREGION         :TWideStringField;
    FPOSTAL_CODE    :TWideStringField;
    FCOUNTRY        :TWideStringField;
    FHOME_PHONE     :TWideStringField;
    FEXTENSION      :TWideStringField;
    FPHOTO          :TBlobField;
    FNOTES          :TBlobField;
    FREPORTS_TO     :TIntegerField;
    FPHOTO_PATH     :TWideStringField;
  public
    destructor Destroy;   override;
    procedure Initialize; override;
    procedure Clear;               
    function  Clone:TEmployeeOrm;
    procedure Assign(AEmployee :TEmployeeOrm);                  
    property OrmVersion  :string     read GetOrmVersion;                
    property LineState   :TOrmStatus read FLineState  write FLineState; 
  published
    property ID_EMPLOYEE    :TIntegerField      read FID_EMPLOYEE    write FID_EMPLOYEE   ;
    property LAST_NAME      :TWideStringField   read FLAST_NAME      write FLAST_NAME     ;
    property FIRST_NAME     :TWideStringField   read FFIRST_NAME     write FFIRST_NAME    ;
    property TITLE          :TWideStringField   read FTITLE          write FTITLE         ;
    property COURTESY_TITLE :TWideStringField   read FCOURTESY_TITLE write FCOURTESY_TITLE;
    property BIRTH_DATE     :TSQLTimeStampField read FBIRTH_DATE     write FBIRTH_DATE    ;
    property HIRE_DATE      :TSQLTimeStampField read FHIRE_DATE      write FHIRE_DATE     ;
    property ADDRESS        :TWideStringField   read FADDRESS        write FADDRESS       ;
    property CITY           :TWideStringField   read FCITY           write FCITY          ;
    property REGION         :TWideStringField   read FREGION         write FREGION        ;
    property POSTAL_CODE    :TWideStringField   read FPOSTAL_CODE    write FPOSTAL_CODE   ;
    property COUNTRY        :TWideStringField   read FCOUNTRY        write FCOUNTRY       ;
    property HOME_PHONE     :TWideStringField   read FHOME_PHONE     write FHOME_PHONE    ;
    property EXTENSION      :TWideStringField   read FEXTENSION      write FEXTENSION     ;
    property PHOTO          :TBlobField         read FPHOTO          write FPHOTO         ;
    property NOTES          :TBlobField         read FNOTES          write FNOTES         ;
    property REPORTS_TO     :TIntegerField      read FREPORTS_TO     write FREPORTS_TO    ;
    property PHOTO_PATH     :TWideStringField   read FPHOTO_PATH     write FPHOTO_PATH    ;
  end;

implementation
uses TypInfo;

{ TEmployeeOrm }

destructor TEmployeeOrm.Destroy;
begin
   inherited;
end;

procedure TEmployeeOrm.Initialize;
begin
   FID_EMPLOYEE := TIntegerField.Create(FDataSet);
   FID_EMPLOYEE.FieldName     := 'ID_EMPLOYEE';
   FID_EMPLOYEE.FieldKind     := fkData;
   FID_EMPLOYEE.DataSet       := FDataSet;
   FID_EMPLOYEE.Size          := 0;
   FID_EMPLOYEE.Required      := True;
   FID_EMPLOYEE.DisplayFormat := '###,##0';
   Fields.Add('ID_EMPLOYEE', FID_EMPLOYEE);

   FLAST_NAME := TWideStringField.Create(FDataSet);
   FLAST_NAME.FieldName     := 'LAST_NAME';
   FLAST_NAME.FieldKind     := fkData;
   FLAST_NAME.DataSet       := FDataSet;
   FLAST_NAME.Size          := 20;
   FLAST_NAME.Required      := True;
   Fields.Add('LAST_NAME', FLAST_NAME);

   FFIRST_NAME := TWideStringField.Create(FDataSet);
   FFIRST_NAME.FieldName     := 'FIRST_NAME';
   FFIRST_NAME.FieldKind     := fkData;
   FFIRST_NAME.DataSet       := FDataSet;
   FFIRST_NAME.Size          := 10;
   FFIRST_NAME.Required      := True;
   Fields.Add('FIRST_NAME', FFIRST_NAME);

   FTITLE := TWideStringField.Create(FDataSet);
   FTITLE.FieldName     := 'TITLE';
   FTITLE.FieldKind     := fkData;
   FTITLE.DataSet       := FDataSet;
   FTITLE.Size          := 30;
   Fields.Add('TITLE', FTITLE);

   FCOURTESY_TITLE := TWideStringField.Create(FDataSet);
   FCOURTESY_TITLE.FieldName     := 'COURTESY_TITLE';
   FCOURTESY_TITLE.FieldKind     := fkData;
   FCOURTESY_TITLE.DataSet       := FDataSet;
   FCOURTESY_TITLE.Size          := 25;
   Fields.Add('COURTESY_TITLE', FCOURTESY_TITLE);

   FBIRTH_DATE := TSQLTimeStampField.Create(FDataSet);
   FBIRTH_DATE.FieldName     := 'BIRTH_DATE';
   FBIRTH_DATE.FieldKind     := fkData;
   FBIRTH_DATE.DataSet       := FDataSet;
   FBIRTH_DATE.Size          := 0;
   FBIRTH_DATE.DisplayFormat := 'dd/mm/yyyy';
   Fields.Add('BIRTH_DATE', FBIRTH_DATE);

   FHIRE_DATE := TSQLTimeStampField.Create(FDataSet);
   FHIRE_DATE.FieldName     := 'HIRE_DATE';
   FHIRE_DATE.FieldKind     := fkData;
   FHIRE_DATE.DataSet       := FDataSet;
   FHIRE_DATE.Size          := 0;
   FHIRE_DATE.DisplayFormat := 'dd/mm/yyyy';
   Fields.Add('HIRE_DATE', FHIRE_DATE);

   FADDRESS := TWideStringField.Create(FDataSet);
   FADDRESS.FieldName     := 'ADDRESS';
   FADDRESS.FieldKind     := fkData;
   FADDRESS.DataSet       := FDataSet;
   FADDRESS.Size          := 60;
   Fields.Add('ADDRESS', FADDRESS);

   FCITY := TWideStringField.Create(FDataSet);
   FCITY.FieldName     := 'CITY';
   FCITY.FieldKind     := fkData;
   FCITY.DataSet       := FDataSet;
   FCITY.Size          := 15;
   Fields.Add('CITY', FCITY);

   FREGION := TWideStringField.Create(FDataSet);
   FREGION.FieldName     := 'REGION';
   FREGION.FieldKind     := fkData;
   FREGION.DataSet       := FDataSet;
   FREGION.Size          := 15;
   Fields.Add('REGION', FREGION);

   FPOSTAL_CODE := TWideStringField.Create(FDataSet);
   FPOSTAL_CODE.FieldName     := 'POSTAL_CODE';
   FPOSTAL_CODE.FieldKind     := fkData;
   FPOSTAL_CODE.DataSet       := FDataSet;
   FPOSTAL_CODE.Size          := 10;
   Fields.Add('POSTAL_CODE', FPOSTAL_CODE);

   FCOUNTRY := TWideStringField.Create(FDataSet);
   FCOUNTRY.FieldName     := 'COUNTRY';
   FCOUNTRY.FieldKind     := fkData;
   FCOUNTRY.DataSet       := FDataSet;
   FCOUNTRY.Size          := 15;
   Fields.Add('COUNTRY', FCOUNTRY);

   FHOME_PHONE := TWideStringField.Create(FDataSet);
   FHOME_PHONE.FieldName     := 'HOME_PHONE';
   FHOME_PHONE.FieldKind     := fkData;
   FHOME_PHONE.DataSet       := FDataSet;
   FHOME_PHONE.Size          := 24;
   Fields.Add('HOME_PHONE', FHOME_PHONE);

   FEXTENSION := TWideStringField.Create(FDataSet);
   FEXTENSION.FieldName     := 'EXTENSION';
   FEXTENSION.FieldKind     := fkData;
   FEXTENSION.DataSet       := FDataSet;
   FEXTENSION.Size          := 4;
   Fields.Add('EXTENSION', FEXTENSION);

   FPHOTO := TBlobField.Create(FDataSet);
   FPHOTO.FieldName     := 'PHOTO';
   FPHOTO.FieldKind     := fkData;
   FPHOTO.BlobType      := ftBlob;
   FPHOTO.DataSet       := FDataSet;
   FPHOTO.Size          := 0;
   Fields.Add('PHOTO', FPHOTO);

   FNOTES := TBlobField.Create(FDataSet);
   FNOTES.FieldName     := 'NOTES';
   FNOTES.FieldKind     := fkData;
   FNOTES.BlobType      := ftBlob;
   FNOTES.DataSet       := FDataSet;
   FNOTES.Size          := 0;
   Fields.Add('NOTES', FNOTES);

   FREPORTS_TO := TIntegerField.Create(FDataSet);
   FREPORTS_TO.FieldName     := 'REPORTS_TO';
   FREPORTS_TO.FieldKind     := fkData;
   FREPORTS_TO.DataSet       := FDataSet;
   FREPORTS_TO.Size          := 0;
   FREPORTS_TO.DisplayFormat := '###,##0';
   Fields.Add('REPORTS_TO', FREPORTS_TO);

   FPHOTO_PATH := TWideStringField.Create(FDataSet);
   FPHOTO_PATH.FieldName     := 'PHOTO_PATH';
   FPHOTO_PATH.FieldKind     := fkData;
   FPHOTO_PATH.DataSet       := FDataSet;
   FPHOTO_PATH.Size          := 255;
   Fields.Add('PHOTO_PATH', FPHOTO_PATH);

   FDataSet.CreateDataSet;   
end;

procedure TEmployeeOrm.Clear;                                   
var Field :TField;                                                  
begin                                                               
   for Field in Fields.Values do begin                              
      Field.Clear;                                                  
   end;                                                             
end;                                                                

function TEmployeeOrm.Clone: TEmployeeOrm;
var Field   :TField;        
    MyField :TField;        
begin
    Result := TEmployeeOrm.Create(DisplayFormats);           
    Result.Edit;                                                  
    for Field in Fields.Values do begin                           
       if not Field.IsNull then begin                             
          Result.Fields.TryGetValue(Field.FieldName, MyField);    
          MyField.Assign(Field);                                  
       end;                                                       
    end;                                                          
                                                                  
                                                                  
    Result.Post;                                                  
end;

procedure TEmployeeOrm.Assign(AEmployee :TEmployeeOrm);    
var Field   :TField;                                                   
    MyField :TField;                                                   
begin                                                                  
   Edit;                                                               
   for Field in AEmployee.Fields.Values do begin                   
      if not Field.IsNull then begin                                   
         Fields.TryGetValue(Field.FieldName, MyField);                 
         MyField.Assign(Field);                                        
      end;                                                             
   end;                                                                
   Post;                                                               
end;


function TEmployeeOrm.GetOrmVersion: string;                       
begin                                                                  
   Result := '5.08';                                                 
   {3.01 Correction of Clone method                                }   
   {3.02 Now NUMERIC field type is considered TCurrencyField class }   
   {3.03 Uses clause adapted for Firemonkey. (not run yet for VCL) }   
   {3.04 Added BLOB SUB_TYPE 1 (TEXT) fields for Memo Fiels        }   
   {3.05 Added BLOB SUB_TYPE 0 (BLOB) fields for Memo Fiels        }   
   {3.06 Improvement (totally new) method "Clone"                  }   
   {     New method "Assign"                                       }   
   {     New property "LineState :TDetailStatus;"                  }   
   {---------------------------------------------------------------}   
   {4.00 Generated with the information of his TFDDataSet Component}   
   {4.01 Method change his name to GetItemVersion from GetVersion  }   
   {4.02 New method "Clear". Set to Null all fiels, but            }   
   {     do not remove the DetailLines, DisplayFormats, etc.       }   
   {4.03 Removed from the first uses clause FMX.Controls and       }   
   {     FMX.StdCtrls, to allow compatibility with FMX and VCL     }   
   {4.04 Now based on TDataSet instead of TFDDatSet, to allow to   }   
   {     take the field definitions from TClientDataSets.          }   
   {4.05 FDetailLines :TList<TccCustomDragItem>; Disappears from   }   
   {     TccCustomDragItem class.                                  }   
   {   - A new property with the same functionallity but personali-}   
   {     zed, appears on each class, defined, here, in the         }   
   {     TDetailDataClass record.                                  }   
   {   - Base class name, changes his name, from TccCustomDragItem }   
   {     to TFieldValuesItem.                                      }   
   {5.00 Items becomes DBRows. Refactoring for accomodate names.   }   
   {5.01 Added the posibility of enable a MathParser.              }   
   {5.02 Added JSON decorators.                                    }   
   {5.03 Modification into Evaluate method.                        }   
   {5.04 New method EvaluateField.                                 }   
   {5.05 Removed JSON decorators.                                  }   
   {5.06 if the class has a Detailed class, i.e. Lines, or         }   
   {     Composition, when we clone the object, the Detail members }   
   {     will be cloned too.                                       }   
   {5.07 Added overloaded SetVar methods for improve the Parser    }   
   {5.08 Complete rename to ORM instead Row.                       }   
end;                                                                   

end.