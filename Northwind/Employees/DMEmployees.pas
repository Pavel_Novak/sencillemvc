unit DMEmployees;

interface

uses
  System.SysUtils, System.Classes, Data.DB,
  FireDAC.Stan.Intf, FireDAC.Stan.Option, FireDAC.Stan.Param, FireDAC.Stan.Error,
  FireDAC.DatS, FireDAC.Phys.Intf, FireDAC.DApt.Intf, FireDAC.Stan.Async, FireDAC.DApt,
  FireDAC.Comp.Client, FireDAC.Comp.DataSet;

type
  TDataModuleEmployees = class(TDataModule)
    QEmployees: TFDQuery;
    QEmployeesID_EMPLOYEE: TIntegerField;
    QEmployeesLAST_NAME: TWideStringField;
    QEmployeesFIRST_NAME: TWideStringField;
    QEmployeesTITLE: TWideStringField;
    QEmployeesCOURTESY_TITLE: TWideStringField;
    QEmployeesBIRTH_DATE: TSQLTimeStampField;
    QEmployeesHIRE_DATE: TSQLTimeStampField;
    QEmployeesADDRESS: TWideStringField;
    QEmployeesCITY: TWideStringField;
    QEmployeesREGION: TWideStringField;
    QEmployeesPOSTAL_CODE: TWideStringField;
    QEmployeesCOUNTRY: TWideStringField;
    QEmployeesHOME_PHONE: TWideStringField;
    QEmployeesEXTENSION: TWideStringField;
    QEmployeesPHOTO: TBlobField;
    QEmployeesNOTES: TBlobField;
    QEmployeesREPORTS_TO: TIntegerField;
    QEmployeesPHOTO_PATH: TWideStringField;
  end;

implementation

{%CLASSGROUP 'FMX.Controls.TControl'}



{$R *.dfm}

end.


