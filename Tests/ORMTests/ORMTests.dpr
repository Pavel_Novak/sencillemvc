program ORMTests;

{$IFNDEF TESTINSIGHT}
{$APPTYPE CONSOLE}
{$ENDIF}{$STRONGLINKTYPES ON}
uses
  SysUtils,
  {$IFDEF TESTINSIGHT}
  TestInsight.DUnitX,
  {$ENDIF }
  DUnitX.Loggers.Console,
  DUnitX.Loggers.Xml.NUnit,
  DUnitX.TestFramework,
  senCille.Tools in '..\..\scFramework\senCille.Tools.pas',
  Test_CategoryORM in 'Test_CategoryORM.pas',
  SmokeTest_DMs in 'SmokeTest_DMs.pas',
  DMCategories in '..\..\Northwind\Categories\DMCategories.pas' {DataModuleCategories: TDataModule},
  DMCustomers in '..\..\Northwind\Customers\DMCustomers.pas' {DataModuleCustomers: TDataModule},
  DMEmployees in '..\..\Northwind\Employees\DMEmployees.pas' {DataModuleEmployees: TDataModule},
  DMDevelopConnection in '..\..\Northwind\DMDevelopConnection.pas' {DevelopConnection: TDataModule},
  DMOrders in '..\..\Northwind\Orders\DMOrders.pas' {DataModuleOrders: TDataModule},
  DMProducts in '..\..\Northwind\Products\DMProducts.pas' {DataModuleProducts: TDataModule},
  DMShippers in '..\..\Northwind\Shippers\DMShippers.pas' {DataModuleShippers: TDataModule},
  DMSuppliers in '..\..\Northwind\Suppliers\DMSuppliers.pas' {DataModuleSuppliers: TDataModule},
  senCille.DBController in '..\..\scFramework\senCille.DBController.pas',
  senCille.SQLConnect in '..\..\scFramework\senCille.SQLConnect.pas',
  ORMCategory in '..\..\Northwind\Categories\ORMCategory.pas',
  ORMCustomer in '..\..\Northwind\Customers\ORMCustomer.pas',
  ORMEmployee in '..\..\Northwind\Employees\ORMEmployee.pas',
  ORMOrder in '..\..\Northwind\Orders\ORMOrder.pas',
  ORMProduct in '..\..\Northwind\Products\ORMProduct.pas',
  ORMShipper in '..\..\Northwind\Shippers\ORMShipper.pas',
  ORMSupplier in '..\..\Northwind\Suppliers\ORMSupplier.pas',
  senCille.ORMClasses in '..\..\scFramework\senCille.ORMClasses.pas',
  SmokeTest_ORMs in 'SmokeTest_ORMs.pas';

var
  runner : ITestRunner;
  results : IRunResults;
  logger : ITestLogger;
  nunitLogger : ITestLogger;
begin
{$IFDEF TESTINSIGHT}
  TestInsight.DUnitX.RunRegisteredTests;
  exit;
{$ENDIF}
  try
    //Check command line options, will exit if invalid
    TDUnitX.CheckCommandLine;
    //Create the test runner
    runner := TDUnitX.CreateRunner;
    //Tell the runner to use RTTI to find Fixtures
    runner.UseRTTI := True;
    //tell the runner how we will log things
    //Log to the console window
    logger := TDUnitXConsoleLogger.Create(true);
    runner.AddLogger(logger);
    //Generate an NUnit compatible XML File
    nunitLogger := TDUnitXXMLNUnitFileLogger.Create(TDUnitX.Options.XMLOutputFile);
    runner.AddLogger(nunitLogger);
    runner.FailsOnNoAsserts := False; //When true, Assertions must be made during tests;

    //Run tests
    results := runner.Execute;
    if not results.AllPassed then
      System.ExitCode := EXIT_ERRORS;

    {$IFNDEF CI}
    //We don't want this happening when running under CI.
    if TDUnitX.Options.ExitBehavior = TDUnitXExitBehavior.Pause then
    begin
      System.Write('Done.. press <Enter> key to quit.');
      System.Readln;
    end;
    {$ENDIF}
  except
    on E: Exception do
      System.Writeln(E.ClassName, ': ', E.Message);
  end;
end.
