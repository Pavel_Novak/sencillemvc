unit DummyAppController;

interface


uses
  senCille.MainController,
  senCille.ControllersManager;

type
  TDummyAppController = class
  private
    FMainController :TMainController;
    FControllers    :TControllersManager;
  public
    constructor Create();
    destructor  Destroy; override;
    property Controllers    :TControllersManager read FControllers    write FControllers;
    property MainController :TMainController     read FMainController write FMainController;
  end;

//var FDummyAppController :TDummyAppController;

implementation

constructor TDummyAppController.Create();
begin
   inherited;
   FMainController := TMainController.Create;
   FControllers := TControllersManager.Create;

   FMainController.InstantiateForTests;
end;

destructor TDummyAppController.Destroy;
begin
   FControllers.Free;
   FMainController.Free;
end;

end.
