unit SmokeTest_DMs;

interface

uses DUnitX.TestFramework,
     senCille.Tools,
     senCille.DBController,
     DMCategories, DMCustomers, DMEmployees, DMOrders, DMProducts, DMShippers, DMSuppliers;

type
  [TestFixture]
  TSmokeTestDataModules = class(TObject)
  private
    FPath           :string;
    FConfigFileName :string;
    FDBController   :TDBController;
  public
    [Setup]    procedure Setup;
    [TearDown] procedure TearDown;
    [Test]     procedure SmokeTest_Category;
    [Test]     procedure SmokeTest_Customer;
    [Test]     procedure SmokeTest_Employee;
    [Test]     procedure SmokeTest_Order;
    [Test]     procedure SmokeTest_Product;
    [Test]     procedure SmokeTest_Shipper;
    [Test]     procedure SmokeTest_Supplier;
  end;

implementation

uses Data.DB,
     FMX.Forms, FMX.Objects,
     System.IOUtils;

procedure TSmokeTestDataModules.Setup;
begin
   FPath           := TPath.GetDirectoryName(ParamStr(0))+'\';
   FConfigFileName := TPath.GetDirectoryName(ParamStr(0))+'\Northwind.ini';

   FDBController := TDBController.Create;
   {Firebird}
   FDBController.LoadConfFromFile(FConfigFileName);
   {--------}
   FDBController.OpenConnection;
end;

procedure TSmokeTestDataModules.TearDown;
begin
   FDBController.Free;
end;

procedure TSmokeTestDataModules.SmokeTest_Category;
var DM :TDataModuleCategories;
begin
   DM := TDataModuleCategories.Create(Application);
   DM.QCategories.Connection := FDBController.DBConnection;
   DM.QCategories.Open;

   DM.QCategories.Insert;
   Assert.IsTrue(DM.QCategories.State = dsInsert, 'First time we edit, state shall be dsInsert');
   DM.QCategoriesID_CATEGORY.AsInteger := -393;
   DM.QCategoriesDS_CATEGORY.AsString  := 'This is the begining!';
   DM.QCategories.Post;

   Assert.IsTrue(DM.QCategories.State = dsBrowse, 'After post, without errors, state shall be dsBrowse');

   DM.QCategories.Edit;
   Assert.IsTrue(DM.QCategories.State = dsEdit, 'The second and so on editions, state shall be dsEdit');
   DM.QCategoriesID_CATEGORY.AsInteger := -999;
   DM.QCategoriesDS_CATEGORY.AsString  := 'This is the end!';
   DM.QCategories.Cancel;

   DM.QCategories.Delete;

   Assert.IsTrue(DM.QCategories.State = dsBrowse, 'After cancel, state shall be dsBrowse');

   DM.QCategories.Free;
end;

procedure TSmokeTestDataModules.SmokeTest_Customer;
var DM :TDataModuleCustomers;
begin
   DM := TDataModuleCustomers.Create(Application);
   DM.QCustomers.Connection := FDBController.DBConnection;
   DM.QCustomers.Open;

   DM.QCustomers.Insert;
   Assert.IsTrue(DM.QCustomers.State = dsInsert, 'First time we edit, state shall be dsInsert');
   DM.QCustomersID_CUSTOMER.AsInteger := -999;
   DM.QCustomersCOMPANY_NAME.AsString := 'This is the begining!';
   DM.QCustomers.Post;

   Assert.IsTrue(DM.QCustomers.State = dsBrowse, 'After post, without errors, state shall be dsBrowse');

   DM.QCustomers.Edit;
   Assert.IsTrue(DM.QCustomers.State = dsEdit, 'The second and so on editions, state shall be dsEdit');
   DM.QCustomersID_CUSTOMER.AsInteger := 10;
   DM.QCustomersCONTACT_NAME.AsString := 'This is the end!';
   DM.QCustomers.Cancel;

   DM.QCustomers.Delete;

   Assert.IsTrue(DM.QCustomers.State = dsBrowse, 'After cancel, state shall be dsBrowse');

   DM.QCustomers.Free;
end;

procedure TSmokeTestDataModules.SmokeTest_Employee;
var DM :TDataModuleEmployees;
begin
   DM := TDataModuleEmployees.Create(Application);
   DM.QEmployees.Connection := FDBController.DBConnection;
   DM.QEmployees.Open;

   DM.QEmployees.Insert;
   Assert.IsTrue(DM.QEmployees.State = dsInsert, 'First time we edit, state shall be dsInsert');
   DM.QEmployeesID_EMPLOYEE.AsInteger := -999;
   DM.QEmployeesLAST_NAME.AsString    := 'This is the begining!';
   DM.QEmployeesFIRST_NAME.AsString   := 'This is the begining!';
   DM.QEmployees.Post;

   Assert.IsTrue(DM.QEmployees.State = dsBrowse, 'After post, without errors, state shall be dsBrowse');

   DM.QEmployees.Edit;
   Assert.IsTrue(DM.QEmployees.State = dsEdit, 'The second and so on editions, state shall be dsEdit');
   DM.QEmployeesID_EMPLOYEE.AsInteger := 10;
   DM.QEmployeesLAST_NAME.AsString    := 'This is the end!';
   DM.QEmployeesFIRST_NAME.AsString   := 'This is the end!';
   DM.QEmployees.Cancel;

   DM.QEmployees.Delete;

   Assert.IsTrue(DM.QEmployees.State = dsBrowse, 'After cancel, state shall be dsBrowse');

   DM.QEmployees.Free;
end;

procedure TSmokeTestDataModules.SmokeTest_Order;
var DM :TDataModuleOrders;
begin
   DM := TDataModuleOrders.Create(Application);
   DM.QOrders.Connection := FDBController.DBConnection;
   DM.QOrders.Open;

   DM.QOrders.Insert;
   Assert.IsTrue(DM.QOrders.State = dsInsert, 'First time we edit, state shall be dsInsert');
   DM.QOrdersID_ORDER.AsInteger   := -999;
   DM.QOrdersID_CUSTOMER.AsString := 'ALFKI';
   DM.QOrders.Post;

   Assert.IsTrue(DM.QOrders.State = dsBrowse, 'After post, without errors, state shall be dsBrowse');

   DM.QOrders.Edit;
   Assert.IsTrue(DM.QOrders.State = dsEdit, 'The second and so on editions, state shall be dsEdit');
   DM.QOrdersID_CUSTOMER.AsInteger := 10;
   DM.QOrdersSHIP_NAME.AsString    := 'This is the end!';
   DM.QOrders.Cancel;

   DM.QOrders.Delete;

   Assert.IsTrue(DM.QOrders.State = dsBrowse, 'After cancel, state shall be dsBrowse');

   DM.QOrders.Free;
end;

procedure TSmokeTestDataModules.SmokeTest_Product;
var DM :TDataModuleProducts;
begin
   DM := TDataModuleProducts.Create(Application);
   DM.QProducts.Connection := FDBController.DBConnection;
   DM.QProducts.Open;

   DM.QProducts.Insert;
   Assert.IsTrue(DM.QProducts.State = dsInsert, 'First time we edit, state shall be dsInsert');
   DM.QProductsID_PRODUCT.AsInteger   := -999;
   DM.QProductsDS_PRODUCT.AsString    := 'This is the begining!';
   DM.QProductsDISCONTINUED.AsInteger := 0;
   DM.QProducts.Post;

   Assert.IsTrue(DM.QProducts.State = dsBrowse, 'After post, without errors, state shall be dsBrowse');

   DM.QProducts.Edit;
   Assert.IsTrue(DM.QProducts.State = dsEdit, 'The second and so on editions, state shall be dsEdit');
   DM.QProductsID_PRODUCT.AsInteger := 10;
   DM.QProductsDS_PRODUCT.AsString  := 'This is the end!';
   DM.QProducts.Cancel;

   DM.QProducts.Delete;

   Assert.IsTrue(DM.QProducts.State = dsBrowse, 'After cancel, state shall be dsBrowse');

   DM.QProducts.Free;
end;

procedure TSmokeTestDataModules.SmokeTest_Shipper;
var DM :TDataModuleShippers;
begin
   DM := TDataModuleShippers.Create(Application);
   DM.QShippers.Connection := FDBController.DBConnection;
   DM.QShippers.Open;

   DM.QShippers.Insert;
   Assert.IsTrue(DM.QShippers.State = dsInsert, 'First time we edit, state shall be dsInsert');
   DM.QShippersID_SHIPPER.AsInteger  := -999;
   DM.QShippersCOMPANY_NAME.AsString := 'This is the begining!';
   DM.QShippers.Post;

   Assert.IsTrue(DM.QShippers.State = dsBrowse, 'After post, without errors, state shall be dsBrowse');

   DM.QShippers.Edit;
   Assert.IsTrue(DM.QShippers.State = dsEdit, 'The second and so on editions, state shall be dsEdit');
   DM.QShippersID_SHIPPER.AsInteger := 10;
   DM.QShippersPHONE.AsString       := 'This is the end!';
   DM.QShippers.Cancel;

   DM.QShippers.Delete;

   Assert.IsTrue(DM.QShippers.State = dsBrowse, 'After cancel, state shall be dsBrowse');

   DM.QShippers.Free;
end;

procedure TSmokeTestDataModules.SmokeTest_Supplier;
var DM :TDataModuleSuppliers;
begin
   DM := TDataModuleSuppliers.Create(Application);
   DM.QSuppliers.Connection := FDBController.DBConnection;
   DM.QSuppliers.Open;

   DM.QSuppliers.Insert;
   Assert.IsTrue(DM.QSuppliers.State = dsInsert, 'First time we edit, state shall be dsInsert');
   DM.QSuppliersID_SUPPLIER.AsInteger := -999;
   DM.QSuppliersCOMPANY_NAME.AsString := 'This is the begining!';
   DM.QSuppliers.Post;

   Assert.IsTrue(DM.QSuppliers.State = dsBrowse, 'After post, without errors, state shall be dsBrowse');

   DM.QSuppliers.Edit;
   Assert.IsTrue(DM.QSuppliers.State = dsEdit, 'The second and so on editions, state shall be dsEdit');
   DM.QSuppliersID_SUPPLIER.AsInteger := 10;
   DM.QSuppliersCONTACT_NAME.AsString := 'This is the end!';
   DM.QSuppliers.Cancel;

   DM.QSuppliers.Delete;

   Assert.IsTrue(DM.QSuppliers.State = dsBrowse, 'After cancel, state shall be dsBrowse');

   DM.QSuppliers.Free;
end;

initialization
  TDUnitX.RegisterTestFixture(TSmokeTestDataModules);
end.
