unit SmokeTest_ORMs;

interface
uses
  DUnitX.TestFramework,
  senCille.Tools,
  ORMCategory, ORMCustomer, ORMEmployee, ORMOrder, ORMProduct, ORMShipper, ORMSupplier;

type

  [TestFixture]
  TSomkeTestsOrms = class(TObject)
  private
  public
    [Setup]    procedure Setup;
    [TearDown] procedure TearDown;
    [Test]     procedure SmokeTest_Category;
    [Test]     procedure SmokeTest_Customer;
    [Test]     procedure SmokeTest_Employee;
    [Test]     procedure SmokeTest_Order;
    [Test]     procedure SmokeTest_Product;
    [Test]     procedure SmokeTest_Shipper;
    [Test]     procedure SmokeTest_Supplier;
  end;

implementation

uses Data.DB;

procedure TSomkeTestsOrms.Setup;
begin
end;

procedure TSomkeTestsOrms.TearDown;
begin
end;

procedure TSomkeTestsOrms.SmokeTest_Category;
var Category :TCategoryOrm;
begin
   Category := TCategoryOrm.Create(TDisplayFormats.Create('defaults'));
   Assert.IsTrue(Category.State = dsBrowse, 'Default state shall be dsBrowse');
   Category.Edit;
   Assert.IsTrue(Category.State = dsInsert, 'First time we edit, state shall be dsInsert');
   Category.ID_CATEGORY.AsInteger := 11;
   Category.DS_CATEGORY.AsString  := 'This is the begining!';
   Category.Post;
   Assert.IsTrue(Category.State = dsBrowse, 'After post, without errors, state shall be dsBrowse');
   Category.Edit;
   Assert.IsTrue(Category.State = dsEdit, 'The second and so on editions, state shall be dsEdit');
   Category.ID_CATEGORY.AsInteger := 10;
   Category.DS_CATEGORY.AsString  := 'This is the end!';
   Category.Cancel;
   Assert.IsTrue(Category.State = dsBrowse, 'After cancel, state shall be dsBrowse');
   Category.Free;
end;

procedure TSomkeTestsOrms.SmokeTest_Customer;
var Customer :TCustomerOrm;
begin
   Customer := TCustomerOrm.Create(TDisplayFormats.Create('defaults'));
   Assert.IsTrue(Customer.State = dsBrowse, 'Default state shall be dsBrowse');
   Customer.Edit;
   Assert.IsTrue(Customer.State = dsInsert, 'First time we edit, state shall be dsInsert');
   Customer.ID_CUSTOMER.AsInteger := 11;
   Customer.COMPANY_NAME.AsString := 'This is the begining!';
   Customer.Post;
   Assert.IsTrue(Customer.State = dsBrowse, 'After post, without errors, state shall be dsBrowse');
   Customer.Edit;
   Assert.IsTrue(Customer.State = dsEdit, 'The second and so on editions, state shall be dsEdit');
   Customer.ID_CUSTOMER.AsInteger := 10;
   Customer.CONTACT_NAME.AsString := 'This is the end!';
   Customer.Cancel;
   Assert.IsTrue(Customer.State = dsBrowse, 'After cancel, state shall be dsBrowse');
   Customer.Free;
end;

procedure TSomkeTestsOrms.SmokeTest_Employee;
var Employee :TEmployeeOrm;
begin
   Employee := TEmployeeOrm.Create(TDisplayFormats.Create('defaults'));
   Assert.IsTrue(Employee.State = dsBrowse, 'Default state shall be dsBrowse');
   Employee.Edit;
   Assert.IsTrue(Employee.State = dsInsert, 'First time we edit, state shall be dsInsert');
   Employee.ID_EMPLOYEE.AsInteger := 11;
   Employee.LAST_NAME.AsString    := 'This is the begining!';
   Employee.FIRST_NAME.AsString   := 'This is the begining!';
   Employee.Post;
   Assert.IsTrue(Employee.State = dsBrowse, 'After post, without errors, state shall be dsBrowse');
   Employee.Edit;
   Assert.IsTrue(Employee.State = dsEdit, 'The second and so on editions, state shall be dsEdit');
   Employee.ID_EMPLOYEE.AsInteger := 10;
   Employee.LAST_NAME.AsString    := 'This is the end!';
   Employee.FIRST_NAME.AsString   := 'This is the end!';
   Employee.Cancel;
   Assert.IsTrue(Employee.State = dsBrowse, 'After cancel, state shall be dsBrowse');
   Employee.Free;
end;

procedure TSomkeTestsOrms.SmokeTest_Order;
var Order :TOrderOrm;
begin
   Order := TOrderOrm.Create(TDisplayFormats.Create('defaults'));
   Assert.IsTrue(Order.State = dsBrowse, 'Default state shall be dsBrowse');
   Order.Edit;
   Assert.IsTrue(Order.State = dsInsert, 'First time we edit, state shall be dsInsert');
   Order.ID_ORDER.AsInteger    := 11;
   Order.ID_CUSTOMER.AsInteger := 32;
   Order.Post;
   Assert.IsTrue(Order.State = dsBrowse, 'After post, without errors, state shall be dsBrowse');
   Order.Edit;
   Assert.IsTrue(Order.State = dsEdit, 'The second and so on editions, state shall be dsEdit');
   Order.ID_CUSTOMER.AsInteger := 10;
   Order.SHIP_NAME.AsString    := 'This is the end!';
   Order.Cancel;
   Assert.IsTrue(Order.State = dsBrowse, 'After cancel, state shall be dsBrowse');
   Order.Free;
end;

procedure TSomkeTestsOrms.SmokeTest_Product;
var Product :TProductOrm;
begin
   Product := TProductOrm.Create(TDisplayFormats.Create('defaults'));
   Assert.IsTrue(Product.State = dsBrowse, 'Default state shall be dsBrowse');
   Product.Edit;
   Assert.IsTrue(Product.State = dsInsert, 'First time we edit, state shall be dsInsert');
   Product.ID_PRODUCT.AsInteger   := 11;
   Product.DS_PRODUCT.AsString    := 'This is the begining!';
   Product.DISCONTINUED.AsInteger := 0;
   Product.Post;
   Assert.IsTrue(Product.State = dsBrowse, 'After post, without errors, state shall be dsBrowse');
   Product.Edit;
   Assert.IsTrue(Product.State = dsEdit, 'The second and so on editions, state shall be dsEdit');
   Product.ID_PRODUCT.AsInteger := 10;
   Product.DS_PRODUCT.AsString  := 'This is the end!';
   Product.Cancel;
   Assert.IsTrue(Product.State = dsBrowse, 'After cancel, state shall be dsBrowse');
   Product.Free;
end;

procedure TSomkeTestsOrms.SmokeTest_Shipper;
var Shipper :TShipperOrm;
begin
   Shipper := TShipperOrm.Create(TDisplayFormats.Create('defaults'));
   Assert.IsTrue(Shipper.State = dsBrowse, 'Default state shall be dsBrowse');
   Shipper.Edit;
   Assert.IsTrue(Shipper.State = dsInsert, 'First time we edit, state shall be dsInsert');
   Shipper.ID_SHIPPER.AsInteger  := 11;
   Shipper.COMPANY_NAME.AsString := 'This is the begining!';
   Shipper.Post;
   Assert.IsTrue(Shipper.State = dsBrowse, 'After post, without errors, state shall be dsBrowse');
   Shipper.Edit;
   Assert.IsTrue(Shipper.State = dsEdit, 'The second and so on editions, state shall be dsEdit');
   Shipper.ID_SHIPPER.AsInteger := 10;
   Shipper.PHONE.AsString       := 'This is the end!';
   Shipper.Cancel;
   Assert.IsTrue(Shipper.State = dsBrowse, 'After cancel, state shall be dsBrowse');
   Shipper.Free;
end;

procedure TSomkeTestsOrms.SmokeTest_Supplier;
var Supplier :TSupplierOrm;
begin
   Supplier := TSupplierOrm.Create(TDisplayFormats.Create('defaults'));
   Assert.IsTrue(Supplier.State = dsBrowse, 'Default state shall be dsBrowse');
   Supplier.Edit;
   Assert.IsTrue(Supplier.State = dsInsert, 'First time we edit, state shall be dsInsert');
   Supplier.ID_SUPPLIER.AsInteger := 11;
   Supplier.COMPANY_NAME.AsString := 'This is the begining!';
   Supplier.Post;
   Assert.IsTrue(Supplier.State = dsBrowse, 'After post, without errors, state shall be dsBrowse');
   Supplier.Edit;
   Assert.IsTrue(Supplier.State = dsEdit, 'The second and so on editions, state shall be dsEdit');
   Supplier.ID_SUPPLIER.AsInteger := 10;
   Supplier.CONTACT_NAME.AsString := 'This is the end!';
   Supplier.Cancel;
   Assert.IsTrue(Supplier.State = dsBrowse, 'After cancel, state shall be dsBrowse');
   Supplier.Free;
end;

initialization
  TDUnitX.RegisterTestFixture(TSomkeTestsOrms);
end.
