unit Test_CategoryORM;

interface
uses
  DUnitX.TestFramework,
  senCille.Tools,
  senCille.ORMClasses,
  ORMCategory;

type
  [TestFixture]
  TestsCategoryOrm = class(TObject)
  private
    Category :TCategoryOrm;
    dummystr :string;
    procedure DoCalcsCalculate(Value :TCustomOrm);
  public
    [Setup]    procedure Setup;
    [TearDown] procedure TearDown;
    {Tests}
    [Test] procedure TestOrmVersion;
    [Test] procedure TestCreateAndFree;
    [Test] procedure TestDisplayFormats;
    [Test] procedure TestClear;
    [Test] procedure TestEditPost;
    [Test] procedure TestEditCancel;
    [Test] procedure TestClone;
    [Test] procedure TestAssign;
    [Test] procedure TestLineState;
    [Test] procedure TestNotNullFields;
    [Test] procedure TestCalculate;
    [Test] procedure TestDisableCalcs;
    [Test] procedure TestEnableCalcs;
    [Test] procedure TestModifiedFunction;
    [Test] procedure TestInViewIndex;
    [Test] procedure TestBindDataset;
    [Test] procedure TestState;
  end;

implementation

uses Data.DB;

procedure TestsCategoryOrm.Setup;
begin
   Category := TCategoryOrm.Create(TDisplayFormats.Create('defaults'));
   dummystr := '';
end;

procedure TestsCategoryOrm.TearDown;
begin
   Category.Free;
end;

procedure TestsCategoryOrm.TestOrmVersion;
begin
   Assert.IsTrue(Category.OrmVersion = '5.08', 'Consider that this tests are not updated to the version of the Row: '+Category.OrmVersion);
end;

procedure TestsCategoryOrm.TestCreateAndFree;
var NewCategory :TCategoryOrm;
    dummystr    :string;
begin
   NewCategory := TCategoryOrm.Create(TDisplayFormats.Create('defaults'));
   try
     NewCategory.Free;
   except
      Assert.IsTrue(False, 'Free method has failed');
   end;
end;

procedure TestsCategoryOrm.TestDisplayFormats;
begin
   Assert.IsTrue(Category.DisplayFormats.IntegerFormat  = '#'               , Category.DisplayFormats.IntegerFormat );
   Assert.IsTrue(Category.DisplayFormats.DecimalFormat  = '#.##'            , Category.DisplayFormats.DecimalFormat );
   Assert.IsTrue(Category.DisplayFormats.CodeFormat     = '0'               , Category.DisplayFormats.CodeFormat    );
   Assert.IsTrue(Category.DisplayFormats.PercentFormat  = '#.## %'          , Category.DisplayFormats.PercentFormat );
   Assert.IsTrue(Category.DisplayFormats.PriceFormat    = '#.##'            , Category.DisplayFormats.PriceFormat   );
   Assert.IsTrue(Category.DisplayFormats.ImportFormat   = '#.##'            , Category.DisplayFormats.ImportFormat  );
   Assert.IsTrue(Category.DisplayFormats.QuantityFormat = '#.##'            , Category.DisplayFormats.QuantityFormat);
   Assert.IsTrue(Category.DisplayFormats.DateFormat     = 'dd/mm/yyyy'      , Category.DisplayFormats.DateFormat    );
   Assert.IsTrue(Category.DisplayFormats.TimeFormat     = 'mm:ss'           , Category.DisplayFormats.TimeFormat    );
   Assert.IsTrue(Category.DisplayFormats.DateTimeFormat = 'dd/mm/yyyy mm:ss', Category.DisplayFormats.DateTimeFormat);
end;

procedure TestsCategoryOrm.TestClear;
begin
   Category.Edit;
   Category.ID_CATEGORY.AsInteger := -1;
   Category.DS_CATEGORY.AsString  := 'This is a text';
   Category.Post;

   Category.Edit;
   Category.Clear;
   {Check the values before confirm, because empty values can't be confirmed}
   Assert.IsTrue(Category.ID_CATEGORY.AsInteger = 0 , Category.ID_CATEGORY.AsString);
   Assert.IsTrue(Category.DS_CATEGORY.AsString  = '', Category.DS_CATEGORY.AsString);

   {We made cancel because empty values can't be confirmed}
   Category.Cancel;
end;

procedure TestsCategoryOrm.TestEditPost;
begin
   Category.Edit;
   Category.ID_CATEGORY.AsInteger := -1;
   Category.DS_CATEGORY.AsString  := 'This is a text';
   Category.Post;

   Assert.IsTrue(Category.ID_CATEGORY.AsInteger = -1              , Category.ID_CATEGORY.AsString);
   Assert.IsTrue(Category.DS_CATEGORY.AsString  = 'This is a text', Category.DS_CATEGORY.AsString);
end;

procedure TestsCategoryOrm.TestEditCancel;
begin
   Category.Edit;
   Category.ID_CATEGORY.AsInteger := 99;
   Category.DS_CATEGORY.AsString  := 'New text';
   Category.Cancel;

   Assert.IsTrue(Category.ID_CATEGORY.AsInteger = 0 , Category.ID_CATEGORY.AsString);
   Assert.IsTrue(Category.DS_CATEGORY.AsString  = '', Category.DS_CATEGORY.AsString);
end;

procedure TestsCategoryOrm.TestClone;
var NewCategory :TCategoryOrm;
begin
   Category.Edit;
   Category.ID_CATEGORY.AsInteger := 99;
   Category.DS_CATEGORY.AsString  := 'New text';
   Category.Post;

   NewCategory := Category.Clone;

   Assert.IsTrue(Category.ID_CATEGORY.AsInteger = 99        , Category.ID_CATEGORY.AsString);
   Assert.IsTrue(Category.DS_CATEGORY.AsString  = 'New text', Category.DS_CATEGORY.AsString);

   Assert.IsTrue(NewCategory.ID_CATEGORY.AsInteger = 99        , NewCategory.ID_CATEGORY.AsString);
   Assert.IsTrue(NewCategory.DS_CATEGORY.AsString  = 'New text', NewCategory.DS_CATEGORY.AsString);

   NewCategory.Free;
end;

procedure TestsCategoryOrm.TestAssign;
var NewCategory :TCategoryOrm;
begin
   Category.Edit;
   Category.ID_CATEGORY.AsInteger := 99;
   Category.DS_CATEGORY.AsString  := 'New text';
   Category.Post;

   NewCategory := TCategoryOrm.Create(TDisplayFormats.Create('defaults'));

   NewCategory.Assign(Category);

   Assert.IsTrue(Category.ID_CATEGORY.AsInteger = 99        , Category.ID_CATEGORY.AsString);
   Assert.IsTrue(Category.DS_CATEGORY.AsString  = 'New text', Category.DS_CATEGORY.AsString);

   Assert.IsTrue(NewCategory.ID_CATEGORY.AsInteger = 99        , NewCategory.ID_CATEGORY.AsString);
   Assert.IsTrue(NewCategory.DS_CATEGORY.AsString  = 'New text', NewCategory.DS_CATEGORY.AsString);

   NewCategory.Free;
end;

procedure TestsCategoryOrm.TestNotNullFields;
begin
   Category.Edit;
   Category.ID_CATEGORY.Clear;
   Category.DS_CATEGORY.AsString := 'New text';
   {This is very annoying test, because stops the normal running}
   {We comment this in development time. In final test time, this shall be uncomment}
   //Assert.WillRaise(Category.Post, EDatabaseError, 'Shall show an Exception, because ID_CATEGORY is NOT NULL');
end;

procedure TestsCategoryOrm.DoCalcsCalculate(Value :TCustomOrm);
begin
   dummystr := 'perfect!';
end;

procedure TestsCategoryOrm.TestCalculate;
begin
   Category.OnCalculate := DoCalcsCalculate;
   Category.Edit;
   Category.ID_CATEGORY.AsInteger := 10;
   Category.DS_CATEGORY.AsString := 'New text';
   Category.Post;

   Category.Calculate;

   Assert.IsTrue(dummystr = 'perfect!', 'Shall be calculated');
end;

procedure TestsCategoryOrm.TestModifiedFunction;
var OldValues :TCategoryOrm;
begin
   Category.Edit;
   Category.ID_CATEGORY.AsInteger := 1;
   Category.DS_CATEGORY.AsString  := 'Hello Workd!';
   Category.Post;

   OldValues := Category.Clone;

   Assert.IsFalse(Category.Modified(OldValues), 'There are identical');

   Category.Edit;
   Category.ID_CATEGORY.AsInteger := 10;
   Category.DS_CATEGORY.AsString  := 'This is the end!';
   Category.Post;

   Assert.IsTrue(Category.Modified(OldValues), 'There are modifications');
end;

procedure TestsCategoryOrm.TestInViewIndex;
begin
   Assert.IsTrue(Category.InViewIndex = -1, 'Value -1 indicate that it has been never assigned.');
end;

procedure TestsCategoryOrm.TestLineState;
begin
   {Test still not developed}
   Assert.IsTrue(True, 'LineState can be tested without "Details"');
end;

procedure TestsCategoryOrm.TestDisableCalcs;
begin
   {This test has only sense if we have assigned a BindDataset}
   Assert.IsTrue(True, 'Test still not developed: DisableCalcs');
end;

procedure TestsCategoryOrm.TestEnableCalcs;
begin
   {This test has only sense if we have assigned a BindDataset}
   Assert.IsTrue(True, 'Test still not developed: EnableCalcs');
end;

procedure TestsCategoryOrm.TestBindDataset;
begin
   Assert.IsTrue(True, 'We have still not made the test with a BindDataSet Assigned');
end;

procedure TestsCategoryOrm.TestState;
begin
   Assert.IsTrue(Category.State = dsBrowse, 'Default state shall be dsBrowse');
   Category.Edit;
   Assert.IsTrue(Category.State = dsInsert, 'First time we edit, state shall be dsInsert');
   Category.ID_CATEGORY.AsInteger := 11;
   Category.DS_CATEGORY.AsString  := 'This is the begining!';
   Category.Post;
   Assert.IsTrue(Category.State = dsBrowse, 'After post, without errors, state shall be dsBrowse');
   Category.Edit;
   Assert.IsTrue(Category.State = dsEdit, 'The second and so on editions, state shall be dsEdit');
   Category.ID_CATEGORY.AsInteger := 10;
   Category.DS_CATEGORY.AsString  := 'This is the end!';
   Category.Cancel;
   Assert.IsTrue(Category.State = dsBrowse, 'After cancel, state shall be dsBrowse');
end;

initialization
  TDUnitX.RegisterTestFixture(TestsCategoryOrm);
end.
