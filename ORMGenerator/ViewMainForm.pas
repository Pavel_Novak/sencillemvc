unit ViewMainForm;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants,
  FMX.Types, FMX.Controls, FMX.Forms, FMX.Graphics, FMX.Dialogs, FMX.StdCtrls, FMX.Layouts, FMX.TreeView,
  FireDAC.Comp.DataSet, FireDAC.Stan.Intf, FireDAC.Stan.Option, FireDAC.Stan.Error, FireDAC.UI.Intf, FireDAC.Phys.Intf,
  FireDAC.Stan.Def, FireDAC.Stan.Pool, FireDAC.Stan.Async, FireDAC.Phys, FireDAC.Phys.IB, FireDAC.Phys.IBDef,
  FireDAC.Phys.IBBase, Data.DB, FireDAC.Comp.Client, FMX.Memo, FMX.Controls.Presentation, FMX.ScrollBox, FireDAC.FMXUI.Wait;

type
  TDetailDataClass = class(TPersistent)
  public
     UnitName   :string;
     TypeName   :string;
     PropName   :string;
     MemberName :string;
  end;

  TDataSetClass = class(TPersistent)
  public
     TableName  :string;
     Singular   :string;
     Plural     :string;
     DataSet    :TDataSet;
     FileName   :string;
     WithDetail :Boolean;
     WithParser :Boolean;
     DetailData :TDetailDataClass;
  end;

  TMainForm = class(TForm)
    Layout1: TLayout;
    Layout2: TLayout;
    Splitter1: TSplitter;
    Layout3: TLayout;
    Splitter2: TSplitter;
    Layout4: TLayout;
    TreeViewClasses: TTreeView;
    DBConnection: TFDConnection;
    PhysIBDriver: TFDPhysIBDriverLink;
    Memo4_0: TMemo;
    Layout5: TLayout;
    BtnRunCurrent: TButton;
    procedure FormCreate(Sender: TObject);
    procedure BtnRunCurrentClick(Sender: TObject);
  private
    function InsertIntoTreeView(RootNode :TTreeViewItem; AText :string; AClass :TDataSetClass):TTreeViewItem;
    procedure RaiseException(Error: string);
    function  RFill(StrData :string; Long :Integer):string;
    function  GetMaxLengthFieldName(aDataSet :TDataSet):Integer;
    procedure GenerateItemClass5_0(DSClass :TDataSetClass);
  public
  end;

var
  MainForm: TMainForm;

implementation

{$R *.fmx}

uses System.Math,
     DMCategories, DMCustomers, DMEmployees, DMProducts, DMShippers, DMSuppliers, DMOrders;


const NorthwindFolder  = '..\Northwind\';

var DataModuleCategories :TDataModuleCategories;
    DataModuleCustomers  :TDataModuleCustomers ;
    DataModuleEmployees  :TDataModuleEmployees ;
    DataModuleProducts   :TDataModuleProducts  ;
    DataModuleShippers   :TDataModuleShippers  ;
    DataModuleSuppliers  :TDataModuleSuppliers ;
    DataModuleOrders     :TDataModuleOrders    ;


procedure TMainForm.BtnRunCurrentClick(Sender: TObject);
var DataSetClass :TDataSetClass;
begin
   if TreeViewClasses.Selected <> nil then begin
     if TreeViewClasses.Selected.TagObject <> nil then begin
        DataSetClass := TDataSetClass(TreeViewClasses.Selected.TagObject);
        GenerateItemClass5_0(DataSetClass);
        Memo4_0.Lines.SaveToFile(DataSetClass.FileName);
     end;
  end;
end;

procedure TMainForm.FormCreate(Sender: TObject);
var DataSetClass   :TDataSetClass;
    Node           :TTreeViewItem;
begin
   DataModuleCategories := TDataModuleCategories.Create(Application);
   DataModuleCustomers  := TDataModuleCustomers.Create(Application) ;
   DataModuleEmployees  := TDataModuleEmployees.Create(Application) ;
   DataModuleProducts   := TDataModuleProducts.Create(Application ) ;
   DataModuleShippers   := TDataModuleShippers.Create(Application ) ;
   DataModuleSuppliers  := TDataModuleSuppliers.Create(Application) ;
   DataModuleOrders     := TDataModuleOrders.Create(Application)    ;

   Node  := InsertIntoTreeView(nil, 'NORTHWIND', nil);


   { =====  NORTHWIND ======}
   {-----------------------------------------------------------------------------}
   DataSetClass := TDataSetClass.Create;
   DataSetClass.TableName  := 'CATEGORIES';
   DataSetClass.Singular   := 'Category';
   DataSetClass.Plural     := 'Categories';
   DataSetClass.DataSet    := DataModuleCategories.QCategories;
   DataSetClass.FileName   := NorthwindFolder+'Categories\ORMCategory.pas';
   DataSetClass.WithDetail := False;
   DataSetClass.WithParser := False;
   InsertIntoTreeView(Node, DataSetClass.TableName, DataSetClass);

   DataSetClass := TDataSetClass.Create;
   DataSetClass.TableName  := 'CUSTOMERS';
   DataSetClass.Singular   := 'Customer';
   DataSetClass.Plural     := 'Customers';
   DataSetClass.DataSet    := DataModuleCustomers.QCustomers;
   DataSetClass.FileName   := NorthwindFolder+'Customers\ORMCustomer.pas';
   DataSetClass.WithDetail := False;
   DataSetClass.WithParser := False;
   InsertIntoTreeView(Node, DataSetClass.TableName, DataSetClass);

   DataSetClass := TDataSetClass.Create;
   DataSetClass.TableName  := 'EMPLOYEES';
   DataSetClass.Singular   := 'Employee';
   DataSetClass.Plural     := 'Employees';
   DataSetClass.DataSet    := DataModuleEmployees.QEmployees;
   DataSetClass.FileName   := NorthwindFolder+'Employees\ORMEmployee.pas';
   DataSetClass.WithDetail := False;
   DataSetClass.WithParser := False;
   InsertIntoTreeView(Node, DataSetClass.TableName, DataSetClass);

   DataSetClass := TDataSetClass.Create;
   DataSetClass.TableName  := 'PRODUCTS';
   DataSetClass.Singular   := 'Product';
   DataSetClass.Plural     := 'Products';
   DataSetClass.DataSet    := DataModuleProducts.QProducts;
   DataSetClass.FileName   := NorthwindFolder+'Products\ORMProduct.pas';
   DataSetClass.WithDetail := False;
   DataSetClass.WithParser := False;
   InsertIntoTreeView(Node, DataSetClass.TableName, DataSetClass);

   DataSetClass := TDataSetClass.Create;
   DataSetClass.TableName  := 'SHIPPERS';
   DataSetClass.Singular   := 'Shipper';
   DataSetClass.Plural     := 'Shippers';
   DataSetClass.DataSet    := DataModuleShippers.QShippers;
   DataSetClass.FileName   := NorthwindFolder+'Shippers\ORMShipper.pas';
   DataSetClass.WithDetail := False;
   DataSetClass.WithParser := False;
   InsertIntoTreeView(Node, DataSetClass.TableName, DataSetClass);

   DataSetClass := TDataSetClass.Create;
   DataSetClass.TableName  := 'SUPPLIERS';
   DataSetClass.Singular   := 'Supplier';
   DataSetClass.Plural     := 'Suppliers';
   DataSetClass.DataSet    := DataModuleSuppliers.QSuppliers;
   DataSetClass.FileName   := NorthwindFolder+'Suppliers\ORMSupplier.pas';
   DataSetClass.WithDetail := False;
   DataSetClass.WithParser := False;
   InsertIntoTreeView(Node, DataSetClass.TableName, DataSetClass);

   DataSetClass := TDataSetClass.Create;
   DataSetClass.TableName  := 'ORDERS';
   DataSetClass.Singular   := 'Order';
   DataSetClass.Plural     := 'Orders';
   DataSetClass.DataSet    := DataModuleOrders.QOrders;
   DataSetClass.FileName   := NorthwindFolder+'Orders\ORMOrder.pas';
   DataSetClass.WithDetail := False;
   DataSetClass.WithParser := False;
   InsertIntoTreeView(Node, DataSetClass.TableName, DataSetClass);
end;

function TMainForm.InsertIntoTreeView(RootNode :TTreeViewItem; AText :string; AClass :TDataSetClass):TTreeViewItem;
begin
   TreeViewClasses.BeginUpdate;
   try
      if RootNode <> nil then begin
         Result := TTreeViewItem.Create(RootNode);
         Result.Parent := RootNode;
      end
      else begin
         Result := TTreeViewItem.Create(TreeViewClasses);
         Result.Parent := TreeViewClasses;
      end;

      Result.TagObject := AClass;

      Result.Text := AText;
      Result.StyleLookup := 'treeviewitemstyle';
   finally
      TreeViewClasses.EndUpdate;
   end;
end;


procedure TMainForm.RaiseException(Error: string);
begin
   raise EInvalidOperation.Create(Error);
end;

function TMainForm.RFill(StrData :string; Long :Integer):string;
var Mascara :string;
    i       :Integer;
begin
   Mascara := '';
   for i := 1 to Long do Mascara := Mascara + ' ';

   if Length(Trim(StrData)) < Long then begin
      Result := StrData + Copy(Mascara, 1, Long-Length(StrData));
   end
   else begin
      Result := Copy(StrData, 1, Long);
   end;
end;

function TMainForm.GetMaxLengthFieldName(aDataSet :TDataSet):Integer;
var i :Integer;
begin
   Result := 0;
   for i := 0 to aDataSet.Fields.Count -1 do begin
      Result := Max(Length(aDataSet.Fields[i].FieldName), Result);
   end;
end;

procedure TMainForm.GenerateItemClass5_0(DSClass :TDataSetClass);
var i           :Integer;
    {------------------}
    Singular :string;
    Plural   :string;
    F        :Integer; {MaxLengthFieldName}
    DS       :TDataSet;
    CFN      :string;  {CurrentFieldName}
begin
   Singular := Trim(DSClass.Singular);
   Plural   := Trim(DSClass.Plural);
   F        := GetMaxLengthFieldName(DSClass.DataSet);
   DS       := DSClass.DataSet;

   Memo4_0.Lines.Clear;
   with Memo4_0.Lines do begin
     Add('(***********************************************************)');
     Add('(* Unit generated automaticaly. Modify with care, please!  *)');
     Add('(* (c) 2016 by Juan Carlos Cilleruelo                      *)');
     Add('(* contact with me at juanc.cilleruelo@sencille.es         *)');
     Add('(***********************************************************)');
     Add('unit ORM'+Singular+';');
     Add('');
     Add('interface');
     Add('');
     Add('uses System.SysUtils, System.Generics.Collections, ');
     Add('     Data.DB,                                      ');
     if not (DSClass.WithDetail) or (DSClass.DetailData.UnitName.Trim = '') then begin
        if DSClass.WithParser then begin
           Add('     senCille.ORMClasses,                           ');
           Add('     senCille.MathParser, senCille.MPElements, senCille.MPOperations;');
        end
        else begin
           Add('     senCille.ORMClasses;                           ');
        end;
     end
     else begin
        if DSClass.WithParser then begin
           Add('     senCille.ORMClasses,                           ');
           Add('     senCille.MathParser, senCille.MPElements, senCille.MPOperations,');
           Add('     '+DSClass.DetailData.UnitName+';           ');
        end
        else begin
           Add('     senCille.ORMClasses,                       ');
           Add('     '+DSClass.DetailData.UnitName+';           ');
        end;
     end;

     Add('');
     Add('type');
     Add('  T'+Singular+'Orm = class(TCustomOrm)');
     Add('  private');
     if DSClass.WithParser then begin
        //Add('    [JSONMarshalled(false)]               ');
        Add('    FParser      :TMathParser;            ');
        //Add('    [JSONMarshalled(false)]               ');
        Add('    FCalcPairs   :TObjectList<TCalcPair>; ');
     end;
     //Add('    [JSONMarshalled(false)]                  ');
     Add('    FLineState   :TOrmStatus;                ');

     if DSClass.WithDetail then begin
        Add('    F'+DSClass.DetailData.PropName+' :TObjectList<'+DSClass.DetailData.TypeName+'>;    ');
     end;

     Add('    function GetOrmVersion :string;          ');
     Add('  private'); {This is necessary}
     for i := 0 to DS.Fields.Count - 1 do begin
        (*case DS.Fields[i].DataType of
           ftADT        :Add('    [JSONName('''+RFill(LowerCase(DS.Fields[i].FieldName+''''), F+1)+')]  F'+RFill(DS.Fields[i].FieldName, F+1)+':TMemoField;'         );  // Abstract Data Type field
           ftDate       :Add('    [JSONName('''+RFill(LowerCase(DS.Fields[i].FieldName+''''), F+1)+')]  F'+RFill(DS.Fields[i].FieldName, F+1)+':TDateField;'         );  // Date field
           ftReference  :Add('    [JSONName('''+RFill(LowerCase(DS.Fields[i].FieldName+''''), F+1)+')]  F'+RFill(DS.Fields[i].FieldName, F+1)+':TReferenceField;'    );  // REF field
           ftDateTime   :Add('    [JSONName('''+RFill(LowerCase(DS.Fields[i].FieldName+''''), F+1)+')]  F'+RFill(DS.Fields[i].FieldName, F+1)+':TDateTimeField;'     );  // Date and time field
           ftSmallint   :Add('    [JSONName('''+RFill(LowerCase(DS.Fields[i].FieldName+''''), F+1)+')]  F'+RFill(DS.Fields[i].FieldName, F+1)+':TSmallintField;'     );  // 16-bit integer field
           ftArray      :Add('    [JSONName('''+RFill(LowerCase(DS.Fields[i].FieldName+''''), F+1)+')]  F'+RFill(DS.Fields[i].FieldName, F+1)+':TArrayField;'        );  // Array field
           ftFloat      :Add('    [JSONName('''+RFill(LowerCase(DS.Fields[i].FieldName+''''), F+1)+')]  F'+RFill(DS.Fields[i].FieldName, F+1)+':TFloatField;'        );  // Floating-point numeric field
           ftTimeStamp  :Add('    [JSONName('''+RFill(LowerCase(DS.Fields[i].FieldName+''''), F+1)+')]  F'+RFill(DS.Fields[i].FieldName, F+1)+':TSQLTimeStampField;' );  // Floating-point numeric field
           ftAutoInc    :Add('    [JSONName('''+RFill(LowerCase(DS.Fields[i].FieldName+''''), F+1)+')]  F'+RFill(DS.Fields[i].FieldName, F+1)+':TAutoIncField;'      );  // Auto-incrementing 32-bit integer counter field
           ftString     :Add('    [JSONName('''+RFill(LowerCase(DS.Fields[i].FieldName+''''), F+1)+')]  F'+RFill(DS.Fields[i].FieldName, F+1)+':TStringField;'       );  // Character or string field
           ftFMTBCD     :Add('    [JSONName('''+RFill(LowerCase(DS.Fields[i].FieldName+''''), F+1)+')]  F'+RFill(DS.Fields[i].FieldName, F+1)+':TFMTBCDField;'       );  // FMT Binary-Coded Decimal field
           ftBCD        :Add('    [JSONName('''+RFill(LowerCase(DS.Fields[i].FieldName+''''), F+1)+')]  F'+RFill(DS.Fields[i].FieldName, F+1)+':TBCDField;'          );  // Binary-Coded Decimal field
           ftGraphic    :Add('    [JSONName('''+RFill(LowerCase(DS.Fields[i].FieldName+''''), F+1)+')]  F'+RFill(DS.Fields[i].FieldName, F+1)+':TGraphicField;'      );  // Bitmap field
           ftTime       :Add('    [JSONName('''+RFill(LowerCase(DS.Fields[i].FieldName+''''), F+1)+')]  F'+RFill(DS.Fields[i].FieldName, F+1)+':TTimeField;'         );  // Time field
           ftTypedBinary:Add('    [JSONName('''+RFill(LowerCase(DS.Fields[i].FieldName+''''), F+1)+')]  F'+RFill(DS.Fields[i].FieldName, F+1)+':TBinaryField;'       );  // Typed binary field
           ftVarBytes   :Add('    [JSONName('''+RFill(LowerCase(DS.Fields[i].FieldName+''''), F+1)+')]  F'+RFill(DS.Fields[i].FieldName, F+1)+':TVarBytesField;'     );  // Variable number of bytes (binary storage)
           ftBlob       :Add('    [JSONName('''+RFill(LowerCase(DS.Fields[i].FieldName+''''), F+1)+')]  F'+RFill(DS.Fields[i].FieldName, F+1)+':TBlobField;'         );  // Binary Large OBject field
           //TIDispatchField
           //TTVariantField
           ftBoolean    :Add('    [JSONName('''+RFill(LowerCase(DS.Fields[i].FieldName+''''), F+1)+')]  F'+RFill(DS.Fields[i].FieldName, F+1)+':TBooleanField;'      );  // Boolean field
           ftInteger    :Add('    [JSONName('''+RFill(LowerCase(DS.Fields[i].FieldName+''''), F+1)+')]  F'+RFill(DS.Fields[i].FieldName, F+1)+':TIntegerField;'      );  // 32-bit integer field
           ftWideString :Add('    [JSONName('''+RFill(LowerCase(DS.Fields[i].FieldName+''''), F+1)+')]  F'+RFill(DS.Fields[i].FieldName, F+1)+':TWideStringField;'   );  // Wide string field
           ftBytes      :Add('    [JSONName('''+RFill(LowerCase(DS.Fields[i].FieldName+''''), F+1)+')]  F'+RFill(DS.Fields[i].FieldName, F+1)+':TBytesField;'        );  // Fixed number of bytes (binary storage)
           //TInterfaceField
           ftWord       :Add('    [JSONName('''+RFill(LowerCase(DS.Fields[i].FieldName+''''), F+1)+')]  F'+RFill(DS.Fields[i].FieldName, F+1)+':TWordField;'         );  // 16-bit unsigned integer field
           ftCurrency   :Add('    [JSONName('''+RFill(LowerCase(DS.Fields[i].FieldName+''''), F+1)+')]  F'+RFill(DS.Fields[i].FieldName, F+1)+':TCurrencyField;'     );  // Money field
           ftLargeInt   :Add('    [JSONName('''+RFill(LowerCase(DS.Fields[i].FieldName+''''), F+1)+')]  F'+RFill(DS.Fields[i].FieldName, F+1)+':TLargeintField;'     );  // Large integer field
           ftDataSet    :Add('    [JSONName('''+RFill(LowerCase(DS.Fields[i].FieldName+''''), F+1)+')]  F'+RFill(DS.Fields[i].FieldName, F+1)+':TDataSetField;'      );  // DataSet field
           ftMemo       :Add('    [JSONName('''+RFill(LowerCase(DS.Fields[i].FieldName+''''), F+1)+')]  F'+RFill(DS.Fields[i].FieldName, F+1)+':TMemoField;'         );  // Text memo field

           ftFixedChar  :Add('    [JSONName('''+RFill(LowerCase(DS.Fields[i].FieldName+''''), F+1)+')]  F'+RFill(DS.Fields[i].FieldName, F+1)+':TStringField;' ); // Character or string field of fixed long
           ftWideMemo   :Add('    [JSONName('''+RFill(LowerCase(DS.Fields[i].FieldName+''''), F+1)+')]  F'+RFill(DS.Fields[i].FieldName, F+1)+':TWideMemoField;'); // Wide string field
           ftUnknown    :RaiseException('TDBIntegrity.AddCurrentValue: Field DataType = ftUnknown'); // Unknown or undetermined
           ftFmtMemo    ,                                                           // Formatted text memo field
           ftParadoxOle ,                                                           // Paradox OLE field
           ftDBaseOle   ,                                                           // dBASE OLE field
           ftCursor                                                                // Output cursor from an Oracle stored procedure (TParam only)
                 :RaiseException('TDBIntegrity.AddCurrentValue: Field DataType not Valid for a Primary Key');
           else RaiseException('TDBIntegrity.AddCurrentValue: Field DataType Unknown but not fkUnknown');
        end;*)
        case DS.Fields[i].DataType of
           ftADT        :Add('    F'+RFill(DS.Fields[i].FieldName, F+1)+':TMemoField;'         );  // Abstract Data Type field
           ftDate       :Add('    F'+RFill(DS.Fields[i].FieldName, F+1)+':TDateField;'         );  // Date field
           ftReference  :Add('    F'+RFill(DS.Fields[i].FieldName, F+1)+':TReferenceField;'    );  // REF field
           ftDateTime   :Add('    F'+RFill(DS.Fields[i].FieldName, F+1)+':TDateTimeField;'     );  // Date and time field
           ftSmallint   :Add('    F'+RFill(DS.Fields[i].FieldName, F+1)+':TSmallintField;'     );  // 16-bit integer field
           ftArray      :Add('    F'+RFill(DS.Fields[i].FieldName, F+1)+':TArrayField;'        );  // Array field
           ftFloat      :Add('    F'+RFill(DS.Fields[i].FieldName, F+1)+':TFloatField;'        );  // Floating-point numeric field
           ftTimeStamp  :Add('    F'+RFill(DS.Fields[i].FieldName, F+1)+':TSQLTimeStampField;' );  // Floating-point numeric field
           ftAutoInc    :Add('    F'+RFill(DS.Fields[i].FieldName, F+1)+':TAutoIncField;'      );  // Auto-incrementing 32-bit integer counter field
           ftString     :Add('    F'+RFill(DS.Fields[i].FieldName, F+1)+':TStringField;'       );  // Character or string field
           ftFMTBCD     :Add('    F'+RFill(DS.Fields[i].FieldName, F+1)+':TFMTBCDField;'       );  // FMT Binary-Coded Decimal field
           ftBCD        :Add('    F'+RFill(DS.Fields[i].FieldName, F+1)+':TBCDField;'          );  // Binary-Coded Decimal field
           ftGraphic    :Add('    F'+RFill(DS.Fields[i].FieldName, F+1)+':TGraphicField;'      );  // Bitmap field
           ftTime       :Add('    F'+RFill(DS.Fields[i].FieldName, F+1)+':TTimeField;'         );  // Time field
           ftTypedBinary:Add('    F'+RFill(DS.Fields[i].FieldName, F+1)+':TBinaryField;'       );  // Typed binary field
           ftVarBytes   :Add('    F'+RFill(DS.Fields[i].FieldName, F+1)+':TVarBytesField;'     );  // Variable number of bytes (binary storage)
           ftBlob       :Add('    F'+RFill(DS.Fields[i].FieldName, F+1)+':TBlobField;'         );  // Binary Large OBject field
           //TIDispatchField
           //TTVariantField
           ftBoolean    :Add('    F'+RFill(DS.Fields[i].FieldName, F+1)+':TBooleanField;'      );  // Boolean field
           ftInteger    :Add('    F'+RFill(DS.Fields[i].FieldName, F+1)+':TIntegerField;'      );  // 32-bit integer field
           ftWideString :Add('    F'+RFill(DS.Fields[i].FieldName, F+1)+':TWideStringField;'   );  // Wide string field
           ftBytes      :Add('    F'+RFill(DS.Fields[i].FieldName, F+1)+':TBytesField;'        );  // Fixed number of bytes (binary storage)
           //TInterfaceField
           ftWord       :Add('    F'+RFill(DS.Fields[i].FieldName, F+1)+':TWordField;'         );  // 16-bit unsigned integer field
           ftCurrency   :Add('    F'+RFill(DS.Fields[i].FieldName, F+1)+':TCurrencyField;'     );  // Money field
           ftLargeInt   :Add('    F'+RFill(DS.Fields[i].FieldName, F+1)+':TLargeintField;'     );  // Large integer field
           ftDataSet    :Add('    F'+RFill(DS.Fields[i].FieldName, F+1)+':TDataSetField;'      );  // DataSet field
           ftMemo       :Add('    F'+RFill(DS.Fields[i].FieldName, F+1)+':TMemoField;'         );  // Text memo field

           ftFixedChar  :Add('    F'+RFill(DS.Fields[i].FieldName, F+1)+':TStringField;' ); // Character or string field of fixed long
           ftWideMemo   :Add('    F'+RFill(DS.Fields[i].FieldName, F+1)+':TWideMemoField;'); // Wide string field
           ftUnknown    :RaiseException('TDBIntegrity.AddCurrentValue: Field DataType = ftUnknown'); // Unknown or undetermined
           ftFmtMemo    ,                                                           // Formatted text memo field
           ftParadoxOle ,                                                           // Paradox OLE field
           ftDBaseOle   ,                                                           // dBASE OLE field
           ftCursor                                                                // Output cursor from an Oracle stored procedure (TParam only)
                 :RaiseException('TDBIntegrity.AddCurrentValue: Field DataType not Valid for a Primary Key');
           else RaiseException('TDBIntegrity.AddCurrentValue: Field DataType Unknown but not fkUnknown');
        end;
     end;
     Add('  public');
     Add('    destructor Destroy;   override;');
     Add('    procedure Initialize; override;');
     Add('    procedure Clear;               ');
     Add('    function  Clone:T'+Singular+'Orm;');
     if DSClass.WithParser then begin
         Add('    function  Evaluate:Boolean;                    ');
         Add('    function  EvaluateField(Field :TField):TField; ');
         Add('    procedure InsertVariablesFrom(AOrm :TCustomOrm);    ');
         Add('    {--- This methods are replics of the correspondant of the Embeded Parser ---} ');
         Add('    procedure SetVar(prmID :string; prmValue :Int64   ); overload;                ');
         Add('    procedure SetVar(prmID :string; prmValue :Extended); overload;                ');
         Add('    procedure SetVar(prmID :string; prmValue :string  ); overload;                ');
         Add('    procedure SetVar(prmID :string; prmValue :Boolean ); overload;                ');
         Add('    {---------------------------------------------------------------------------} ');
     end;
     Add('    procedure Assign(A'+Singular+' :T'+Singular+'Orm);                  ');
     Add('    property OrmVersion  :string     read GetOrmVersion;                ');
     Add('    property LineState   :TOrmStatus read FLineState  write FLineState; ');

     if DSClass.WithDetail then begin
        Add('    property '+DSClass.DetailData.PropName+' :TObjectList<'+DSClass.DetailData.TypeName+'> read F'+DSClass.DetailData.PropName+' write F'+DSClass.DetailData.PropName+';    ');
     end;
     if DSClass.WithParser then begin
        //Add('    property CalcPairs :TObjectList<TCalcPair> read FCalcPairs write FCalcPairs; ');
     end;

     Add('  published');
     for i := 0 to DS.Fields.Count - 1 do begin
        CFN := DS.Fields[i].FieldName;
        case DS.Fields[i].DataType of
           ftADT        :Add('    property '+RFill(CFN, F)+' :TMemoField         read F'+RFill(CFN, F)+' write F'+RFill(CFN, F)+';');
           ftDate       :Add('    property '+RFill(CFN, F)+' :TDateField         read F'+RFill(CFN, F)+' write F'+RFill(CFN, F)+';');
           ftReference  :Add('    property '+RFill(CFN, F)+' :TReferenceField    read F'+RFill(CFN, F)+' write F'+RFill(CFN, F)+';');
           ftDateTime   :Add('    property '+RFill(CFN, F)+' :TDateTimeField     read F'+RFill(CFN, F)+' write F'+RFill(CFN, F)+';');
           ftSmallint   :Add('    property '+RFill(CFN, F)+' :TSmallintField     read F'+RFill(CFN, F)+' write F'+RFill(CFN, F)+';');
           ftArray      :Add('    property '+RFill(CFN, F)+' :TArrayField        read F'+RFill(CFN, F)+' write F'+RFill(CFN, F)+';');
           ftFloat      :Add('    property '+RFill(CFN, F)+' :TFloatField        read F'+RFill(CFN, F)+' write F'+RFill(CFN, F)+';');
           ftTimeStamp  :Add('    property '+RFill(CFN, F)+' :TSQLTimeStampField read F'+RFill(CFN, F)+' write F'+RFill(CFN, F)+';');
           ftAutoInc    :Add('    property '+RFill(CFN, F)+' :TAutoIncField      read F'+RFill(CFN, F)+' write F'+RFill(CFN, F)+';');
           ftString     :Add('    property '+RFill(CFN, F)+' :TStringField       read F'+RFill(CFN, F)+' write F'+RFill(CFN, F)+';');
           ftFMTBCD     :Add('    property '+RFill(CFN, F)+' :TFMTBCDField       read F'+RFill(CFN, F)+' write F'+RFill(CFN, F)+';');
           ftBCD        :Add('    property '+RFill(CFN, F)+' :TBCDField          read F'+RFill(CFN, F)+' write F'+RFill(CFN, F)+';');
           ftGraphic    :Add('    property '+RFill(CFN, F)+' :TGraphicField      read F'+RFill(CFN, F)+' write F'+RFill(CFN, F)+';');
           ftTime       :Add('    property '+RFill(CFN, F)+' :TTimeField         read F'+RFill(CFN, F)+' write F'+RFill(CFN, F)+';');
           ftTypedBinary:Add('    property '+RFill(CFN, F)+' :TBinaryField       read F'+RFill(CFN, F)+' write F'+RFill(CFN, F)+';');
           ftVarBytes   :Add('    property '+RFill(CFN, F)+' :TVarBytesField     read F'+RFill(CFN, F)+' write F'+RFill(CFN, F)+';');
           ftBlob       :Add('    property '+RFill(CFN, F)+' :TBlobField         read F'+RFill(CFN, F)+' write F'+RFill(CFN, F)+';');
           //TIDispatchField
           //TTVariantField
           ftBoolean    :Add('    property '+RFill(CFN, F)+' :TBooleanField      read F'+RFill(CFN, F)+' write F'+RFill(CFN, F)+';');
           ftInteger    :Add('    property '+RFill(CFN, F)+' :TIntegerField      read F'+RFill(CFN, F)+' write F'+RFill(CFN, F)+';');
           ftWideString :Add('    property '+RFill(CFN, F)+' :TWideStringField   read F'+RFill(CFN, F)+' write F'+RFill(CFN, F)+';');
           ftBytes      :Add('    property '+RFill(CFN, F)+' :TBytesField        read F'+RFill(CFN, F)+' write F'+RFill(CFN, F)+';');
           //TInterfaceField
           ftWord       :Add('    property '+RFill(CFN, F)+' :TWordField         read F'+RFill(CFN, F)+' write F'+RFill(CFN, F)+';');
           ftCurrency   :Add('    property '+RFill(CFN, F)+' :TCurrencyField     read F'+RFill(CFN, F)+' write F'+RFill(CFN, F)+';');
           ftLargeInt   :Add('    property '+RFill(CFN, F)+' :TLargeIntField     read F'+RFill(CFN, F)+' write F'+RFill(CFN, F)+';');
           ftDataSet    :Add('    property '+RFill(CFN, F)+' :TDataSetField      read F'+RFill(CFN, F)+' write F'+RFill(CFN, F)+';');
           ftMemo       :Add('    property '+RFill(CFN, F)+' :TMemoField         read F'+RFill(CFN, F)+' write F'+RFill(CFN, F)+';');

           ftFixedChar  :Add('    property '+RFill(CFN, F)+' :TStringField       read F'+RFill(CFN, F)+' write F'+RFill(CFN, F)+';');
           ftWideMemo   :Add('    property '+RFill(CFN, F)+' :TWideMemoField     read F'+RFill(CFN, F)+' write F'+RFill(CFN, F)+';');
           ftUnknown    :RaiseException('TDBIntegrity.AddCurrentValue: Field DataType = ftUnknown'); // Unknown or undetermined
           ftFmtMemo    ,                                                           // Formatted text memo field
           ftParadoxOle ,                                                           // Paradox OLE field
           ftDBaseOle   ,                                                           // dBASE OLE field
           ftCursor                                                                // Output cursor from an Oracle stored procedure (TParam only)
                 :RaiseException('TDBIntegrity.AddCurrentValue: Field DataType not Valid for a Primary Key');
           else RaiseException('TDBIntegrity.AddCurrentValue: Field DataType Unknown but not fkUnknown');
        end;
     end;
     Add('  end;');
     {---- End of Interface ----}
     Add('');
     Add('implementation');
     Add('uses TypInfo;');
     Add('');
     Add('{ T'+Singular+'Orm }');
     //Add('constructor T'+Singular+'Orm.Create;');
     //Add('begin');
     //Add('   inherited; ');
     //Add('   Initialize;');
     //Add('end;');
     Add('');
     Add('destructor T'+Singular+'Orm.Destroy;');
     Add('begin');

     if DSClass.WithDetail then begin
        Add('   FreeAndNil(F'+DSClass.DetailData.PropName+'); ');
     end;

     if DSClass.WithParser then begin
        Add('   FParser.Free;     ');
        Add('   FCalcPairs.Free;  ');
     end;

     Add('   inherited;');
     Add('end;');
     Add('');
     Add('procedure T'+Singular+'Orm.Initialize;');
     if DSClass.WithParser then begin
        Add('var Field       :TField;   ');
        Add('    Name        :string;   ');
        Add('    TargetField :TField;   ');
     end;
     Add('begin');

     if DSClass.WithDetail then begin
        Add('   F'+DSClass.DetailData.PropName+' := TObjectList<'+DSClass.DetailData.TypeName+'>.Create;  ');
        Add('   F'+DSClass.DetailData.PropName+'.OwnsObjects := True;               ');
        Add('');
     end;

     for i := 0 to DS.Fields.Count - 1 do begin
        CFN := DS.Fields[i].FieldName;
        case DS.Fields[i].DataType of
           ftADT        :begin
              Add('   F'+CFN+' := TADTField.Create(FDataSet);');
              Add('   F'+CFN+'.FieldName     := '''+CFN+''';');
              Add('   F'+CFN+'.FieldKind     := fkData;');
              Add('   F'+CFN+'.DataSet       := FDataSet;');
              Add('   F'+CFN+'.Size          := '+IntToStr(DS.Fields[i].Size)+';');
              {Required Field}
              if DS.Fields[i].Required then Add('   F'+CFN+'.Required      := True;');
           end;
           ftDate       :begin
              Add('   F'+CFN+' := TDateField.Create(FDataSet);');
              Add('   F'+CFN+'.FieldName     := '''+CFN+''';');
              Add('   F'+CFN+'.FieldKind     := fkData;');
              Add('   F'+CFN+'.DataSet       := FDataSet;');
              Add('   F'+CFN+'.Size          := '+IntToStr(DS.Fields[i].Size)+';');
              {Required Field}
              if DS.Fields[i].Required then Add('   F'+CFN+'.Required      := True;');
              Add('   F'+CFN+'.DisplayFormat := ''dd/mm/yyyy'';');
           end;
           ftReference  :begin
              Add('   F'+CFN+' := TReferenceField.Create(FDataSet);');
              Add('   F'+CFN+'.FieldName     := '''+CFN+''';');
              Add('   F'+CFN+'.FieldKind     := fkData;');
              Add('   F'+CFN+'.DataSet       := FDataSet;');
              Add('   F'+CFN+'.Size          := '+IntToStr(DS.Fields[i].Size)+';');
              {Required Field}
              if DS.Fields[i].Required then Add('   F'+CFN+'.Required      := True;');
           end;
           ftDateTime   :begin
              Add('   F'+CFN+' := TDateTimeField.Create(FDataSet);');
              Add('   F'+CFN+'.FieldName     := '''+CFN+''';');
              Add('   F'+CFN+'.FieldKind     := fkData;');
              Add('   F'+CFN+'.DataSet       := FDataSet;');
              Add('   F'+CFN+'.Size          := '+IntToStr(DS.Fields[i].Size)+';');
              {Required Field}
              if DS.Fields[i].Required then Add('   F'+CFN+'.Required      := True;');
              Add('   F'+CFN+'.DisplayFormat := ''dd/mm/yyyy'';');
           end;
           ftSmallint   :begin
              Add('   F'+CFN+' := TSmallintField.Create(FDataSet);');
              Add('   F'+CFN+'.FieldName     := '''+CFN+''';');
              Add('   F'+CFN+'.FieldKind     := fkData;');
              Add('   F'+CFN+'.DataSet       := FDataSet;');
              Add('   F'+CFN+'.Size          := '+IntToStr(DS.Fields[i].Size)+';');
              {Required Field}
              if DS.Fields[i].Required then Add('   F'+CFN+'.Required      := True;');
              Add('   F'+CFN+'.DisplayFormat := ''###,##0'';');
           end;
           ftArray      :begin
              Add('   F'+CFN+' := TArrayField.Create(FDataSet);');
              Add('   F'+CFN+'.FieldName     := '''+CFN+''';');
              Add('   F'+CFN+'.FieldKind     := fkData;');
              Add('   F'+CFN+'.DataSet       := FDataSet;');
              Add('   F'+CFN+'.Size          := '+IntToStr(DS.Fields[i].Size)+';');
              {Required Field}
              if DS.Fields[i].Required then Add('   F'+CFN+'.Required      := True;');
               end;
           ftFloat      :begin
              Add('   F'+CFN+' := TFloatField.Create(FDataSet);');
              Add('   F'+CFN+'.FieldName     := '''+CFN+''';');
              Add('   F'+CFN+'.FieldKind     := fkData;');
              Add('   F'+CFN+'.DataSet       := FDataSet;');
              Add('   F'+CFN+'.Size          := '+IntToStr(DS.Fields[i].Size)+';');
              {Required Field}
              if DS.Fields[i].Required then Add('   F'+CFN+'.Required      := True;');
              Add('   F'+CFN+'.DisplayFormat := ''###,##0.00'';');
           end;
           ftTimeStamp  :begin
              Add('   F'+CFN+' := TSQLTimeStampField.Create(FDataSet);');
              Add('   F'+CFN+'.FieldName     := '''+CFN+''';');
              Add('   F'+CFN+'.FieldKind     := fkData;');
              Add('   F'+CFN+'.DataSet       := FDataSet;');
              Add('   F'+CFN+'.Size          := '+IntToStr(DS.Fields[i].Size)+';');
              {Required Field}
              if DS.Fields[i].Required then Add('   F'+CFN+'.Required      := True;');
              Add('   F'+CFN+'.DisplayFormat := ''dd/mm/yyyy'';');
           end;
           ftAutoInc    :begin
              Add('   F'+CFN+' := TAutoIncField.Create(FDataSet);');
              Add('   F'+CFN+'.FieldName     := '''+CFN+''';');
              Add('   F'+CFN+'.FieldKind     := fkData;');
              Add('   F'+CFN+'.DataSet       := FDataSet;');
              Add('   F'+CFN+'.Size          := '+IntToStr(DS.Fields[i].Size)+';');
              {Required Field}
              if DS.Fields[i].Required then Add('   F'+CFN+'.Required      := True;');
              Add('   F'+CFN+'.DisplayFormat := ''###,##0'';');
           end;
           ftString     :begin
              Add('   F'+CFN+' := TStringField.Create(FDataSet);');
              Add('   F'+CFN+'.FieldName     := '''+CFN+''';');
              Add('   F'+CFN+'.FieldKind     := fkData;');
              Add('   F'+CFN+'.DataSet       := FDataSet;');
              Add('   F'+CFN+'.Size          := '+IntToStr(DS.Fields[i].Size)+';');
              {Required Field}
              if DS.Fields[i].Required then Add('   F'+CFN+'.Required      := True;');
           end;
           ftFMTBCD     :begin
              Add('   F'+CFN+' := TFMTBCDField.Create(FDataSet);');
              Add('   F'+CFN+'.FieldName     := '''+CFN+''';');
              Add('   F'+CFN+'.FieldKind     := fkData;');
              Add('   F'+CFN+'.DataSet       := FDataSet;');
              Add('   F'+CFN+'.Size          := '+IntToStr(DS.Fields[i].Size)+';');
              {Required Field}
              if DS.Fields[i].Required then Add('   F'+CFN+'.Required      := True;');
              Add('   F'+CFN+'.DisplayFormat := ''###,##0.00'';');
           end;
           ftBCD        :begin
              Add('   F'+CFN+' := TBCDField.Create(FDataSet);');
              Add('   F'+CFN+'.FieldName     := '''+CFN+''';');
              Add('   F'+CFN+'.FieldKind     := fkData;');
              Add('   F'+CFN+'.DataSet       := FDataSet;');
              Add('   F'+CFN+'.Size          := '+IntToStr(DS.Fields[i].Size)+';');
              {Required Field}
              if DS.Fields[i].Required then Add('   F'+CFN+'.Required      := True;');
              Add('   F'+CFN+'.DisplayFormat := ''###,##0.00'';');
           end;
           ftGraphic    :begin
              Add('   F'+CFN+' := TGraphicField.Create(FDataSet);');
              Add('   F'+CFN+'.FieldName     := '''+CFN+''';');
              Add('   F'+CFN+'.FieldKind     := fkData;');
              Add('   F'+CFN+'.DataSet       := FDataSet;');
              Add('   F'+CFN+'.Size          := '+IntToStr(DS.Fields[i].Size)+';');
              {Required Field}
              if DS.Fields[i].Required then Add('   F'+CFN+'.Required      := True;');
           end;
           ftTime       :begin
              Add('   F'+CFN+' := TTimeField.Create(FDataSet);');
              Add('   F'+CFN+'.FieldName     := '''+CFN+''';');
              Add('   F'+CFN+'.FieldKind     := fkData;');
              Add('   F'+CFN+'.DataSet       := FDataSet;');
              Add('   F'+CFN+'.Size          := '+IntToStr(DS.Fields[i].Size)+';');
              {Required Field}
              if DS.Fields[i].Required then Add('   F'+CFN+'.Required      := True;');
              Add('   F'+CFN+'.DisplayFormat := ''hh:ss'';');
           end;
           ftTypedBinary:begin
              Add('   F'+CFN+' := TBinaryField.Create(FDataSet);');
              Add('   F'+CFN+'.FieldName     := '''+CFN+''';');
              Add('   F'+CFN+'.FieldKind     := fkData;');
              Add('   F'+CFN+'.DataSet       := FDataSet;');
              Add('   F'+CFN+'.Size          := '+IntToStr(DS.Fields[i].Size)+';');
              {Required Field}
              if DS.Fields[i].Required then Add('   F'+CFN+'.Required      := True;');
           end;
           //TBCDField
           ftVarBytes   :begin
              Add('   F'+CFN+' := TVarBytesField.Create(FDataSet);');
              Add('   F'+CFN+'.FieldName     := '''+CFN+''';');
              Add('   F'+CFN+'.FieldKind     := fkData;');
              Add('   F'+CFN+'.DataSet       := FDataSet;');
              Add('   F'+CFN+'.Size          := '+IntToStr(DS.Fields[i].Size)+';');
              {Required Field}
              if DS.Fields[i].Required then Add('   F'+CFN+'.Required      := True;');
           end;
           ftBlob       :begin
              Add('   F'+CFN+' := TBlobField.Create(FDataSet);');
              Add('   F'+CFN+'.FieldName     := '''+CFN+''';');
              Add('   F'+CFN+'.FieldKind     := fkData;');
              Add('   F'+CFN+'.BlobType      := ftBlob;'); {$Message Warn 'Esto hay que revisarlo'}
              Add('   F'+CFN+'.DataSet       := FDataSet;');
              Add('   F'+CFN+'.Size          := '+IntToStr(DS.Fields[i].Size)+';');
              {Required Field}
              if DS.Fields[i].Required then Add('   F'+CFN+'.Required      := True;');
           end;
           //TIDispatchField
           //TTVariantField
           ftBoolean    :begin
              Add('   F'+CFN+' := TBooleanField.Create(FDataSet);');
              Add('   F'+CFN+'.FieldName     := '''+CFN+''';');
              Add('   F'+CFN+'.FieldKind     := fkData;');
              Add('   F'+CFN+'.DataSet       := FDataSet;');
              Add('   F'+CFN+'.Size          := '+IntToStr(DS.Fields[i].Size)+';');
              {Required Field}
              if DS.Fields[i].Required then Add('   F'+CFN+'.Required      := True;');
           end;
           ftInteger    :begin
              Add('   F'+CFN+' := TIntegerField.Create(FDataSet);');
              Add('   F'+CFN+'.FieldName     := '''+CFN+''';');
              Add('   F'+CFN+'.FieldKind     := fkData;');
              Add('   F'+CFN+'.DataSet       := FDataSet;');
              Add('   F'+CFN+'.Size          := '+IntToStr(DS.Fields[i].Size)+';');
              {Required Field}
              if DS.Fields[i].Required then Add('   F'+CFN+'.Required      := True;');
              Add('   F'+CFN+'.DisplayFormat := ''###,##0'';');
           end;
           ftWideString :begin
              Add('   F'+CFN+' := TWideStringField.Create(FDataSet);');
              Add('   F'+CFN+'.FieldName     := '''+CFN+''';');
              Add('   F'+CFN+'.FieldKind     := fkData;');
              Add('   F'+CFN+'.DataSet       := FDataSet;');
              Add('   F'+CFN+'.Size          := '+IntToStr(DS.Fields[i].Size)+';');
              {Required Field}
              if DS.Fields[i].Required then Add('   F'+CFN+'.Required      := True;');
           end;
           ftBytes      :begin
              Add('   F'+CFN+' := TBytesField.Create(FDataSet);');
              Add('   F'+CFN+'.FieldName     := '''+CFN+''';');
              Add('   F'+CFN+'.FieldKind     := fkData;');
              Add('   F'+CFN+'.DataSet       := FDataSet;');
              Add('   F'+CFN+'.Size          := '+IntToStr(DS.Fields[i].Size)+';');
              {Required Field}
              if DS.Fields[i].Required then Add('   F'+CFN+'.Required      := True;');
           end;
           //TInterfaceField
           ftWord       :begin
              Add('   F'+CFN+' := TWordField.Create(FDataSet);');
              Add('   F'+CFN+'.FieldName     := '''+CFN+''';');
              Add('   F'+CFN+'.FieldKind     := fkData;');
              Add('   F'+CFN+'.DataSet       := FDataSet;');
              Add('   F'+CFN+'.Size          := '+IntToStr(DS.Fields[i].Size)+';');
              {Required Field}
              if DS.Fields[i].Required then Add('   F'+CFN+'.Required      := True;');
              Add('   F'+CFN+'.DisplayFormat := ''###,##0'';');
           end;
           ftCurrency   :begin
              Add('   F'+CFN+' := TCurrencyField.Create(FDataSet);');
              Add('   F'+CFN+'.FieldName     := '''+CFN+''';');
              Add('   F'+CFN+'.FieldKind     := fkData;');
              Add('   F'+CFN+'.DataSet       := FDataSet;');
              Add('   F'+CFN+'.Size          := '+IntToStr(DS.Fields[i].Size)+';');
              {Required Field}
              if DS.Fields[i].Required then Add('   F'+CFN+'.Required      := True;');
              Add('   F'+CFN+'.DisplayFormat := ''###,##0.00'';');
           end;
           ftLargeInt   :begin
              Add('   F'+CFN+' := TLargeIntField.Create(FDataSet);');
              Add('   F'+CFN+'.FieldName     := '''+CFN+''';');
              Add('   F'+CFN+'.FieldKind     := fkData;');
              Add('   F'+CFN+'.DataSet       := FDataSet;');
              Add('   F'+CFN+'.Size          := '+IntToStr(DS.Fields[i].Size)+';');
              {Required Field}
              if DS.Fields[i].Required then Add('   F'+CFN+'.Required      := True;');
              Add('   F'+CFN+'.DisplayFormat := ''###,##0'';');
           end;
           ftDataSet    :begin
              Add('   F'+CFN+' := TDataSetField.Create(FDataSet);');
              Add('   F'+CFN+'.FieldName     := '''+CFN+''';');
              Add('   F'+CFN+'.FieldKind     := fkData;');
              Add('   F'+CFN+'.DataSet       := FDataSet;');
              Add('   F'+CFN+'.Size          := '+IntToStr(DS.Fields[i].Size)+';');
              {Required Field}
              if DS.Fields[i].Required then Add('   F'+CFN+'.Required      := True;');
           end;
           ftMemo       :begin
              Add('   F'+CFN+' := TMemoField.Create(FDataSet);');
              Add('   F'+CFN+'.FieldName     := '''+CFN+''';');
              Add('   F'+CFN+'.FieldKind     := fkData;');
              Add('   F'+CFN+'.BlobType      := ftMemo;'); {$Message Warn 'Esto hay que revisarlo'}
              Add('   F'+CFN+'.DataSet       := FDataSet;');
              Add('   F'+CFN+'.Size          := '+IntToStr(DS.Fields[i].Size)+';');
              {Required Field}
              if DS.Fields[i].Required then Add('   F'+CFN+'.Required      := True;');
           end;
           ftFixedChar  :begin
              Add('   F'+CFN+' := TFixedCharField.Create(FDataSet);');
              Add('   F'+CFN+'.FieldName     := '''+CFN+''';');
              Add('   F'+CFN+'.FieldKind     := fkData;');

              Add('   F'+CFN+'.DataSet       := FDataSet;');
              Add('   F'+CFN+'.Size          := '+IntToStr(DS.Fields[i].Size)+';');
              {Required Field}
              if DS.Fields[i].Required then Add('   F'+CFN+'.Required      := True;');
           end;
           ftWideMemo   :begin
              Add('   F'+CFN+' := TWideMemoField.Create(FDataSet);');
              Add('   F'+CFN+'.FieldName     := '''+CFN+''';');
              Add('   F'+CFN+'.FieldKind     := fkData;');
              Add('   F'+CFN+'.BlobType      := ftMemo;'); {$Message Warn 'Esto hay que revisarlo'}
              Add('   F'+CFN+'.DataSet       := FDataSet;');
              Add('   F'+CFN+'.Size          := '+IntToStr(DS.Fields[i].Size)+';');
              {Required Field}
              if DS.Fields[i].Required then Add('   F'+CFN+'.Required      := True;');
           end;
           ftUnknown    :RaiseException('TDBIntegrity.AddCurrentValue: Field DataType = ftUnknown'); // Unknown or undetermined
           ftFmtMemo    ,                                                           // Formatted text memo field
           ftParadoxOle ,                                                           // Paradox OLE field
           ftDBaseOle   ,                                                           // dBASE OLE field
           ftCursor                                                                 // Output cursor from an Oracle stored procedure (TParam only)
                 :RaiseException('TDBIntegrity.AddCurrentValue: Field DataType not Valid for a Primary Key');
           else RaiseException('TDBIntegrity.AddCurrentValue: Field DataType Unknown but not fkUnknown');
        end;
        Add('   Fields.Add('''+CFN+''''+', F'+CFN+');');
        Add('');
     end;

     if DSClass.WithParser then begin
        Add('   FParser := TMathParser.Create(nil);                             ');
        Add('   FCalcPairs := TObjectList<TCalcPair>.Create;                    ');
        Add('   FCalcPairs.OwnsObjects := False;                                ');
        Add('                                                                   ');
        Add('   for Field in FDataSet.Fields do begin                           ');
        Add('       Name := Field.FieldName;                                    ');
        Add('       if Name.StartsWith(''FORMULA_'') then begin                 ');
        Add('          TargetField := FDataSet.FindField(Name.SubString(8));    ');
        Add('          if not (TargetField = nil) then begin                    ');
        Add('             FCalcPairs.Add(TCalcPair.Create);                     ');
        Add('             FCalcPairs.Last.Formula := Field;                     ');
        Add('             FCalcPairs.Last.Result  := TargetField;               ');
        Add('          end;                                                     ');
        Add('       end;                                                        ');
        Add('   end;                                                            ');
     end;

     Add('   FDataSet.CreateDataSet;   ');
     Add('end;');
     Add('');
     Add('procedure T'+Singular+'Orm.Clear;                                   ');
     Add('var Field :TField;                                                  ');
     Add('begin                                                               ');
     Add('   for Field in Fields.Values do begin                              ');
     Add('      Field.Clear;                                                  ');
     Add('   end;                                                             ');
     Add('end;                                                                ');
     Add('');
     Add('function T'+Singular+'Orm.Clone: T'+Singular+'Orm;');
     Add('var Field   :TField;        ');
     Add('    MyField :TField;        ');
     if DSClass.WithDetail then begin
        Add('    My'+DSClass.DetailData.MemberName+' :'+DSClass.DetailData.TypeName+';    ');
     end;
     Add('begin');
     Add('    Result := T'+Singular+'Orm.Create(DisplayFormats);           ');
     Add('    Result.Edit;                                                  ');
     Add('    for Field in Fields.Values do begin                           ');
     Add('       if not Field.IsNull then begin                             ');
     Add('          Result.Fields.TryGetValue(Field.FieldName, MyField);    ');
     Add('          MyField.Assign(Field);                                  ');
     Add('       end;                                                       ');
     Add('    end;                                                          ');
     Add('                                                                  ');
     if DSClass.WithDetail then begin
        Add('    for My'+DSClass.DetailData.MemberName+' in '+DSClass.DetailData.PropName+' do begin        ');
        Add('       Result.'+DSClass.DetailData.PropName+'.Add(My'+DSClass.DetailData.MemberName+'.Clone);  ');
        Add('    end;                                                                                       ');
     end;
     Add('                                                                  ');
     Add('    Result.Post;                                                  ');
     Add('end;');
     Add('');
     Add('procedure T'+Singular+'Orm.Assign(A'+Singular+' :T'+Singular+'Orm);    ');
     Add('var Field   :TField;                                                   ');
     Add('    MyField :TField;                                                   ');
     Add('begin                                                                  ');
     Add('   Edit;                                                               ');
     Add('   for Field in A'+Singular+'.Fields.Values do begin                   ');
     Add('      if not Field.IsNull then begin                                   ');
     Add('         Fields.TryGetValue(Field.FieldName, MyField);                 ');
     Add('         MyField.Assign(Field);                                        ');
     Add('      end;                                                             ');
     Add('   end;                                                                ');
     Add('   Post;                                                               ');
     Add('end;');
     Add('');
     if DSClass.WithParser then begin
        Add('procedure T'+Singular+'Orm.SetVar(prmID :string; prmValue :Int64);     ');
        Add('begin                                                                  ');
        Add('   FParser.SetVar(prmID, prmValue);                                    ');
        Add('end;                                                                   ');
        Add('                                                                       ');
        Add('procedure T'+Singular+'Orm.SetVar(prmID :string; prmValue :Extended);  ');
        Add('begin                                                                  ');
        Add('   FParser.SetVar(prmID, prmValue);                                    ');
        Add('end;                                                                   ');
        Add('                                                                       ');
        Add('procedure T'+Singular+'Orm.SetVar(prmID :string; prmValue :Boolean);   ');
        Add('begin                                                                  ');
        Add('   FParser.SetVar(prmID, prmValue);                                    ');
        Add('end;                                                                   ');
        Add('                                                                       ');
        Add('procedure T'+Singular+'Orm.SetVar(prmID :string; prmValue :string);    ');
        Add('begin                                                                  ');
        Add('   FParser.SetVar(prmID, prmValue);                                    ');
        Add('end;                                                                   ');
        Add('                                                                       ');
        Add('procedure T'+Singular+'Orm.InsertVariablesFrom(AOrm :TCustomOrm);                                                                                              ');
        Add('var locField :TField;                                                                                                                                          ');
        Add('begin                                                                                                                                                          ');
        Add('   for locField in AOrm.Fields.Values do begin                                                                                                                 ');
        Add('      case locField.DataType of                                                                                                                                ');
        Add('         ftSmallint   :FParser.SetVar(locField.FieldName, locField.AsInteger);  // :TSmallintField;''     ); // 16-bit integer field                           ');
        Add('         ftFloat      :FParser.SetVar(locField.FieldName, locField.AsFloat);    // :TFloatField;''        ); // Floating-point numeric field                   ');
        Add('         ftString     :FParser.SetVar(locField.FieldName, locField.AsString);   // :TStringField;''       ); // Character or string field                      ');
        Add('         ftFMTBCD     :FParser.SetVar(locField.FieldName, locField.AsExtended); // :TFMTBCDField;''       ); // FMT Binary-Coded Decimal field                 ');
        Add('         ftBCD        :FParser.SetVar(locField.FieldName, locField.AsExtended); // :TBCDField;''          ); // Binary-Coded Decimal field                     ');
        Add('         ftBoolean    :begin // Boolean field                                                                                                                  ');
        Add('            if locField.AsBoolean then                                                                                                                         ');
        Add('               FParser.SetVar(locField.FieldName, ''Y'')                                                                                                       ');
        Add('            else                                                                                                                                               ');
        Add('               FParser.SetVar(locField.FieldName, ''N'');                                                                                                      ');
        Add('         end;                                                                                                                                                  ');
        Add('         ftInteger    :FParser.SetVar(locField.FieldName, locField.AsInteger);  // :TIntegerField;''      ); // 32-bit integer field                           ');
        Add('         ftWideString :FParser.SetVar(locField.FieldName, locField.AsString );  // :TWideStringField;''   ); // Wide string field                              ');
        Add('         ftWord       :FParser.SetVar(locField.FieldName, locField.AsInteger);  // :TWordField;''         ); // 16-bit unsigned integer field                  ');
        Add('         ftCurrency   :FParser.SetVar(locField.FieldName, locField.AsCurrency); // :TCurrencyField;''     ); // Money field                                    ');
        Add('         ftLargeInt   :FParser.SetVar(locField.FieldName, locField.AsInteger);  // :TLargeintField;''     ); // Large integer field                            ');
        Add('         ftFixedChar  :FParser.SetVar(locField.FieldName, locField.AsString);   // :TStringField;''       ); // Character or string field of fixed long        ');
        Add('         //ftADT        :Add(''    F''+RFill(DS.Fields[i].FieldName, F+1)+''     :TMemoField;''         );  // Abstract Data Type field                        ');
        Add('         //ftDate       :Add(''    F''+RFill(DS.Fields[i].FieldName, F+1)+''     :TDateField;''         );  // Date field                                      ');
        Add('         //ftReference  :Add(''    F''+RFill(DS.Fields[i].FieldName, F+1)+''     :TReferenceField;''    );  // REF field                                       ');
        Add('         //ftDateTime   :Add(''    F''+RFill(DS.Fields[i].FieldName, F+1)+''     :TDateTimeField;''     );  // Date and time field                             ');
        Add('         //ftArray      :Add(''    F''+RFill(DS.Fields[i].FieldName, F+1)+''     :TArrayField;''        );  // Array field                                     ');
        Add('         //ftTimeStamp  :Add(''    F''+RFill(DS.Fields[i].FieldName, F+1)+''     :TSQLTimeStampField;'' );  // Floating-point numeric field                    ');
        Add('         //ftAutoInc    :Add(''    F''+RFill(DS.Fields[i].FieldName, F+1)+''     :TAutoIncField;''      );  // Auto-incrementing 32-bit integer counter field  ');
        Add('         //ftGraphic    :Add(''    F''+RFill(DS.Fields[i].FieldName, F+1)+''     :TGraphicField;''      );  // Bitmap field                                    ');
        Add('         //ftTime       :Add(''    F''+RFill(DS.Fields[i].FieldName, F+1)+''     :TTimeField;''         );  // Time field                                      ');
        Add('         //ftTypedBinary:Add(''    F''+RFill(DS.Fields[i].FieldName, F+1)+''     :TBinaryField;''       );  // Typed binary field                              ');
        Add('         //ftVarBytes   :Add(''    F''+RFill(DS.Fields[i].FieldName, F+1)+''     :TVarBytesField;''     );  // Variable number of bytes (binary storage)       ');
        Add('         //ftBlob       :Add(''    F''+RFill(DS.Fields[i].FieldName, F+1)+''     :TBlobField;''         );  // Binary Large OBject field                       ');
        Add('         //TIDispatchField                                                                                                                                     ');
        Add('         //TTVariantField                                                                                                                                      ');
        Add('         //ftBytes      :Add(''    F''+RFill(DS.Fields[i].FieldName, F+1)+''     :TBytesField;''        );  // Fixed number of bytes (binary storage)          ');
        Add('         //TInterfaceField                                                                                                                                     ');
        Add('         //ftDataSet    :Add(''    F''+RFill(DS.Fields[i].FieldName, F+1)+''     :TDataSetField;''      );  // DataSet field                                   ');
        Add('         //ftMemo       :Add(''    F''+RFill(DS.Fields[i].FieldName, F+1)+''     :TMemoField;''         );  // Text memo field                                 ');
        Add('         //ftWideMemo   :Add(''    F''+RFill(DS.Fields[i].FieldName, F+1)+''     :TWideMemoField;''); // Wide string field                                     ');
        Add('         //ftUnknown    :RaiseException(''TDBIntegrity.AddCurrentValue: Field DataType = ftUnknown''); // Unknown or undetermined                              ');
        Add('         //ftFmtMemo    ,                                                           // Formatted text memo field                                               ');
        Add('         //ftParadoxOle ,                                                           // Paradox OLE field                                                       ');
        Add('         //ftDBaseOle   ,                                                           // dBASE OLE field                                                         ');
        Add('         //ftCursor                                                                 // Output cursor from an Oracle stored procedure (TParam only)             ');
        Add('         //      :RaiseException(''TDBIntegrity.AddCurrentValue: Field DataType not Valid for a Primary Key'');                                                ');
        Add('         //else RaiseException(''TDBIntegrity.AddCurrentValue: Field DataType Unknown but not fkUnknown'');                                                    ');
        Add('      end;                                                                                                                                                     ');
        Add('   end;                                                                                                                                                        ');
        Add('end;                                                                                                                                                           ');
        Add('                                                                                                                                                               ');
        Add('function T'+Singular+'Orm.Evaluate:Boolean;                                                                                                                    ');
        Add('var Pair :TCalcPair;                                                                                                                                           ');
        Add('begin                                                                                                                                                          ');
        Add('   for Pair in FCalcPairs do begin                                                                                                                             ');
        Add('      if Pair.Formula.AsString.Trim <> '''' then begin                                                                                                         ');
        Add('         FParser.Formula := Pair.Formula.AsString;                                                                                                             ');
        Add('         if FParser.Evaluate then begin                                                                                                                        ');
        Add('            case Pair.Result.DataType of                                                                                                                       ');
        Add('               ftSmallint   :Pair.Result.Value := FParser.AsInteger;                                                                                           ');
        Add('               ftFloat      :Pair.Result.Value := FParser.AsFloat;                                                                                             ');
        Add('               ftString     :Pair.Result.Value := FParser.AsString;                                                                                            ');
        Add('               ftFMTBCD     :Pair.Result.Value := FParser.AsFloat;                                                                                             ');
        Add('               ftBCD        :Pair.Result.Value := FParser.AsFloat;                                                                                             ');
        Add('               ftBoolean    :Pair.Result.Value := FParser.AsBoolean;                                                                                           ');
        Add('               ftInteger    :Pair.Result.Value := FParser.AsInteger;                                                                                           ');
        Add('               ftWideString :Pair.Result.Value := FParser.AsString;                                                                                            ');
        Add('               ftWord       :Pair.Result.Value := FParser.AsInteger;                                                                                           ');
        Add('               ftCurrency   :Pair.Result.Value := FParser.AsFloat;                                                                                             ');
        Add('               ftLargeInt   :Pair.Result.Value := FParser.AsInteger;                                                                                           ');
        Add('               ftFixedChar  :Pair.Result.Value := FParser.AsString;                                                                                            ');
        Add('            end;                                                                                                                                               ');
        Add('         end                                                                                                                                                   ');
        Add('         else begin                                                                                                                                            ');
        Add('            FParser.Reset;                                                                                                                                     ');
        Add('            Pair.Result.Clear;                                                                                                                                 ');
        Add('         end;                                                                                                                                                  ');
        Add('      end;                                                                                                                                                     ');
        Add('   end;                                                                                                                                                        ');
        Add('end;                                                                                                                                                           ');
        Add('                                                                                                                                                               ');
        Add('function T'+Singular+'Orm.EvaluateField(Field :TField):TField;                                                                                                 ');
        Add('var Pair :TCalcPair;                                                                                                                                           ');
        Add('begin                                                                                                                                                          ');
        Add('   for Pair in FCalcPairs do begin                                                                                                                             ');
        Add('      if Pair.Formula.FieldName = Field.FieldName then begin                                                                                                   ');
        Add('         if Pair.Formula.AsString.Trim <> '''' then begin                                                                                                      ');
        Add('            FParser.Formula := Pair.Formula.AsString;                                                                                                          ');
        Add('            if FParser.Evaluate then begin                                                                                                                     ');
        Add('               case Pair.Result.DataType of                                                                                                                    ');
        Add('                  ftSmallint   :Pair.Result.Value := FParser.AsInteger;                                                                                        ');
        Add('                  ftFloat      :Pair.Result.Value := FParser.AsFloat;                                                                                          ');
        Add('                  ftString     :Pair.Result.Value := FParser.AsString;                                                                                         ');
        Add('                  ftFMTBCD     :Pair.Result.Value := FParser.AsFloat;                                                                                          ');
        Add('                  ftBCD        :Pair.Result.Value := FParser.AsFloat;                                                                                          ');
        Add('                  ftBoolean    :Pair.Result.Value := FParser.AsBoolean;                                                                                        ');
        Add('                  ftInteger    :Pair.Result.Value := FParser.AsInteger;                                                                                        ');
        Add('                  ftWideString :Pair.Result.Value := FParser.AsString;                                                                                         ');
        Add('                  ftWord       :Pair.Result.Value := FParser.AsInteger;                                                                                        ');
        Add('                  ftCurrency   :Pair.Result.Value := FParser.AsFloat;                                                                                          ');
        Add('                  ftLargeInt   :Pair.Result.Value := FParser.AsInteger;                                                                                        ');
        Add('                  ftFixedChar  :Pair.Result.Value := FParser.AsString;                                                                                         ');
        Add('               end;                                                                                                                                            ');
        Add('               Result := Pair.Result;                                                                                                                          ');
        Add('            end                                                                                                                                                ');
        Add('            else begin                                                                                                                                         ');
        Add('               FParser.Reset;                                                                                                                                  ');
        Add('               Pair.Result.Value := ''Expresi�n Err�nea: ''+Pair.Formula.AsString;                                                                             ');
        Add('               Result := Pair.Result;                                                                                                                          ');
        Add('            end;                                                                                                                                               ');
        Add('         end;                                                                                                                                                  ');
        Add('         Break; {After found the field to calculate, no more loops}                                                                                            ');
        Add('      end;                                                                                                                                                     ');
        Add('   end;                                                                                                                                                        ');
        Add('end;                                                                                                                                                           ');
     end;
     Add('');
     Add('function T'+Singular+'Orm.GetOrmVersion: string;                       ');
     Add('begin                                                                  ');
     Add('   Result := ''5.08'';                                                 ');
     Add('   {3.01 Correction of Clone method                                }   ');
     Add('   {3.02 Now NUMERIC field type is considered TCurrencyField class }   ');
     Add('   {3.03 Uses clause adapted for Firemonkey. (not run yet for VCL) }   ');
     Add('   {3.04 Added BLOB SUB_TYPE 1 (TEXT) fields for Memo Fiels        }   ');
     Add('   {3.05 Added BLOB SUB_TYPE 0 (BLOB) fields for Memo Fiels        }   ');
     Add('   {3.06 Improvement (totally new) method "Clone"                  }   ');
     Add('   {     New method "Assign"                                       }   ');
     Add('   {     New property "LineState :TDetailStatus;"                  }   ');
     Add('   {---------------------------------------------------------------}   ');
     Add('   {4.00 Generated with the information of his TFDDataSet Component}   ');
     Add('   {4.01 Method change his name to GetItemVersion from GetVersion  }   ');
     Add('   {4.02 New method "Clear". Set to Null all fiels, but            }   ');
     Add('   {     do not remove the DetailLines, DisplayFormats, etc.       }   ');
     Add('   {4.03 Removed from the first uses clause FMX.Controls and       }   ');
     Add('   {     FMX.StdCtrls, to allow compatibility with FMX and VCL     }   ');
     Add('   {4.04 Now based on TDataSet instead of TFDDataSet, to allow to  }   ');
     Add('   {     take the field definitions from TClientDataSets.          }   ');
     Add('   {4.05 FDetailLines :TList<TccCustomDragItem>; Disappears from   }   ');
     Add('   {     TccCustomDragItem class.                                  }   ');
     Add('   {   - A new property with the same functionallity but personali-}   ');
     Add('   {     zed, appears on each class, defined, here, in the         }   ');
     Add('   {     TDetailDataClass record.                                  }   ');
     Add('   {   - Base class name, changes his name, from TccCustomDragItem }   ');
     Add('   {     to TFieldValuesItem.                                      }   ');
     Add('   {5.00 Items becomes DBRows. Refactoring for accomodate names.   }   ');
     Add('   {5.01 Added the posibility of enable a MathParser.              }   ');
     Add('   {5.02 Added JSON decorators.                                    }   ');
     Add('   {5.03 Modification into Evaluate method.                        }   ');
     Add('   {5.04 New method EvaluateField.                                 }   ');
     Add('   {5.05 Removed JSON decorators.                                  }   ');
     Add('   {5.06 if the class has a Detailed class, i.e. Lines, or         }   ');
     Add('   {     Composition, when we clone the object, the Detail members }   ');
     Add('   {     will be cloned too.                                       }   ');
     Add('   {5.07 Added overloaded SetVar methods for improve the Parser    }   ');
     Add('   {5.08 Complete rename to ORM instead Row.                       }   ');
     Add('end;                                                                   ');
     Add('');
     Add('end.');
   end;
end;

end.
