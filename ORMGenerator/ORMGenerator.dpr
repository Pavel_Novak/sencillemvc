program ORMGenerator;





uses
  System.StartUpCopy,
  FMX.Forms,
  ViewMainForm in 'ViewMainForm.pas' {MainForm},
  DMCategories in '..\Northwind\Categories\DMCategories.pas' {DataModuleCategories: TDataModule},
  DMCustomers in '..\Northwind\Customers\DMCustomers.pas' {DataModuleCustomers: TDataModule},
  DMEmployees in '..\Northwind\Employees\DMEmployees.pas' {DataModuleEmployees: TDataModule},
  DMOrders in '..\Northwind\Orders\DMOrders.pas' {DataModuleOrders: TDataModule},
  DMProducts in '..\Northwind\Products\DMProducts.pas' {DataModuleProducts: TDataModule},
  DMShippers in '..\Northwind\Shippers\DMShippers.pas' {DataModuleShippers: TDataModule},
  DMSuppliers in '..\Northwind\Suppliers\DMSuppliers.pas' {DataModuleSuppliers: TDataModule},
  DMDevelopConnection in '..\Northwind\DMDevelopConnection.pas' {DevelopConnection: TDataModule};

{$R *.res}

begin
  Application.Initialize;
  Application.CreateForm(TMainForm, MainForm);
  Application.CreateForm(TDevelopConnection, DevelopConnection);
  Application.Run;
end.
