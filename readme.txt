Copyright 2016 by                                               
  senCille Software (Juan Carlos Cilleruelo)                          
                                                                      
   http://sencille.es                                                 
                                                                      
 Read this before modify the content.
 ==================================================                   
                                                                      
   This project is about the construction of an Model-View-Controller framework
   for Delphi with FMX and FireDAC.                                                     
                                                                     
                                                                      
Backup of this files                                 
====================                                
                                                                      
   All necessary backup is made inside git.
                                                                      
                                                                      
 Contact information                                             
 ---------------------                                                
                                                                      
      soporte@sencille.es                                            
                                                                      
      Phone: +34 607 880 970                                          
                                                                      
      Calle de las Tercias 19                                        
       47300 PENAFIEL (Valladolid) SPAIN                              
                                                                      
      http://sencille.es