program Northwind;

uses
  System.StartUpCopy,
  FMX.Forms,
  MainMenu in 'Northwind\MainMenu\MainMenu.pas' {MainMenuForm},
  DMDevelopConnection in 'Northwind\DMDevelopConnection.pas' {DevelopConnection: TDataModule},
  DMSuppliers in 'Northwind\Suppliers\DMSuppliers.pas' {DataModuleSuppliers: TDataModule},
  DMShippers in 'Northwind\Shippers\DMShippers.pas' {DataModuleShippers: TDataModule},
  DMProducts in 'Northwind\Products\DMProducts.pas' {DataModuleProducts: TDataModule},
  DMOrders in 'Northwind\Orders\DMOrders.pas' {DataModuleOrders: TDataModule},
  DMEmployees in 'Northwind\Employees\DMEmployees.pas' {DataModuleEmployees: TDataModule},
  DMCustomers in 'Northwind\Customers\DMCustomers.pas' {DataModuleCustomers: TDataModule},
  DMCategories in 'Northwind\Categories\DMCategories.pas' {DataModuleCategories: TDataModule},
  senCille.Tools in 'scFramework\senCille.Tools.pas',
  ORMCategory in 'Northwind\Categories\ORMCategory.pas',
  ORMCustomer in 'Northwind\Customers\ORMCustomer.pas',
  ORMEmployee in 'Northwind\Employees\ORMEmployee.pas',
  ORMOrder in 'Northwind\Orders\ORMOrder.pas',
  ORMProduct in 'Northwind\Products\ORMProduct.pas',
  ORMShipper in 'Northwind\Shippers\ORMShipper.pas',
  ORMSupplier in 'Northwind\Suppliers\ORMSupplier.pas',
  senCille.ORMClasses in 'scFramework\senCille.ORMClasses.pas',
  ModelCategories in 'Northwind\Categories\ModelCategories.pas',
  senCille.CustomModel in 'scFramework\senCille.CustomModel.pas',
  senCille.DBController in 'scFramework\senCille.DBController.pas',
  senCille.SQLConnect in 'scFramework\senCille.SQLConnect.pas';

{$R *.res}

begin
  Application.Initialize;
  Application.CreateForm(TMainMenuForm, MainMenuForm);
  Application.CreateForm(TDevelopConnection, DevelopConnection);
  Application.Run;
end.
