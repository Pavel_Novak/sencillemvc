Content of the folder [bin]                                 
 ---------------------------------------------------------------      
                                                                      
 Copyright 2016 by                                               
  senCille Software (Juan Carlos Cilleruelo)                          
                                                                      
   http://sencille.es                                                 
                                                                      
 Read this before modify the content.
 ==================================================                   
                                                                      
   This folder contains the target files of all the project.                                                     
                                                                      
   This means that all the files, not added to git, can be 
   deleted, because is the execution of the project who generate
   all of them.
                                                                      
Backup of this files                                 
====================                                
                                                                      
   It's not necessary.
                                                                      
                                                                      
 Contact information                                             
 ---------------------                                                
                                                                      
     � soporte@sencille.es                                            
                                                                      
     � Tel�fono: +34 607 880 970                                          
                                                                      
     � Calle de las Tercias 19                                        
       47300 PE�AFIEL (Valladolid) SPAIN                              
                                                                      
     � http://sencille.es