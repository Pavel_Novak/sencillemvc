Contenido del directorio [senCilleDB]                                 
 ---------------------------------------------------------------      
                                                                      
 Copyright 1996-2016 by                                               
  senCille Software (Juan Carlos Cilleruelo)                          
                                                                      
   http://sencille.es                                                 
                                                                      
 Lea esto antes de tocar el contetenido                               
 ==================================================                   
                                                                      
   Esta carpeta contiene los archivos de base de datos de la aplica-  
   ci�n senCille.                                                     
                                                                      
   Estos archivos son manejados por el propio programa y no hay nada  
   que el usuario de su computadora pueda realizar con ellos sin uti- 
   lizar la aplicaci�n, salvo una cosa...                             
                                                                      
Copias de seguridad de estos archivos                                 
======================================                                
                                                                      
   Incluya esta carpeta al completo en la estrategia de copias de se- 
   guridad de su computadora.                                         
                                                                      
   Cada archivo contiene todos los datos referidos a una empresa del  
   programa  senCille. El m�s  importante de todos es "senCille.ib",  
   pues contiene las referencias al resto de los archivos de base de  
   datos.                                                             
                                                                      
     � Haga copia de los archivos con regularidad.                    
                                                                      
     � Si necesita restaurar alguna de las copias, siga las instruc-  
       ciones proporcionadas en la ayuda del programa.                
                                                                      
     � No dude en  ponerse en  contacto con nosotros  para cualquier  
       otra duda.                                                     
                                                                      
 Contacte con nosotros                                                
 ---------------------                                                
                                                                      
     � soporte@sencille.es                                            
                                                                      
     � Tel�fono: 983 880 970                                          
                                                                      
     � Calle de las Tercias 19                                        
       47300 PE�AFIEL (Valladolid) SPAIN                              
                                                                      
     � http://sencille.es                                             
                                                                      
                                                                      
