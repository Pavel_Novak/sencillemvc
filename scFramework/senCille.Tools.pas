unit senCille.Tools;

interface

uses Data.DB, System.Classes,
     FMX.TreeView, FMX.Types, FMX.ListBox;

type TTypeOfField       = (tcString, tcInteger, tcBoolean, tcFloat, tcDate);
     TShowMessageEvent  = procedure(Value :string ) of object;
   (*
   Records can have constructor, with at least one parameter.
   Records support properties and static methods.
   Records support operator overloading.
   Records can have methods (functions, procedures).
   Records do NOT support virtual methods ("virtual", "dynamic", and "message")
   Records do NOT support destructors.
   *)
  TDisplayFormats = record
    strict private
    type  str30 = string;
    public
    constructor Create(const DummyPrm :string);
    var
      IntegerFormat  :str30;
      DecimalFormat  :str30;
      CodeFormat     :str30;
      PercentFormat  :str30;
      PriceFormat    :str30;
      ImportFormat   :str30;
      QuantityFormat :str30;
      DateFormat     :str30;
      TimeFormat     :str30;
      DateTimeFormat :str30;
  end;

  TTools = class
     class procedure AssignFormatField(AField :TField; ADisplayFormats :TDisplayFormats);
     class function CloneField(Source: TField; AOwner: TComponent): TField;
     class function NoAssign(AField :TField):Boolean;
     class procedure AssignFormatToDBFields(ADataSet: TDataSet; ADisplayFormats :TDisplayFormats);
  end;

implementation

uses System.SysUtils, System.Math, System.IOUtils, System.StrUtils, System.Messaging,
     System.Masks, System.TypInfo, System.Variants,
     FMX.Utils,
     {---------------------------------------------------------}
     {$IF DEFINED(MSWINDOWS)}
        Winapi.ShellAPI, Winapi.Windows,
     {$ELSEIF DEFINED(MACOS)}
        Posix.Stdlib,
     {$ENDIF}
     FireDAC.Comp.DataSet;

{ TDisplayFormats }
constructor TDisplayFormats.Create(const DummyPrm: string);
begin
   {Cause the Constructor is not mandatory, this is for configure default values}
   IntegerFormat  := '#';
   DecimalFormat  := '#.##';
   CodeFormat     := '0';
   PercentFormat  := '#.## %';
   PriceFormat    := '#.##';
   ImportFormat   := '#.##';
   QuantityFormat := '#.##';
   DateFormat     := 'dd/mm/yyyy';
   TimeFormat     := 'mm:ss';
   DateTimeFormat := 'dd/mm/yyyy mm:ss';
end;


{ TDelegate }

class procedure TTools.AssignFormatField(AField :TField; ADisplayFormats :TDisplayFormats);
{-----------------------------------------------------------------------
   Hierarchy of TFields

   Do not confuse TCurrencyField with the Currency data type.
        - Currency fields use the double data type to store and manipulate their values.
             This data type is the format used by the physical database tables for currency fields.
        - The TBCDField class uses the Currency data type to store and manipulate its value.

          TField
             |_ TStringField
                     |_ TWideStringField
                     |_ TGUIDField
             |_ TNumericField
                     |_ TIntegerField
                               |_ TSmallIntField
                               |_ TShortIntField
                               |_ TByteField
                               |_ TWordField
                               |_ TAutoIncField
                     |_ TLongWordField
                               |_ TUnsignedAutoIncField
                     |_ TLargeIntField
                     |_ TFloatField
                               |_ TCurrencyField
                     |_ TSingleField      (float)
                     |_ TExtendedField    (float)
                     |_ TBCDField         (4 decimal places and 20 significant digits)
                     |_ TFMTBCDField

             |_ TDateTimeField
             |_ TSQLTimeStampField

             |_ TBooleanField
             |_ TBinaryField
             |_ TBlobField
             |_ TObjectField
             |_ TVariantField
             |_ TInterfaceField
             |_ TAggregateField
-----------------------------------------------------------------------}
var FieldNumeric   :TNumericField;
    FieldDateTime  :TDateTimeField;
    FieldTimeStamp :TSQLTimeStampField;
begin
   if AField is TNumericField then begin
      FieldNumeric := TNumericField(AField);
      FieldNumeric.Alignment := taRightJustify;
      if (LeftStr(FieldNumeric.FieldName, 4) = 'INT_') then FieldNumeric.DisplayFormat := ADisplayFormats.IntegerFormat  else
      if (LeftStr(FieldNumeric.FieldName, 4) = 'DEC_') then FieldNumeric.DisplayFormat := ADisplayFormats.DecimalFormat  else
      if (LeftStr(FieldNumeric.FieldName, 4) = 'COD_') then FieldNumeric.DisplayFormat := ADisplayFormats.CodeFormat     else
      if (LeftStr(FieldNumeric.FieldName, 4) = 'PRC_') then FieldNumeric.DisplayFormat := ADisplayFormats.PercentFormat  else
      if (LeftStr(FieldNumeric.FieldName, 4) = 'PRI_') then FieldNumeric.DisplayFormat := ADisplayFormats.PriceFormat    else
      if (LeftStr(FieldNumeric.FieldName, 4) = 'IMP_') then FieldNumeric.DisplayFormat := ADisplayFormats.ImportFormat   else
      if (LeftStr(FieldNumeric.FieldName, 4) = 'QTY_') then FieldNumeric.DisplayFormat := ADisplayFormats.QuantityFormat else
      if (FieldNumeric is TIntegerField ) or
         (FieldNumeric is TLongWordField) or
         (FieldNumeric is TLargeIntField) then FieldNumeric.DisplayFormat := ADisplayFormats.IntegerFormat else
      if (FieldNumeric is TFloatField   ) or
         (FieldNumeric is TSingleField  ) or
         (FieldNumeric is TExtendedField) or
         (FieldNumeric is TBCDField     ) or
         (FieldNumeric is TFMTBCDField  ) then FieldNumeric.DisplayFormat := ADisplayFormats.DecimalFormat else
   end else
   if AField is TDateTimeField then begin
      FieldDateTime := TDateTimeField(AField);
      if (LeftStr(FieldDateTime.FieldName, 4) = 'DTE_') then FieldDateTime.DisplayFormat := ADisplayFormats.DateFormat     else
      if (LeftStr(FieldDateTime.FieldName, 4) = 'TME_') then FieldDateTime.DisplayFormat := ADisplayFormats.TimeFormat     else
      if (LeftStr(FieldDateTime.FieldName, 4) = 'DTT_') then FieldDateTime.DisplayFormat := ADisplayFormats.DateTimeFormat else
   end else
   if AField is TSQLTimeStampField then begin
      FieldTimeStamp := TSQLTimeStampField(AField);
      if (LeftStr(FieldTimeStamp.FieldName, 4) = 'DTE_') then FieldTimeStamp.DisplayFormat := ADisplayFormats.DateFormat     else
      if (LeftStr(FieldTimeStamp.FieldName, 4) = 'TME_') then FieldTimeStamp.DisplayFormat := ADisplayFormats.TimeFormat     else
      if (LeftStr(FieldTimeStamp.FieldName, 4) = 'DTT_') then FieldTimeStamp.DisplayFormat := ADisplayFormats.DateTimeFormat else
   end;
end;


class function TTools.CloneField(Source: TField; AOwner: TComponent): TField;

   procedure SetProp(Name: string);
   var V: variant;
       PropInfo: PPropInfo;
   begin
      PropInfo := System.TypInfo.GetPropInfo(Source, Name);
      if PropInfo <> nil then begin
         try V := System.TypInfo.GetPropValue(Source,Name);
            if not VarIsNull(V) then
               System.TypInfo.SetPropValue(Result,Name,V);
         except ; //just kill exception
         end;
      end;
   end;

begin
   Result := TFieldClass(Source.ClassType).Create(AOwner);

   Result.Alignment              := Source.Alignment;
   Result.AutoGenerateValue      := Source.AutoGenerateValue;
   Result.CustomConstraint       := Source.CustomConstraint;
   Result.ConstraintErrorMessage := Source.ConstraintErrorMessage;
   Result.DefaultExpression      := Source.DefaultExpression;
   Result.DisplayLabel           := Source.DisplayLabel;
   Result.DisplayWidth           := Source.DisplayWidth;
   Result.FieldKind              := Source.FieldKind;
   Result.FieldName              := Source.FieldName;
   Result.ImportedConstraint     := Source.ImportedConstraint;
   Result.LookupDataSet          := Source.LookupDataSet;
   Result.LookupKeyFields        := Source.LookupKeyFields;
   Result.LookupResultField      := Source.LookupResultField;
   Result.KeyFields              := Source.KeyFields;
   Result.LookupCache            := Source.LookupCache;
   Result.ProviderFlags          := Source.ProviderFlags;
   Result.ReadOnly               := Source.ReadOnly;
   Result.Required               := Source.Required;
   Result.Visible                := Source.Visible;

   SetProp('EditMask'          );
   SetProp('FixedChar'         );
   SetProp('Size'              );
   SetProp('Transliterate'     );
   SetProp('DisplayFormat'     );
   SetProp('EditFormat'        );
   SetProp('Currency'          );
   SetProp('MaxValue'          );
   SetProp('MinValue'          );
   SetProp('Precision'         );
   SetProp('DisplayValues'     );
   SetProp('BlobType'          );
   SetProp('ObjectType'        );
   SetProp('IncludeObjectField');
   SetProp('ReferenceTableName');
   SetProp('Active'            );
   SetProp('Expression'        );
   SetProp('GroupingLevel'     );
   SetProp('IndexName'         );
end;

class function TTools.NoAssign(AField :TField):Boolean;
begin
   {Conditions for not assign values}
   Result := (AField.ReadOnly) or
             (AField is TFDAutoIncField);
end;

class procedure TTools.AssignFormatToDBFields(ADataSet: TDataSet; ADisplayFormats :TDisplayFormats);
var Field :TField;
begin
   for Field in ADataSet.Fields do begin
      if Length(Field.FieldName) >= 4 then begin
         TTools.AssignFormatField(Field, ADisplayFormats);
      end;
   end;
end;

end.






