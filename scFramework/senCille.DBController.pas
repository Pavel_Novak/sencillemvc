unit senCille.DBController;

interface

uses System.Classes,
     FireDAC.Stan.Intf, FireDAC.Stan.Option, FireDAC.Stan.Error, FireDAC.UI.Intf,
     FireDAC.Phys.Intf, FireDAC.Stan.Def, FireDAC.Stan.Pool, FireDAC.Stan.Async,
     FireDAC.Phys, FireDAC.FMXUI.Wait, FireDAC.Comp.UI,
     {Firebird}
     FireDAC.Phys.FB,
     FireDAC.Phys.IBBase,          {This unit is necessary in both cases}
     {------------------}
     FireDAC.Comp.Client;

type
  TDBController = class(TPersistent)
  private
    {this is going to be public until solve the interdependency with Setup class}
    FDBConnection :TFDConnection; {is a TIBCConnection descendant with helpers}
    FDataFolder    :string;
    FHostName      :string;
    FMasterDB      :string;
    FDataBase      :string;
    FUserName      :string;
    FPassword      :string;
    FClientLibrary :string;
    FVendorHome    :string;
    FUserLogged    :string;
    {Firebird}
    FPhysicalDriver  :TFDPhysFBDriverLink;
    {--------}
    procedure SetDatabase(Value :string);
  protected
  public
    //FDBConnection :TFDConnection; {is a TIBCConnection descendant with helpers}
    constructor Create; overload;
    destructor Destroy; override;
    function  DatabaseExists:Boolean;  {This is not for a property read}
    function  Clone(prmDBName :string):TDBController;
    procedure OpenConnection;
    function  IsConnected:Boolean;
    function  Disconnect:Boolean;
    procedure LoadConfFromFile(prmFileName :string);
    procedure SaveConfToFile(prmFileName :string);
    function GetQuery(const SQL :array of string):TFDQuery;

    {Only for File Based Databases}
    class function DBFileExtension:string;
    {-----------------------------}
    property DBConnection  :TFDConnection  read FDBConnection;
    property ClientLibrary :string         read FClientLibrary write FClientLibrary;
    property VendorHome    :string         read FVendorHome    write FVendorHome;
    property DataFolder    :string         read FDataFolder    write FDataFolder;
    property HostName      :string         read FHostName      write FHostName  ;
    property MasterDB      :string         read FMasterDB      write FMasterDB  ;
    property DataBase      :string         read FDataBase      write SetDatabase;
    property UserName      :string         read FUserName      write FUserName  ;
    property Password      :string         read FPassword      write FPassword  ;
    property UserLogged    :string         read FUserLogged    write FUserLogged;
  end;

implementation

uses System.IOUtils, System.SysUtils, System.IniFiles,
     senCille.SQLConnect,
     FMX.Dialogs;

function GetAppFolder: string;
begin
   Result := TPath.GetDirectoryName(ParamStr(0));
end;

constructor TDBController.Create;
begin
   FHostName  := '';
   FMasterDB  := '';
   FDataBase  := '';
   FUserName  := '';
   FPassword  := '';

   FClientLibrary := GetAppFolder+PathDelim+'fbembed.dll';
   FVendorHome    := '';
   FPhysicalDriver := TFDPhysFBDriverLink.Create(nil);

   FPhysicalDriver.VendorHome := FVendorHome;
   FPhysicalDriver.VendorLib  := FClientLibrary;

   FDBConnection := TFDConnection.Create(nil);
   FDBConnection.LoginPrompt    := False;
   FDBConnection.ConnectionName := '';
   FDBConnection.FormatOptions.StrsTrim2Len     := True; // Cut the length of data to the max length of the field.
   FDBConnection.FormatOptions.StrsTrim         := True; // Removie leading blanks.
   FDBConnection.FormatOptions.StrsEmpty2Null   := True; // In string values, let blanck fields to NULL value.
   FDBConnection.ResourceOptions.AssignedValues := [rvAutoReconnect];
   FDBConnection.ResourceOptions.AutoReconnect  := True;
   FDBConnection.Params.Add('DriverID=FB'              );
   FDBConnection.Params.Add('CharacterSet=UTF8'        );
   FDBConnection.Params.Add('VendorLib='+FClientLibrary);
   FDBConnection.Params.Add('Protocol=Local'           );
   FDBConnection.Params.Add('SQLDialect=3'             );
   FDBConnection.Params.Add('UseUnicode=True'          );
   FDBConnection.Params.Add('ExtendedMetadata=True'    );
   FDBConnection.Params.Add('Pooled=False'             );
   FDBConnection.Params.Add('CreateDatabase=No'        );
   FDBConnection.Params.Add('PageSize=4096'            );
   FDBConnection.Params.Add('EnableMemos=True'         );
end;

destructor TDBController.Destroy;
begin
   FDBConnection.Free;
   FPhysicalDriver.Free;
end;

function TDBController.Clone(prmDBName: string): TDBController;
begin
   Result := TDBController.Create;
   Result.DataFolder := FDataFolder;
   Result.HostName   := FHostName;
   Result.MasterDB   := FMasterDB;
   Result.DataBase   := prmDBName;  {<= DBName assignament}
   Result.UserName   := FUserName;
   Result.Password   := FPassword;
end;

function TDBController.IsConnected:Boolean;
begin
   Result := FDBConnection.Connected;
end;

function TDBController.Disconnect:Boolean;
begin
   FDBConnection.Close;
   Result := not FDBConnection.Connected;
end;

procedure TDBController.OpenConnection;
begin
   {If it is previously connected the do nothing}
   if not FDBConnection.Connected then begin
      {Firebird}
      FDBConnection.Params.Add('Database=' +DataFolder+PathDelim+FDatabase+DBFileExtension);
      FDBConnection.Params.Add('User_Name='+FUserName);
      FDBConnection.Params.Add('Password=' +FPassword);
      {---------}
      FDBConnection.Open;
   end;
end;

procedure TDBController.LoadConfFromFile(prmFileName :string);
var INIFile :TIniFile;
begin
   INIFile := TIniFile.Create(prmFileName);
   try
      FDataBase   := INIFile.ReadString('DBCONFIG', 'DataBase'   , '');
      FUserName   := INIFile.ReadString('DBCONFIG', 'User_Name'  , '');
      FDataFolder := INIFile.ReadString('OPTIONS' , 'DataFolder' , '');
      FPassword   := INIFile.ReadString('DBCONFIG', 'Password'   , '');
   finally
      INIFile.Free;
   end;
end;

procedure TDBController.SaveConfToFile(prmFileName :string);
var INIFile :TIniFile;
begin
   INIFile := TIniFile.Create(prmFileName);
   try
     {Firebird}
     INIFile.WriteString('DBCONFIG', 'Server'   , HostName);
     INIFile.WriteString('DBCONFIG', 'DataBase' , Database);
     INIFile.WriteString('DBCONFIG', 'User_Name', UserName);
     INIFile.WriteString('DBCONFIG', 'Password' , Password);
   finally
      INIFile.Free;
   end;
end;

function TDBController.GetQuery(const SQL: array of string): TFDQuery;
var Query :TFDQuery;
    i     :Integer;
begin
   Query := TFDQuery.Create(nil);
   Query.Connection := Self.FDBConnection;
   for i := Low(SQL) to High(SQL) do begin
      Query.SQL.Add(SQL[i]);
   end;
   Result := Query;
end;

function TDBController.DatabaseExists:Boolean;
begin
   Result := TFile.Exists(DataFolder+PathDelim+FDatabase+DBFileExtension);
end;

procedure TDBController.SetDatabase(Value :string);
begin
   if FDatabase <> Value then
      FDatabase := Value;
end;

class function TDBController.DBFileExtension:string;
begin
   Result := '.FDB';
end;

end.
