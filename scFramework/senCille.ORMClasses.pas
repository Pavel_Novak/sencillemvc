unit senCille.ORMClasses;

interface

uses System.Classes, System.UITypes, FMX.StdCtrls, System.Generics.Collections, System.Contnrs,
     DataSnap.DBClient, Data.DB,
     FMX.Controls, FMX.Types, senCille.Tools;
   {$TYPEINFO ON}
type
  TOrmStatus = (rsUnchanged, rsModified, rsNew, rsDeleted, rsUnknown, rsShallBeSaved);

  TscValidateEvent = procedure(Sender: TField) of object;

  TCustomOrm = class; {fordware declaration}

  TCalculateEvent = procedure(Value :TCustomOrm) of object;

  TCalcPair = class
    Result  :TField;
    Formula :TField;
  end;

  TCustomOrm = class
  private
    FPrimaryKeyFields :TFields;
    FCalculating      :Boolean;  {The DragItem is now in the middle of Calcs}
    FOnCalculate      :TCalculateEvent;
    FBindDataSet      :TClientDataSet;
    FAllowCalcs       :Boolean; {Barrier to allow or not allow calculations}
    FDisplayFormats   :TDisplayFormats;
    FInViewIndex      :Integer; {This index points to the current Item in, by example, a TreeView component}
    procedure SetOnCalculate(Value :TCalculateEvent);
    procedure SetBindDataSet(Value :TClientDataSet);
    procedure HandleFieldsValidation(Sender: TField);
    function GetDataSetState:TDataSetState;
  protected
    FDataSet :TClientDataSet; {This is mandatory to allow all fields to have a Father}
    procedure AssignFormatToOwnFields;
  public
    Fields  :TDictionary<string, TField>;
    constructor Create(ADisplayFormats :TDisplayFormats);
    destructor Destroy; override;
    procedure Initialize; virtual; abstract;
    function  Modified(OldValuesItem: TCustomOrm):Boolean;
    function  Calculate:Boolean;
    {---------------------}
    procedure New;
    procedure Edit;
    {---------------------}
    procedure Post;
    procedure Cancel;
    procedure DisableCalcs;
    procedure EnableCalcs;
    procedure SendUpdates;
    property PrimaryKeyFields :TFields         read FPrimaryKeyFields;
    property DisplayFormats   :TDisplayFormats read FDisplayFormats;
    property InViewIndex      :Integer         read FInViewIndex       write FInViewIndex;
  published
    property OnCalculate     :TCalculateEvent  read FOnCalculate       write SetOnCalculate;
    property BindDataSet     :TClientDataSet   read FBindDataSet       write SetBindDataSet;
    property State           :TDataSetState    read GetDataSetState;
  end;

implementation

uses System.SysUtils, System.StrUtils, FireDAC.Comp.DataSet;

{ TCustomOrm }

constructor TCustomOrm.Create(ADisplayFormats :TDisplayFormats);
var Field :TField;
begin
   FInViewIndex := -1; {This is the value for a InViewIndex never assigned}
   FAllowCalcs    := True;
   FDataSet       := TClientDataSet.Create(nil);
   Fields         := TObjectDictionary<string, TField>.Create([doOwnsValues]);
   FCalculating   := False;

   FDisplayFormats := ADisplayFormats;

   Initialize;

   {Assign the Display format to each Field}
   for Field in Fields.Values do begin
      if Length(Field.FieldName) >= 4 then begin
         TTools.AssignFormatField(Field, FDisplayFormats);
      end;
   end;
end;

destructor TCustomOrm.Destroy;
begin
   if Assigned(FBindDataSet) then begin
      if FBindDataSet.State in dsEditModes then FBindDataSet.Cancel;
      //FBindDataSet.EmptyDataSet;
      FBindDataSet := nil;
   end;

   FBindDataSet := nil;
   FOnCalculate := nil;
   FreeAndNil(Fields);
   FreeAndNil(FDataSet);
   inherited;
end;

procedure TCustomOrm.AssignFormatToOwnFields;
var Field :TField;
begin
   for Field in Fields.Values do begin
      if Length(Field.FieldName) >= 4 then begin
         TTools.AssignFormatField(Field, FDisplayFormats);
      end;
   end;
end;

procedure TCustomOrm.New;
begin
   FDataSet.Insert;
   if Assigned(FBindDataSet) then begin
      if FBindDataSet.State in dsEditModes then begin
         FBindDataSet.Cancel;
      end;
      FBindDataSet.Append;
   end;
end;

procedure TCustomOrm.Edit;
begin
   FDataSet.Edit;
   if Assigned(FBindDataSet) then begin
      if FBindDataSet.State in dsEditModes then begin
         FBindDataSet.Cancel;
      end;
      FBindDataSet.Edit;
   end;
end;

function TCustomOrm.Modified(OldValuesItem: TCustomOrm):Boolean;
var CurrentField :TField;
begin
   Result := False;
   for CurrentField in FDataSet.Fields do begin
      {avoid use the imports (calcs) into the comparate, because may be little diffs between DB and Calcs}
      if LeftStr(CurrentField.FieldName, 4) <> 'IMP_' then begin
         if Fields.ContainsKey(CurrentField.FieldName) then begin
            if Fields[CurrentField.FieldName].DisplayText <> OldValuesItem.Fields[CurrentField.FieldName].DisplayText then begin
               Result := True;
               Break;
            end;
         end;
      end;
   end;
end;

procedure TCustomOrm.Post;
var Field :TField;
begin
   if Assigned(FBindDataSet) then begin
      for Field in BindDataSet.Fields do begin
         Field.OnValidate := nil;
      end;
      SendUpdates;
      FBindDataSet.Post;
   end;
   FDataSet.Post;
end;

procedure TCustomOrm.Cancel;
var Field :TField;
begin
   if Assigned(FBindDataSet) then begin
      FBindDataSet.Cancel;
      for Field in BindDataSet.Fields do begin
         Field.OnValidate := nil;
      end;
   end;
   FDataSet.Cancel;
end;

function TCustomOrm.Calculate: Boolean;
begin
   {   Normally the execution of the calculate modify other fields of the Item.   }
   {   If each modification call other time the calculate delegate this falls     }
   { into a infinity loop.                                                        }
   {   The variable FCalculating acts as a barrier that allows the first call to  }
   { OnCalculate finish without other interferences.                              }
   if not FCalculating then begin
      if Assigned(OnCalculate) then begin
         FCalculating := True;
         try
            OnCalculate(Self); {<= Here is the Main pourpouse of this function}
         finally
            FCalculating := False;
         end;
         Result := True;
      end
      else Result := False;
   end
   else Result := False;
end;

procedure TCustomOrm.SetOnCalculate(Value :TCalculateEvent);
begin
   FOnCalculate := Value;
end;

procedure TCustomOrm.SetBindDataSet(Value :TClientDataSet);
var Field :TField;
begin
   {If BindDataSet is currently assigned and is going to be set to nil...}
   {     Disable OnValidate event handler of each field}
   if (FBindDataSet <> nil) and (Value = nil) then begin
      for Field in BindDataSet.Fields do begin
          Field.OnValidate := nil;
      end;
   end;

   FBindDataSet := Value;

   if FBindDataSet <> nil then begin
      SendUpdates;
      for Field in BindDataSet.Fields do begin
         Field.OnValidate := HandleFieldsValidation;
      end;
   end;
end;

procedure TCustomOrm.SendUpdates;
var CurrentField :TField;
begin
   if not Assigned(FBindDataSet) then Exit;

   if FBindDataSet.IsEmpty then begin
      FBindDataSet.Append;
   end else
   if not (FBindDataSet.State in dsEditModes) then begin
      FBindDataSet.Edit;
      //raise Exception.Create('senCille: The Dataset binded shall be in anyone of the Edit modes');
   end;

   {In all cases, at this point FBindDataSet shall be in Edit or Insert mode and we put into the values}
   FBindDataSet.DisableControls;
   DisableCalcs;
   try
      for CurrentField in FBindDataSet.Fields do begin
         if Fields.ContainsKey(CurrentField.FieldName) then begin
            {If Dest Field is ReadOnly (by example the lookup fields) go to the next field}
            if FBindDataSet.FieldByName(CurrentField.FieldName).ReadOnly then Continue;
            FBindDataSet.FieldByName(CurrentField.FieldName).OnValidate := nil;
            try

               if Fields[CurrentField.FieldName].IsNull then
                  FBindDataSet.FieldByName(CurrentField.FieldName).Clear
               else
                  FBindDataSet.FieldByName(CurrentField.FieldName).Value := Fields[CurrentField.FieldName].Value;

            finally
               FBindDataSet.FieldByName(CurrentField.FieldName).OnValidate := HandleFieldsValidation;
            end;
         end;
      end;
   finally
      EnableCalcs;
      FBindDataSet.EnableControls;
   end;
end;

procedure TCustomOrm.HandleFieldsValidation(Sender: TField);
var i :string;
begin
   {A Sender Field has changed his value}
   {We use the FieldName of Sender to assign his new value to her correspondent property in the Orm}
   if Fields.ContainsKey(Sender.FieldName) then begin
      if Sender.IsNull then Fields[Sender.FieldName].Clear
                       else Fields[Sender.FieldName].Value := Sender.Value;
      if FAllowCalcs then begin
         {<======>}
         Calculate;
         {<======>}

         FBindDataSet.DisableControls;
         try
            {At this point if the calc has modify any data, we shall feed-back to his field}
            {We traverse all fields and send to DataSet all that were changed}
            for i in Fields.Keys do begin
               if FBindDataSet.FieldByName(Fields[i].FieldName).AutoGenerateValue = arAutoInc then Continue;
               if FBindDataSet.FieldByName(Fields[i].FieldName).ReadOnly                      then Continue;
               if (Fields[i].FieldName <> Sender.FieldName) then begin
                  FBindDataSet.FieldByName(Fields[i].FieldName).OnValidate := nil;
                  try
                     FBindDataSet.FieldByName(Fields[i].FieldName).Value := Fields[i].Value;
                  finally
                     FBindDataSet.FieldByName(Fields[i].FieldName).OnValidate := HandleFieldsValidation;
                  end;
               end;
            end;
         finally
            FBindDataSet.EnableControls;
         end;
      end;
   end;
end;

function TCustomOrm.GetDataSetState:TDataSetState;
begin
   Result := FDataSet.State;
end;

procedure TCustomOrm.DisableCalcs;
begin
   FAllowCalcs := False;
end;

procedure TCustomOrm.EnableCalcs;
begin
   FAllowCalcs := True;
   //Calculate; The programmer shall calculate after enable calcs. This is too transparent for the process.
end;

end.




