unit senCille.SQLConnect;

interface

uses
  SysUtils, Classes, DB, SqlExpr,
  FireDAC.Stan.Intf, FireDAC.Stan.Option, FireDAC.Stan.Error, FireDAC.UI.Intf,
  FireDAC.Phys.Intf, FireDAC.Stan.Def, FireDAC.Stan.Pool, FireDAC.Stan.Async,
  FireDAC.Phys, FireDAC.FMXUI.Wait, FireDAC.Comp.UI, FireDAC.Phys.IBBase,
  FireDAC.Phys.IB, FireDAC.Comp.Client, FireDAC.Comp.Script,
  Data.SQLTimSt;

type
  TSQLConnectionHelper = class helper for TFDConnection
  private
  protected
    function GetServerDate():TSQLTimeStamp;
  public
    function CreateQuery(const prmSQL :array of string):TFDQuery;
    function CreateScript(const prmSQL :array of string):TFDScript;
  published
    property ServerDate :TSQLTimeStamp read GetServerDate;
  end;

implementation

function TSQLConnectionHelper.CreateQuery(const prmSQL :array of string):TFDQuery;
var Query :TFDQuery;
    i     :Integer;
begin
   Query := TFDQuery.Create(nil);
   Query.Connection := Self;
   for i := Low(prmSQL) to High(prmSQL) do begin
      Query.SQL.Add(prmSQL[i]);
   end;
   Result := Query;
end;

function TSQLConnectionHelper.CreateScript(const prmSQL :array of string):TFDScript;
var Script :TFDScript;
    i      :Integer;
    SQL    :TStringList;
begin
   SQL := TStringList.Create;
   try
      for i := Low(prmSQL) to High(prmSQL) do begin
         SQL.Add(prmSQL[i]);
      end;
      Script := TFDScript.Create(nil);
      Script.Connection := Self;
      Script.SQLScripts[0].SQL.Add(SQL.Text);
      Result := Script;
   finally
      SQL.Free;
   end;
end;

function TSQLConnectionHelper.GetServerDate: TSQLTimeStamp;
begin
   Result := DateTimeToSQLTimeStamp(Date);
end;

end.

